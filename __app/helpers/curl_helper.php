<?php
defined('BASEPATH') or exit('No direct script access allowed');

function curl_post($url, $payload, $headers = [])
{
	$ch = curl_init($url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($payload));
	if ($headers)
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

	// execute!
	$response = curl_exec($ch);

	// close the connection, release resources used
	curl_close($ch);

	return $response;
}

function curl_get($url, $payload = [], $headers = [])
{
	$ch = curl_init($url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
	if ($payload)
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($payload));
	if ($headers)
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

	// execute!
	$response = curl_exec($ch);

	// close the connection, release resources used
	curl_close($ch);


	return $response;
}
