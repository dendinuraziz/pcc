<?php
defined('BASEPATH') or exit('No direct script access allowed');

include_once('curl_helper.php');
function get_simpatika_peserta($token)
{
	$CI = &get_instance();
	$endpoint = $CI->config->item('endpoint_api_simpatika') . '/ptk/profil-calon-kamad';
	$headers = [
		'Authorization: Bearer ' . $token
	];
	$res = curl_get($endpoint, [], $headers);

	return json_decode($res, true);
}
