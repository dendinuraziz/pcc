<?php defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/API_Controller.php';

class Aman extends API_Controller
{
    public function __construct() {
        parent::__construct();
        $this->load->model("M_api","m");
    }

 
    public function generateKeyDendi(){
        $this->load->library('Authorization_Token');
        $token = array(
            "iss" => 1,
            "aud" => 1,            
            "data" => array(
                "id" => 1,
                "firstname" => "aman"
            )
        );

        echo  $token = $this->authorization_token->generateToken($token);
    }
    public function shortlist()
    {
        $this->load->library('Authorization_Token');
        header("Access-Control-Allow-Origin: *");

         $token = $this->authorization_token->validateToken();
        
        $user_data = $this->_apiConfig([
            'methods' => ['POST'],
            'requireAuthorization' => true,
        ]);

        $madrasah = $this->m->getMadrasah()->result_array();
        $jml      = $this->m->getMadrasah()->num_rows();

        if($jml>0){
                $this->api_return(
                    [
                        'code' => 200,
                        'message' => "success",                      
                        "data" => [
                            'jumlah' => $jml,
                            'shortlist' => $madrasah
                        ],
                    ],
                200);
        }else{

            $this->api_return(
                [
                    'code' => 400,
                    'message' => "Data tidak ditemukan"
                    
                ],
            200);
        }
    }


    public function asesor()
    {
        $this->load->library('Authorization_Token');
        header("Access-Control-Allow-Origin: *");

         $token = $this->authorization_token->validateToken();
        
        $user_data = $this->_apiConfig([
            'methods' => ['POST'],
            'requireAuthorization' => true,
        ]);

        $asesor      = $this->m->getAsesor()->row_array();
        $madrasah    = $this->m->getAsesorMadrasah($asesor['id'])->row_array();
        $jmlAsesor   = $this->m->getAsesor();
        $jmlMadrasah = $this->m->getAsesorMadrasah($asesor['id']);
        $dokumentasi = $this->db->query("select file from tr_persyaratan where madrasah_id='{$madrasah['id']}'")->result_array();
       
        $dataAsesor  = array(
            "nik" => $asesor['nik'],
            "nama" => $asesor['nama'],            
            "visitasi" => $madrasah,
            "dokumentasi" => $dokumentasi,
            "bukti" => "https://appmadrasah.kemenag.go.id/bkba/api/aman/bukti?nsm={$madrasah['nsm']}&nik={$asesor['nik']}"
            
        );

        $dataAsesorNot  = array(
            "nik" => $asesor['nik'],
            "nama" => $asesor['nama'],            
            "visitasi" => "Belum Visitasi"
            
            
        );


        if($jmlAsesor->num_rows() > 0 && $jmlMadrasah->num_rows() >0){
                $this->api_return(
                    [
                        'code' => 200,
                        'message' => "success",                      
                        "data" => $dataAsesor
                    ],
                200);

        }else if($jmlAsesor->num_rows() > 0 && $jmlMadrasah->num_rows() ==0){

            $this->api_return(
                [
                    'code' => 200,
                    'message' => "success",                      
                    "data" => $dataAsesorNot
                ],
            200);
        
        }else{

            $this->api_return(
                [
                    'code' => 400,
                    'message' => "Data tidak ditemukan"
                    
                ],
            200);
        }
    }


    public function bukti(){

                    $this->load->helper('exportpdf_helper'); 
    
                    $pdf_filename = 'Bukti Visitasi.pdf';	 
                    $data['madrasah'] = $this->db->query("select * from madrasahedm where nsm='{$_GET['nsm']}' and id  IN(select madrasah_id from visitasi_kegiatan)")->row();
                    $data['petugas'] = $this->db->get_where("asesor",array("nik"=>$_GET['nik']))->row();
                    $user_info = $this->load->view('bukti', $data, true);
                
                    
                     $output = $user_info;
                    
                    generate_pdf($output, $pdf_filename);
     }



     public function dnt()
    {
        $this->load->library('Authorization_Token');
        header("Access-Control-Allow-Origin: *");

         $token = $this->authorization_token->validateToken();
        
        $user_data = $this->_apiConfig([
            'methods' => ['POST'],
            'requireAuthorization' => true,
        ]);

        $madrasah = $this->m->getMadrasahDNT()->result_array();
        $jml      = $this->m->getMadrasahDNT()->num_rows();

        if($jml>0){
                $this->api_return(
                    [
                        'code' => 200,
                        'message' => "success",                      
                        "data" => [
                            'jumlah' => $jml,
                            'shortlist' => $madrasah
                        ],
                    ],
                200);
        }else{

            $this->api_return(
                [
                    'code' => 400,
                    'message' => "Data tidak ditemukan"
                    
                ],
            200);
        }
    }
}