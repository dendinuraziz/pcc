<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Web extends CI_Controller {

	function __construct()
	{
		parent::__construct();	
		
		$this->load->model("M_web","mf");
		date_default_timezone_set("Asia/Jakarta");
	}
	
	public function index()
	{
		$data['title']  = "Pendis Care Center (PCC) ";
		$data['konten'] = "page_default";
		$this->load->view("page_header",$data);
		
	}
	public function dasarhukum()
	{
		$data['title']  = "Dasar Hukum | Pendis Care Center (PCC) ";
		$data['konten'] = "page/dasarhukum";
		$this->load->view("page_header",$data);
		
	}

	public function piloting()
	{
		$data['title']  = "Piloting  | Pendis Care Center (PCC) ";
		$data['konten'] = "page/piloting";
		$this->load->view("page_header",$data);
		
	}

	public function sk()
	{
		$tahun = $this->input->get_post("tahun");
		$data['title']  = "Piloting  | Pendis Care Center (PCC) ";
		$data['konten'] = "page/piloting_data";
		$this->load->view("page_header",$data);
		
	}

	public function timeline()
	{
		$data['title']  = "Timeline  | Pendis Care Center (PCC) ";
		$data['konten'] = "page/kalender";
		$this->load->view("page_header",$data);
		
	}

	public function kamus()
	{
		$data['title']  = "Kamus Evidenci  | Pendis Care Center (PCC) ";
		$data['konten'] = "page/kamus";
		$this->load->view("page_header",$data);
		
	}

	public function faq()
	{
		$data['title']  = "Faq  | Pendis Care Center (PCC) ";
		$data['konten'] = "page/faq";
		$this->load->view("page_header",$data);
		
	}

	public function kontak()
	{
		$data['title']  = "Kontak Center  | Pendis Care Center (PCC) ";
		$data['konten'] = "page/kontak";
		$this->load->view("page_header",$data);
		
	}

	public function sign()
	{
		$data['title']  = "Pendis Care Center (PCC)  - Silahkan Masuk";
		$data['konten'] = "sign";
		$this->load->view("page_header",$data);
		
	}

	public function notdendi()
	{
		$data['title']  = "Pendis Care Center (PCC)  - Silahkan Masuk";
		$data['konten'] = "page_default";
		$this->load->view("page_header",$data);
		
	}

	public function bot(){

		$message = strtoupper($this->input->get_post("messageValue"));
		$data = $this->db->query("SELECT * FROM chatbot WHERE UPPER(messages) LIKE '%$message%'")->row();
		  if(!is_null($data)){
			echo $data->response;
		  }else{
			echo "Maaf, kami tidak mengerti yang Anda maksud, apakah Anda ingin terhubung dengan live Agent kami ?";

		  }
	}
   
	 
	
}
