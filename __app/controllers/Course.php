<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Course extends CI_Controller {

	function __construct()
	{
		parent::__construct();	
		
		$this->load->model("M_web","mf");
		date_default_timezone_set("Asia/Jakarta");
	}
	

	public function data($slug)
	{
		$data['data']	= $this->db->get_where("v_pelatihan",array("slug_kategori"=>$slug))->row();
		$data['kategori']	= $this->db->get_where("kategori",array("slug"=>$slug))->row();
		$data['title']  = "Course - ".$data['kategori']->nama;
		$data['konten'] = "page/course";
		$this->load->view("page_header",$data);
		
	}

	public function search()
	{
		
		$q = $this->security->xss_clean(($this->db->escape_str($this->input->get("q"))));
		$data['data']	= $this->db->query("SELECT * from v_pelatihan where UPPER(nama) LIKE '%".strtoupper($q)."%'")->result();
		$data['title']  = "Course - ".$q;
		$data['konten'] = "page/coursedata";
		$this->load->view("page_header",$data);
		
	}


	public function modul($slug)
	{
		$data['data']	= $this->db->get_where("v_pelatihan",array("slug_kategori"=>$slug))->row();
		$data['kategori']	= $this->db->get_where("modul",array("slug"=>$slug))->row();
		$data['title']  = "Course - ".$data['kategori']->nama;
		$data['konten'] = "page/coursemodul";
		$this->load->view("page_header",$data);
		
	}
   
	 
	
}
