<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>__statics/js/slider/css/style.css" media="screen" />

    <!-- THE PREVIEW STYLE SHEETS, NO NEED TO LOAD IN YOUR DOM -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>__statics/js/slider/css/navstylechange.css" media="screen" />
    


    <!-- SLIDER REVOLUTION 4.x SCRIPTS  -->
    <script type="text/javascript" src="<?php echo base_url(); ?>__statics/js/slider/rs-plugin/js/jquery.themepunch.plugins.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>__statics/js/slider/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>

	<!-- SLIDER REVOLUTION 4.x CSS SETTINGS -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>__statics/js/slider/rs-plugin/css/settings.css" media="screen" />


    


	
    <div class="tp-banner-container" >
		<div class="tp-banner" style="z-index:-1000000000000">
			<ul>	<!-- SLIDE  -->
			
				

              
				<li data-transition="slidedown" data-slotamount="7" data-masterspeed="1000" >
					<!-- MAIN IMAGE -->
					<img src="<?php echo base_url(); ?>__statics/js/slider/images/dummy.png" data-lazyload="<?php echo base_url(); ?>__statics/js/slider/images/transparent.png" style='background-color:#b2c4cc' alt=""  data-bgfit="cover" data-bgposition="left top" data-bgrepeat="no-repeat">
					<!-- LAYERS -->

					<!-- LAYER NR. 1 -->
					<div class="tp-caption customin skewtoleft"
						data-x="877"
						data-y="54"
						data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
						data-speed="600"
						data-start="1500"
						data-easing="Back.easeOut"
						data-endspeed="400"
						data-endeasing="Back.easeIn"
						style="z-index: 2"><img src="<?php echo base_url(); ?>__statics/js/slider/images/cloud2.png" alt="">
					</div>

					<!-- LAYER NR. 2 -->
					<div class="tp-caption customin skewtoleft"
						data-x="84"
						data-y="80"
						data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
						data-speed="600"
						data-start="1300"
						data-easing="Back.easeOut"
						data-endspeed="400"
						data-endeasing="Back.easeIn"
						style="z-index: 3"><img src="<?php echo base_url(); ?>__statics/js/slider/images/cloud3.png" alt="">
					</div>

					<!-- LAYER NR. 3 -->
					<div class="tp-caption customin skewtoleft"
						data-x="473"
						data-y="123"
						data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
						data-speed="600"
						data-start="1700"
						data-easing="Back.easeOut"
						data-endspeed="400"
						data-endeasing="Back.easeIn"
						style="z-index: 4"><img src="<?php echo base_url(); ?>__statics/js/slider/images/cloud1.png" alt="">
					</div>

					<!-- LAYER NR. 4 -->
					<div class="tp-caption sfb ltl"
						data-x="639"
						data-y="346"
						data-speed="600"
						data-start="1200"
						data-easing="Back.easeOut"
						data-endspeed="400"
						data-endeasing="Back.easeIn"
						data-captionhidden="on"
						style="z-index: 5"><img src="<?php echo base_url(); ?>__statics/js/slider/images/hill3.png" alt="">
					</div>

					<!-- LAYER NR. 5 -->
					<div class="tp-caption sfb ltl"
						data-x="228"
						data-y="360"
						data-speed="600"
						data-start="1100"
						data-easing="Back.easeOut"
						data-endspeed="400"
						data-endeasing="Back.easeIn"
						data-captionhidden="on"
						style="z-index: 6"><img src="<?php echo base_url(); ?>__statics/js/slider/images/hill4.png" alt="">
					</div>

					<!-- LAYER NR. 6 -->
					<div class="tp-caption grassfloor lfb ltb"
						data-x="center" data-hoffset="0"
						data-y="bottom" data-voffset="50"
						data-speed="600"
						data-start="500"
						data-easing="Back.easeOut"
						data-endspeed="600"
						data-endeasing="Power4.easeIn"
						data-captionhidden="on"
						style="z-index: 7">
					</div>

					<!-- LAYER NR. 7 -->
					<div class="tp-caption sfb ltl"
						data-x="142"
						data-y="375"
						data-speed="600"
						data-start="800"
						data-easing="Back.easeOut"
						data-endspeed="400"
						data-endeasing="Back.easeIn"
						data-captionhidden="on"
						style="z-index: 8"><img src="<?php echo base_url(); ?>__statics/js/slider/images/hill2.png" alt="">
					</div>

					<!-- LAYER NR. 8 -->
					<div class="tp-caption sfb ltl"
						data-x="496"
						data-y="367"
						data-speed="600"
						data-start="900"
						data-easing="Back.easeOut"
						data-endspeed="400"
						data-endeasing="Back.easeIn"
						data-captionhidden="on"
						style="z-index: 9"><img src="<?php echo base_url(); ?>__statics/js/slider/images/hill1.png" alt="">
					</div>

					<!-- LAYER NR. 9 -->
					<div class="tp-caption sfb ltl"
						data-x="918"
						data-y="379"
						data-speed="600"
						data-start="1000"
						data-easing="Back.easeOut"
						data-endspeed="400"
						data-endeasing="Back.easeIn"
						data-captionhidden="on"
						style="z-index: 10"><img src="<?php echo base_url(); ?>__statics/js/slider/images/hill2.png" alt="">
					</div>

					<!-- LAYER NR. 10 -->
					<div class="tp-caption lfb skewtoleft"
						data-x="-20"
						data-y="10"
						data-speed="2000"
						data-start="1300"
						data-easing="Power4.easeOut"
						data-endspeed="400"
						data-endeasing="Power1.easeIn"
						style="z-index: 11"><img src="<?php echo base_url(); ?>__statics/js/slider/images/pogramming.svg" width="600px" >
					</div>

				

					<!-- LAYER NR. 12 -->
					

					<!-- LAYER NR. 13 -->
					<div class="tp-caption large_bold_white customin ltl"
						data-x="596"
						data-y="101"
						data-customin="x:0;y:0;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
						data-speed="1000"
						data-start="1850"
						data-easing="Back.easeInOut"
						data-endspeed="400"
						data-endeasing="Back.easeIn"
						style="z-index: 14">Pendis Care Center (PCC) 
					</div>

					
					<!-- LAYER NR. 16 -->
					<div class="tp-caption mediumlarge_light_white customin ltl"
						data-x="600"
						data-y="180"
						data-customin="x:0;y:0;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
						data-speed="1000"
						data-start="2050"
						data-easing="Back.easeInOut"
						data-endspeed="400"
						data-endeasing="Back.easeIn"
						style="z-index: 17">Pembangunan Zona Integritas (ZI)
					</div>
                    <div class="tp-caption  customin ltl tp-caption medium_bg_red skewfromright "
						data-x="600"
						data-y="240"
						data-customin="x:0;y:0;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
						data-speed="1000"
						data-start="2050"
						data-easing="Back.easeInOut"
						data-endspeed="400"
						data-endeasing="Back.easeIn"
						style="z-index: 17">SIGAP-ZI (Sistem informasi gabungan Zona Integritas)
					</div>
				</li>

                <li data-transition="zoomout" data-slotamount="7" data-masterspeed="1000" >
					<!-- MAIN IMAGE -->
					<img src="<?php echo base_url(); ?>__statics/js/slider/images/dummy.png" data-lazyload="<?php echo base_url(); ?>__statics/js/slider/images/darkblurbg.jpg"  alt="darkblurbg"  data-bgfit="cover" data-bgposition="left top" data-bgrepeat="no-repeat">
					<!-- LAYERS -->

					<!-- LAYER NR. 1 -->
					<div class="tp-caption customin"
						data-x="474"
						data-y="189"
						data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
						data-speed="500"
						data-start="800"
						data-easing="Power3.easeInOut"
						data-endspeed="300"
						style="z-index: 2"><img src="<?php echo base_url(); ?>__statics/js/slider/images/macbook2.png" alt="">
					</div>

					<!-- LAYER NR. 2 -->
					<div class="tp-caption customin"
						data-x="245"
						data-y="92"
						data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
						data-speed="500"
						data-start="500"
						data-easing="Power3.easeInOut"
						data-endspeed="300"
						style="z-index: 3"><img src="<?php echo base_url(); ?>__statics/js/slider/images/imac1.png" alt="">
					</div>

					<!-- LAYER NR. 3 -->
					<div class="tp-caption customin"
						data-x="693"
						data-y="69"
						data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
						data-speed="500"
						data-start="1300"
						data-easing="Power3.easeInOut"
						data-endspeed="300"
						style="z-index: 4"><img src="<?php echo base_url(); ?>__statics/js/slider/images/lupe_macbook.png" alt="">
					</div>

					<!-- LAYER NR. 4 -->
					<div class="tp-caption customin"
						data-x="100"
						data-y="171"
						data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
						data-speed="500"
						data-start="1400"
						data-easing="Power3.easeInOut"
						data-endspeed="300"
						style="z-index: 5"><img src="<?php echo base_url(); ?>__statics/js/slider/images/lupe_imac.png" alt="">
					</div>

					<!-- LAYER NR. 5 -->
					<div class="tp-caption medium_bg_asbestos skewfromleft customout"
						data-x="104"
						data-y="154"
						data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
						data-speed="800"
						data-start="1500"
						data-easing="Power4.easeOut"
						data-endspeed="300"
						data-endeasing="Power1.easeIn"
						data-captionhidden="on"
						style="z-index: 6">Kontak Center (Robot dan pegawai)
					</div>

					<!-- LAYER NR. 6 -->
					<div class="tp-caption medium_bg_red skewfromright customout"
						data-x="820"
						data-y="274"
						data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
						data-speed="800"
						data-start="1700"
						data-easing="Power4.easeOut"
						data-endspeed="300"
						data-endeasing="Power1.easeIn"
						data-captionhidden="on"
						style="z-index: 7">Frequently Asked Question
					</div>

					<!-- LAYER NR. 7 -->
					<div class="tp-caption medium_bg_orange skewfromright customout"
						data-x="820"
						data-y="314"
						data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
						data-speed="800"
						data-start="1800"
						data-easing="Power4.easeOut"
						data-endspeed="300"
						data-endeasing="Power1.easeIn"
						data-captionhidden="on"
						style="z-index: 8">Time Line Pembangunan ZI
					</div>

					<!-- LAYER NR. 8 -->
					<div class="tp-caption medium_bg_darkblue skewfromleft customout"
						data-x="168"
						data-y="193"
						data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
						data-speed="800"
						data-start="1600"
						data-easing="Power4.easeOut"
						data-endspeed="300"
						data-endeasing="Power1.easeIn"
						data-captionhidden="on"
						style="z-index: 9">Daftar Pilot Project ZI (2020-2023)
					</div>

					<!-- LAYER NR. 9 -->
					<div class="tp-caption large_bold_white customin customout"
						data-x="428"
						data-y="34"
						data-customin="x:0;y:0;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
						data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
						data-speed="600"
						data-start="1100"
						data-easing="Back.easeOut"
						data-endspeed="300"
						data-endeasing="Power1.easeIn"
						style="z-index: 10">Pendis Care Center (PCC) 
					</div>

				
				</li>
				<!-- SLIDE  -->
				
				<!-- SLIDE  -->
		
			</ul>
			<div class="tp-bannertimer"></div>
		</div>
</div>

   

   <div class="container mauMobilGa" >
        <div class="intro-area-2" >
            <div class="row justify-content-center">
                <div class="col-lg-12">
                    <div class="intro-slider owl-carousel">

					  <?php 
					   $modul = $this->db->get("modul")->result();
					     foreach($modul as $rm){
					  ?>
                      
                        <div class="item">
                            <div class="single-intro-wrap">
                                <div class="thumb">
                                <a href="<?php echo $rm->slug; ?>"><img src="<?php echo base_url(); ?>__statics/img/intro/<?php echo $rm->cover; ?>" alt="img"></a>
                                </div>
                                <div class="wrap-details">
                                    <h6><a href="<?php echo $rm->slug; ?>"><?php echo $rm->nama; ?></a></h6>
                                   
                                </div>
                            </div>
                        </div>
					  <?php 
						 }
						?>
                        
                    </div>
                </div>
            </div>
        </div>            
    </div>



	<div class="potential-area-2 pt-2  ">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 align-self-center">
                    <div class="thumb mb-lg-0 mb-4 me-xl-5 me-lg-3 me-0">
                        <img src="<?php echo base_url(); ?>__statics/img/logo-zi.png" alt="img">
                    </div>
                </div>
                <div class="col-lg-6 align-self-center">
                    <div class="section-title mb-0">
                        <h2>Tentang Pendis Care Center</h2>
                        <p>Pembangunan Zona Integritas (ZI) merupakan ikhtiar untuk meningkatkan kualitas pelayanan publik. Direktorat Jenderal Pendidikan Islam telah menetapkan sejumlah Perguruan Tinggi Keagamaan Islam Negeri (PTKIN) dan Madrasah sebagai pilot project Pembangunan ZI menuju wilayah Bebas dari Korupsi (WBK) dan Wilayah Birokrasi Bersih dan Melayani (WBBM).
							<br>Pembangunan ZI pada PTKIN dan Madrasah cenderung berjalan lambat. Hal ini terlihat dari tingkat capaian predikat ZI-WBK dan WBBM. Dari tahun 2017 sampai 2023, hanya terdapat 5 (lima) satuan kerja yang berhasil mendapatkan predikat ZI-WBK dari Kementerian PANRB, yaitu UIN Antasari Banjarmasin (2017), IAIN Salatiga (2020), MAN IC Serpong (2021), MAN Karangasem (2022), dan MAN 2 Kudus (2022). Rendahnya tingkat capaian pembangunan ZI pada PTKIN dan Madrasah disebabkan kurang optimalnya pelaksanaan bimbingan teknis yang dilakukan oleh Direktorat Jenderal Pendidikan Islam. Dalam rangka optimalisasi fasilitasi pembangunan ZI pada PTKIN dan Madrasah, Direktorat Jenderal Pendidikan Islam membuat Pendis Care Center (PCC).
							<br>PCC merupakan sebuah platform yang berisi berbagai informasi terkait pembangunan ZI. Melalui PCC, diharapkan dapat memudahan PTKIN dan Madrasah pilot project ZI untuk membangun ZI-WBK/WBBM.</p>
						   <br>
						 
                       
                    </div>
                </div>                
            </div>
        </div>            
    </div>


    <!-- trending courses Area Start-->
    <section class="trending-courses-area " id="tutorial">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-title">
                        <h2>Tutorial  </h2>
                    </div>
                </div>
                <div class="col-lg-12">
                    <ul class="edl-nav nav nav-pills">
                        <li class="nav-item">
                            <button class="nav-link active" id="pills-0-tab" data-bs-toggle="pill" data-bs-target="#pills-0">Semua Kategori </button>
                        </li>
						<?php 
						 $kat = $this->db->get("kategori")->result();
						   foreach($kat as $row){
							?>
							
							<li class="nav-item">
								<button class="nav-link" id="pills-<?php echo $row->id; ?>-tab" data-bs-toggle="pill" data-bs-target="#pills-<?php echo $row->id; ?>"><?php echo $row->nama; ?> </button>
							</li>
							<?php
						   }
						 ?>

					
                    </ul>
                    <div class="tab-content">
					<?php 
						 $kat = $this->db->get("kategori")->result();
						   foreach($kat as $rkat){
							?>

                        <div class="tab-pane fade show <?php echo ($rkat->id==1) ? "active":""; ?>" id="pills-<?php echo $rkat->id; ?>">
                            <div class="course-slider owl-carousel">

							<?php 
								$kat = $this->db->query("select * from pelatihan where kategori_id='{$rkat->id}' order by nonaktif desc")->result();
                                $cat = array("0"=>"red","1"=>"green");
                                $status = array("0"=>"Video","1"=>"Dokumen");
								foreach($kat as $row){
									if($rkat->id==1){
										$cover = explode("v=",$row->link);
										$cover ="https://img.youtube.com/vi/".$cover[1]."/hqdefault.jpg";

									}else{

										$cover = base_url()."__statics/img/logo-zi.png";
									}
							?>
							
                                <div class="item">
                                    <div class="single-course-wrap">
										
                                        <div class="thumb">
										
                                            <a href="<?php echo site_url('pelatihan/detail/'.$row->slug); ?>" class="cat cat-<?php echo $cat[$row->nonaktif]; ?>"><?php echo $this->Reff->get_kondisi(array("id"=>$row->kategori_id),"kategori","nama"); ?></a>
                                           
                                            <img src="<?php echo $cover; ?>" alt="img">
											
                                        </div>
										
                                        <div class="wrap-details">
                                            <h6><a href="<?php echo site_url('pelatihan/detail/'.$row->slug); ?>"><?php echo $row->nama; ?></a></h6>
                                            <div class="user-area">
                                                <div class="user-details">
                                                    <img src="<?php echo base_url(); ?>__statics/img/author/1.png" alt="img">
                                                    <a href="<?php echo site_url('pelatihan/detail/'.$row->slug); ?>">Pendis Care Center </a>
                                                </div>
                                                
                                            </div>
                                            <div class="price-wrap">
                                                <div class="row align-items-center">
                                                    <div class="col-12">
                                                        <a href="<?php echo site_url('pelatihan/detail/'.$row->slug); ?>"><?php echo $this->Reff->get_kondisi(array("id"=>$row->kategori_id),"kategori","nama"); ?></a>
                                                    </div>
                                                   
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

								<?php 
								}
								?>

								


                               


                            </div>
                        </div>
                       <?php 
						   }
						   ?>
                       
                       
                       
                        
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- trending courses Area End -->

	<div class="text-center pd-top-135">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-8">
                    <div class="section-title">
                        <h2>Pendis Care Center (PCC) </h2>
                        <p>Menggunakan Formula 4P  (Product, Price, Place, Promotion)</p>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-lg-4 col-sm-6">
                    <div class="single-intro-wrap-2">
                        <div class="thumb">
                            <img src="<?php echo base_url(); ?>__statics/img/intro/03.png" alt="img">
                        </div>
                        <div class="wrap-details">
                            <h4><a href="#">Fasilitas </a></h4>
                            <p>PCC fasilitasi dan konsultasi pembangunan ZI yang lebih berkualitas, intens, efektif,  dan efisien  </p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6">
                    <div class="single-intro-wrap-2">
                        <div class="thumb">
                            <img src="<?php echo base_url(); ?>__statics/img/intro/brain.png" alt="img">
                        </div>
                        <div class="wrap-details">
                            <h4><a href="#">Kemudahan </a></h4>
                            <p>Stakeholder dapat mengakses PCC kapan dan dimana saja, sesuai kebutuhan.</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6">
                    <div class="single-intro-wrap-2">
                        <div class="thumb">
                            <img src="<?php echo base_url(); ?>__statics/img/intro/02.png" alt="img">
                        </div>
                        <div class="wrap-details">
                            <h4><a href="#">Price</a></h4>
                            <p>Diperolehnya kemudahan, efektivitas, efisiensi waktu dan penghematan biaya dalam pendampingan pembangunan ZI </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>            
    </div>



    <section class="trending-courses-area " id="tips">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-title">
                        <h2>Tips dan Trik </h2>
                    </div>
                </div>
                <div class="col-lg-12">
                    <ul class="edl-nav nav nav-pills">
                       
						

					
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane fade show active" id="pills-0">
                            <div class="course-slider owl-carousel">
							<?php 
                                        $video = $this->db->query('SELECT * FROM tips ')->result();
                                        foreach($video as $r){
                                            $cover = explode("v=",$r->link);
                                            $cover ="https://img.youtube.com/vi/".$cover[1]."/maxresdefault.jpg";
                                        ?>
							
                                <div class="item">
                                    <div class="single-course-wrap">
                                        <div class="thumb">
                                            <a href="#" class="cat cat-blue">Tips & Trik</a>
                                            <img src="<?php echo $cover; ?>" alt="img">
                                        </div>
                                        <div class="wrap-details">
                                            <h6><a href="<?php echo str_replace("watch?v=","embed/",$r->link);; ?>" class="play-btn"><?php echo $r->nama; ?></a></h6>
                                            <div class="user-area">
                                                <div class="user-details">
                                                    <img src="<?php echo base_url(); ?>__statics/img/author/1.png" alt="img">
                                                    <a href="#">Pendis Care Center(PCC) </a>
                                                </div>
                                                
                                            </div>
                                            <div class="price-wrap">
                                                <div class="row align-items-center">
                                                    <div class="col-6">
                                                        <a href="#">Video </a>
                                                    </div>
                                                  
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

								<?php 
										}
										?>

								


                               


                            </div>
                        </div>
                       
                       
                       
                       
                        
                    </div>
                </div>
            </div>
        </div>
    </section>




    <!-- testimonial courses Area Start-->
    <section class="testimonial-courses-area pd-bottom-150">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-title">
                        <h2> Testimoni  </h2>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="testimonial-slider owl-carousel">

					<?php 
					  $testimoni = $this->db->query("select * from testimoni order by tanggal DESC")->result();
					    foreach($testimoni as $rowtes){
					?>
                        <div class="item">
                            <div class="single-testimonial-wrap">
                                <div class="thumb">
                                    <span><b>MAN Insan Cendekia Serpong</b></span>
                                </div>
                                <div class="wrap-details">
									
													<div class="user-rating">
														<?php 
														  for($a=1;$a <6;$a++){
															$style="";
															  if($a<=$rowtes->rate){
																  $style="color: #d6c839 ";
															  }
															?>
																<i class="fa fa-star" style="<?php echo $style; ?>"></i>
														<?php 
														  }
														  ?>
													
														
														
													</div>
                                    <p><?php echo $rowtes->testimoni; ?></p>
                                    <span><?php echo $this->Reff->get_kondisi(array("id"=>$rowtes->peserta_id),"peserta","nama"); ?> <br> <?php echo $this->Reff->formattimestamp($rowtes->tanggal); ?> </span>
                                   
                                </div>
                            </div>
                        </div>
					<?php 
						}
					?>
                       
                        
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- testimonial courses Area End -->

   
    <!-- client Area End -->

    <!-- about Area Start-->
   <!--  <section class="about-area " id="login">
        <div class="container">
            <div class="row justify-content-center">
              
                <div class="col-lg-12">
                    <div class="single-about-wrap">
                        <div class="thumb">
                            <img src="<?php echo base_url(); ?>__statics/img/intro/brain.png" alt="img">
                        </div>
                        <div class="wrap-details">
                            <h3><a href="#"> Masuk menggunakan SSO EMIS </a></h3>
                            <p>Pendis Care Center (PCC) terintegrasi dengan Education Management Information System (EMIS) </p>
                            <a class="btn btn-base-light-border" href="<?= site_url('login/oauth_simpatika') ?>">Login SSO Madrasah </a>
                            <a class="btn btn-base-light-border" href="<?= site_url('login/oauth_simpatika') ?>">Login SSO PTKIN </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section> -->