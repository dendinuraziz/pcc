<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>__statics/js/slider/css/style.css" media="screen" />

    <!-- THE PREVIEW STYLE SHEETS, NO NEED TO LOAD IN YOUR DOM -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>__statics/js/slider/css/navstylechange.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>__statics/js/slider/css/noneed.css" media="screen" />


    <!-- SLIDER REVOLUTION 4.x SCRIPTS  -->
    <script type="text/javascript" src="<?php echo base_url(); ?>__statics/js/slider/rs-plugin/js/jquery.themepunch.plugins.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>__statics/js/slider/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>

	<!-- SLIDER REVOLUTION 4.x CSS SETTINGS -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>__statics/js/slider/rs-plugin/css/settings.css" media="screen" />


    




	
    <section class="tp-banner-container" >
		<div class="tp-banner" style="z-index:-1000000000000">
			<ul>	<!-- SLIDE  -->
			
				

              
				<li data-transition="slidedown" data-slotamount="7" data-masterspeed="1000" >
					<!-- MAIN IMAGE -->
					<img src="<?php echo base_url(); ?>__statics/js/slider/images/dummy.png" data-lazyload="<?php echo base_url(); ?>__statics/js/slider/images/transparent.png" style='background-color:#b2c4cc' alt=""  data-bgfit="cover" data-bgposition="left top" data-bgrepeat="no-repeat">
					<!-- LAYERS -->

					<!-- LAYER NR. 1 -->
					<div class="tp-caption customin skewtoleft"
						data-x="877"
						data-y="54"
						data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
						data-speed="600"
						data-start="1500"
						data-easing="Back.easeOut"
						data-endspeed="400"
						data-endeasing="Back.easeIn"
						style="z-index: 2"><img src="<?php echo base_url(); ?>__statics/js/slider/images/cloud2.png" alt="">
					</div>

					<!-- LAYER NR. 2 -->
					<div class="tp-caption customin skewtoleft"
						data-x="84"
						data-y="80"
						data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
						data-speed="600"
						data-start="1300"
						data-easing="Back.easeOut"
						data-endspeed="400"
						data-endeasing="Back.easeIn"
						style="z-index: 3"><img src="<?php echo base_url(); ?>__statics/js/slider/images/cloud3.png" alt="">
					</div>

					<!-- LAYER NR. 3 -->
					<div class="tp-caption customin skewtoleft"
						data-x="473"
						data-y="123"
						data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
						data-speed="600"
						data-start="1700"
						data-easing="Back.easeOut"
						data-endspeed="400"
						data-endeasing="Back.easeIn"
						style="z-index: 4"><img src="<?php echo base_url(); ?>__statics/js/slider/images/cloud1.png" alt="">
					</div>

					<!-- LAYER NR. 4 -->
					<div class="tp-caption sfb ltl"
						data-x="639"
						data-y="346"
						data-speed="600"
						data-start="1200"
						data-easing="Back.easeOut"
						data-endspeed="400"
						data-endeasing="Back.easeIn"
						data-captionhidden="on"
						style="z-index: 5"><img src="<?php echo base_url(); ?>__statics/js/slider/images/hill3.png" alt="">
					</div>

					<!-- LAYER NR. 5 -->
					<div class="tp-caption sfb ltl"
						data-x="228"
						data-y="360"
						data-speed="600"
						data-start="1100"
						data-easing="Back.easeOut"
						data-endspeed="400"
						data-endeasing="Back.easeIn"
						data-captionhidden="on"
						style="z-index: 6"><img src="<?php echo base_url(); ?>__statics/js/slider/images/hill4.png" alt="">
					</div>

					<!-- LAYER NR. 6 -->
					<div class="tp-caption grassfloor lfb ltb"
						data-x="center" data-hoffset="0"
						data-y="bottom" data-voffset="50"
						data-speed="600"
						data-start="500"
						data-easing="Back.easeOut"
						data-endspeed="600"
						data-endeasing="Power4.easeIn"
						data-captionhidden="on"
						style="z-index: 7">
					</div>

					<!-- LAYER NR. 7 -->
					<div class="tp-caption sfb ltl"
						data-x="142"
						data-y="375"
						data-speed="600"
						data-start="800"
						data-easing="Back.easeOut"
						data-endspeed="400"
						data-endeasing="Back.easeIn"
						data-captionhidden="on"
						style="z-index: 8"><img src="<?php echo base_url(); ?>__statics/js/slider/images/hill2.png" alt="">
					</div>

					<!-- LAYER NR. 8 -->
					<div class="tp-caption sfb ltl"
						data-x="496"
						data-y="367"
						data-speed="600"
						data-start="900"
						data-easing="Back.easeOut"
						data-endspeed="400"
						data-endeasing="Back.easeIn"
						data-captionhidden="on"
						style="z-index: 9"><img src="<?php echo base_url(); ?>__statics/js/slider/images/hill1.png" alt="">
					</div>

					<!-- LAYER NR. 9 -->
					<div class="tp-caption sfb ltl"
						data-x="918"
						data-y="379"
						data-speed="600"
						data-start="1000"
						data-easing="Back.easeOut"
						data-endspeed="400"
						data-endeasing="Back.easeIn"
						data-captionhidden="on"
						style="z-index: 10"><img src="<?php echo base_url(); ?>__statics/js/slider/images/hill2.png" alt="">
					</div>

					<!-- LAYER NR. 10 -->
					<div class="tp-caption lfb skewtoleft"
						data-x="-20"
						data-y="10"
						data-speed="2000"
						data-start="1300"
						data-easing="Power4.easeOut"
						data-endspeed="400"
						data-endeasing="Power1.easeIn"
						style="z-index: 11"><img src="<?php echo base_url(); ?>__statics/js/slider/images/pogramming.svg" width="600px" >
					</div>

				

					<!-- LAYER NR. 12 -->
					

					<!-- LAYER NR. 13 -->
					<div class="tp-caption large_bold_white customin ltl"
						data-x="596"
						data-y="101"
						data-customin="x:0;y:0;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
						data-speed="1000"
						data-start="1850"
						data-easing="Back.easeInOut"
						data-endspeed="400"
						data-endeasing="Back.easeIn"
						style="z-index: 14">Pelatihan  Online GTK 
					</div>

					
					<!-- LAYER NR. 16 -->
					<div class="tp-caption mediumlarge_light_white customin ltl"
						data-x="600"
						data-y="180"
						data-customin="x:0;y:0;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
						data-speed="1000"
						data-start="2050"
						data-easing="Back.easeInOut"
						data-endspeed="400"
						data-endeasing="Back.easeIn"
						style="z-index: 17">Dapatkan pengalaman menarik dan
					</div>
                    <div class="tp-caption  customin ltl tp-caption medium_bg_red skewfromright "
						data-x="600"
						data-y="240"
						data-customin="x:0;y:0;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
						data-speed="1000"
						data-start="2050"
						data-easing="Back.easeInOut"
						data-endspeed="400"
						data-endeasing="Back.easeIn"
						style="z-index: 17">Sertifikat gratis
					</div>
				</li>

                <li data-transition="zoomout" data-slotamount="7" data-masterspeed="1000" >
					<!-- MAIN IMAGE -->
					<img src="<?php echo base_url(); ?>__statics/js/slider/images/dummy.png" data-lazyload="<?php echo base_url(); ?>__statics/js/slider/images/darkblurbg.jpg"  alt="darkblurbg"  data-bgfit="cover" data-bgposition="left top" data-bgrepeat="no-repeat">
					<!-- LAYERS -->

					<!-- LAYER NR. 1 -->
					<div class="tp-caption customin"
						data-x="474"
						data-y="189"
						data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
						data-speed="500"
						data-start="800"
						data-easing="Power3.easeInOut"
						data-endspeed="300"
						style="z-index: 2"><img src="<?php echo base_url(); ?>__statics/js/slider/images/macbook2.png" alt="">
					</div>

					<!-- LAYER NR. 2 -->
					<div class="tp-caption customin"
						data-x="245"
						data-y="92"
						data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
						data-speed="500"
						data-start="500"
						data-easing="Power3.easeInOut"
						data-endspeed="300"
						style="z-index: 3"><img src="<?php echo base_url(); ?>__statics/js/slider/images/imac1.png" alt="">
					</div>

					<!-- LAYER NR. 3 -->
					<div class="tp-caption customin"
						data-x="693"
						data-y="69"
						data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
						data-speed="500"
						data-start="1300"
						data-easing="Power3.easeInOut"
						data-endspeed="300"
						style="z-index: 4"><img src="<?php echo base_url(); ?>__statics/js/slider/images/lupe_macbook.png" alt="">
					</div>

					<!-- LAYER NR. 4 -->
					<div class="tp-caption customin"
						data-x="100"
						data-y="171"
						data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
						data-speed="500"
						data-start="1400"
						data-easing="Power3.easeInOut"
						data-endspeed="300"
						style="z-index: 5"><img src="<?php echo base_url(); ?>__statics/js/slider/images/lupe_imac.png" alt="">
					</div>

					<!-- LAYER NR. 5 -->
					<div class="tp-caption medium_bg_asbestos skewfromleft customout"
						data-x="104"
						data-y="154"
						data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
						data-speed="800"
						data-start="1500"
						data-easing="Power4.easeOut"
						data-endspeed="300"
						data-endeasing="Power1.easeIn"
						data-captionhidden="on"
						style="z-index: 6">Sertifikat Gratis
					</div>

					<!-- LAYER NR. 6 -->
					<div class="tp-caption medium_bg_red skewfromright customout"
						data-x="820"
						data-y="274"
						data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
						data-speed="800"
						data-start="1700"
						data-easing="Power4.easeOut"
						data-endspeed="300"
						data-endeasing="Power1.easeIn"
						data-captionhidden="on"
						style="z-index: 7">Pengalaman Mengajar
					</div>

					<!-- LAYER NR. 7 -->
					<div class="tp-caption medium_bg_orange skewfromright customout"
						data-x="820"
						data-y="314"
						data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
						data-speed="800"
						data-start="1800"
						data-easing="Power4.easeOut"
						data-endspeed="300"
						data-endeasing="Power1.easeIn"
						data-captionhidden="on"
						style="z-index: 8">yang tidak dapat dilupakan
					</div>

					<!-- LAYER NR. 8 -->
					<div class="tp-caption medium_bg_darkblue skewfromleft customout"
						data-x="168"
						data-y="193"
						data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
						data-speed="800"
						data-start="1600"
						data-easing="Power4.easeOut"
						data-endspeed="300"
						data-endeasing="Power1.easeIn"
						data-captionhidden="on"
						style="z-index: 9">Setelah mengikuti pelatihan secara daring
					</div>

					<!-- LAYER NR. 9 -->
					<div class="tp-caption large_bold_white customin customout"
						data-x="428"
						data-y="34"
						data-customin="x:0;y:0;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
						data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
						data-speed="600"
						data-start="1100"
						data-easing="Back.easeOut"
						data-endspeed="300"
						data-endeasing="Power1.easeIn"
						style="z-index: 10">GTK Madrasah Berbagi
					</div>

				
				</li>
				<!-- SLIDE  -->
				<li data-transition="slideleft" data-slotamount="7" data-masterspeed="2000" >
					<!-- MAIN IMAGE -->
					<img src="<?php echo base_url(); ?>__statics/js/slider/images/dummy.png" data-lazyload="<?php echo base_url(); ?>__statics/js/slider/images/transparent.png" style='background-color:#b2c4cc' alt=""  data-bgfit="cover" data-bgposition="left top" data-bgrepeat="no-repeat">
					<!-- LAYERS -->

					<!-- LAYER NR. 1 -->
					<div class="tp-caption lfr"
						data-x="889"
						data-y="118"
						data-speed="600"
						data-start="1000"
						data-easing="Back.easeOut"
						data-endspeed="300"
						style="z-index: 2"><img src="<?php echo base_url(); ?>__statics/js/slider/images/cloud2.png" alt="">
					</div>

					<!-- LAYER NR. 2 -->
					<div class="tp-caption lfr"
						data-x="339"
						data-y="67"
						data-speed="600"
						data-start="1100"
						data-easing="Back.easeOut"
						data-endspeed="300"
						style="z-index: 3"><img src="<?php echo base_url(); ?>__statics/js/slider/images/cloud3.png" alt="">
					</div>

					<!-- LAYER NR. 3 -->
					<div class="tp-caption lfr"
						data-x="12"
						data-y="181"
						data-speed="600"
						data-start="1200"
						data-easing="Back.easeOut"
						data-endspeed="300"
						style="z-index: 4"><img src="<?php echo base_url(); ?>__statics/js/slider/images/cloud1.png" alt="">
					</div>

					<!-- LAYER NR. 4 -->
					<div class="tp-caption lfr"
						data-x="120"
						data-y="352"
						data-speed="600"
						data-start="900"
						data-easing="Back.easeOut"
						data-endspeed="300"
						data-captionhidden="on"
						style="z-index: 5"><img src="<?php echo base_url(); ?>__statics/js/slider/images/hill3.png" alt="">
					</div>

					<!-- LAYER NR. 5 -->
					<div class="tp-caption lfr"
						data-x="696"
						data-y="377"
						data-speed="600"
						data-start="800"
						data-easing="Back.easeOut"
						data-endspeed="300"
						data-captionhidden="on"
						style="z-index: 6"><img src="<?php echo base_url(); ?>__statics/js/slider/images/hill4.png" alt="">
					</div>

					<!-- LAYER NR. 6 -->
					<div class="tp-caption grassfloor lfb ltr"
						data-x="center" data-hoffset="0"
						data-y="bottom" data-voffset="50"
						data-speed="600"
						data-start="0"
						data-easing="Power4.easeOut"
						data-endspeed="600"
						data-endeasing="Power4.easeIn"
						data-captionhidden="on"
						style="z-index: 7">
					</div>

					<!-- LAYER NR. 7 -->
					<div class="tp-caption lfr"
						data-x="464"
						data-y="382"
						data-speed="600"
						data-start="500"
						data-easing="Back.easeOut"
						data-endspeed="300"
						data-captionhidden="on"
						style="z-index: 8"><img src="<?php echo base_url(); ?>__statics/js/slider/images/hill2.png" alt="">
					</div>

					<!-- LAYER NR. 8 -->
					<div class="tp-caption lfr"
						data-x="-59"
						data-y="366"
						data-speed="600"
						data-start="600"
						data-easing="Back.easeOut"
						data-endspeed="300"
						data-captionhidden="on"
						style="z-index: 9"><img src="<?php echo base_url(); ?>__statics/js/slider/images/hill1.png" alt="">
					</div>

					<!-- LAYER NR. 9 -->
					<div class="tp-caption lfr"
						data-x="857"
						data-y="386"
						data-speed="600"
						data-start="700"
						data-easing="Back.easeOut"
						data-endspeed="300"
						data-captionhidden="on"
						style="z-index: 10"><img src="<?php echo base_url(); ?>__statics/js/slider/images/hill2.png" alt="">
					</div>

					<!-- LAYER NR. 10 -->
					<div class="tp-caption medium_bg_orange customin customout"
						data-x="center" data-hoffset="0"
						data-y="80"
						data-customin="x:0;y:0;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
						data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
						data-speed="1000"
						data-start="1700"
						data-easing="Back.easeInOut"
						data-endspeed="300"
						style="z-index: 11">Segera Mendaftar !
					</div>

					<!-- LAYER NR. 11 -->
					<div class="tp-caption mediumlarge_light_white_center customin customout"
						data-x="center" data-hoffset="0"
						data-y="bottom" data-voffset="-200"
						data-customin="x:0;y:0;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
						data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
						data-speed="1000"
						data-start="1900"
						data-easing="Back.easeInOut"
						data-endspeed="300"
						style="z-index: 12">Selama ada kesempatan<br/>
			untuk meningkatkan <br/>
			Kompetensi guru Madrasah
					</div>

					<!-- LAYER NR. 12 -->
					<div class="tp-caption customin"
						data-x="110"
						data-y="118"
						data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
						data-speed="300"
						data-start="1300"
						data-easing="Back.easeOut"
						data-endspeed="300"
						data-captionhidden="on"
						style="z-index: 13"><img src="<?php echo base_url(); ?>__statics/js/slider/images/tree.png" alt="">
					</div>

					<!-- LAYER NR. 13 -->
		
					<!-- LAYER NR. 14 -->
					<div class="tp-caption customin skewtoright"
						data-x="817"
						data-y="197"
						data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
						data-speed="1000"
						data-start="2000"
						data-easing="Back.easeOut"
						data-endspeed="600"
						data-endeasing="Power4.easeIn"
						style="z-index: 15"><img src="<?php echo base_url(); ?>__statics/js/slider/images/guy3.png" alt="">
					</div>

					<!-- LAYER NR. 15 -->
				

					<!-- LAYER NR. 16 -->
					<div class="tp-caption customin customout"
						data-x="126"
						data-y="269"
						data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
						data-customout="x:0;y:180;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
						data-speed="600"
						data-start="1800"
						data-easing="Back.easeOut"
						data-end="4400"
			data-endspeed="600"
						data-endeasing="Bounce.easeOut"
						data-captionhidden="on"
						style="z-index: 17"><img src="<?php echo base_url(); ?>__statics/js/slider/images/apple.png" alt="">
					</div>

					<!-- LAYER NR. 17 -->
					<div class="tp-caption customin customout"
						data-x="167"
						data-y="303"
						data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
						data-customout="x:0;y:180;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
						data-speed="600"
						data-start="1500"
						data-easing="Back.easeOut"
						data-end="4100"
			data-endspeed="600"
						data-endeasing="Bounce.easeOut"
						data-captionhidden="on"
						style="z-index: 18"><img src="<?php echo base_url(); ?>__statics/js/slider/images/apple.png" alt="">
					</div>

					<!-- LAYER NR. 18 -->
					<div class="tp-caption customin customout"
						data-x="264"
						data-y="304"
						data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
						data-customout="x:0;y:180;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
						data-speed="600"
						data-start="1600"
						data-easing="Back.easeOut"
						data-end="4200"
			data-endspeed="600"
						data-endeasing="Bounce.easeOut"
						data-captionhidden="on"
						style="z-index: 19"><img src="<?php echo base_url(); ?>__statics/js/slider/images/apple.png" alt="">
					</div>

					<!-- LAYER NR. 19 -->
					<div class="tp-caption customin customout"
						data-x="296"
						data-y="250"
						data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
						data-customout="x:0;y:180;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
						data-speed="600"
						data-start="1700"
						data-easing="Back.easeOut"
						data-end="4300"
			data-endspeed="600"
						data-endeasing="Bounce.easeOut"
						data-captionhidden="on"
						style="z-index: 20"><img src="<?php echo base_url(); ?>__statics/js/slider/images/apple.png" alt="">
					</div>

					<!-- LAYER NR. 20 -->
					<div class="tp-caption customin customout"
						data-x="223"
						data-y="263"
						data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
						data-customout="x:0;y:180;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
						data-speed="600"
						data-start="1400"
						data-easing="Back.easeOut"
						data-end="4000"
			data-endspeed="600"
						data-endeasing="Bounce.easeOut"
						data-captionhidden="on"
						style="z-index: 21"><img src="<?php echo base_url(); ?>__statics/js/slider/images/apple.png" alt="">
					</div>

					<!-- LAYER NR. 21 -->
					<div class="tp-caption customin customout"
						data-x="166"
						data-y="256"
						data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
						data-customout="x:0;y:180;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
						data-speed="600"
						data-start="1900"
						data-easing="Back.easeOut"
						data-end="4500"
			data-endspeed="600"
						data-endeasing="Bounce.easeOut"
						data-captionhidden="on"
						style="z-index: 22"><img src="<?php echo base_url(); ?>__statics/js/slider/images/apple.png" alt="">
					</div>

					<!-- LAYER NR. 22 -->
					<div class="tp-caption customin customout"
						data-x="118"
						data-y="219"
						data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
						data-customout="x:0;y:180;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
						data-speed="600"
						data-start="2000"
						data-easing="Back.easeOut"
						data-end="4600"
			data-endspeed="600"
						data-endeasing="Bounce.easeOut"
						data-captionhidden="on"
						style="z-index: 23"><img src="<?php echo base_url(); ?>__statics/js/slider/images/apple.png" alt="">
					</div>

					<!-- LAYER NR. 23 -->
					<div class="tp-caption customin customout"
						data-x="251"
						data-y="208"
						data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
						data-customout="x:0;y:180;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
						data-speed="600"
						data-start="2100"
						data-easing="Back.easeOut"
						data-end="4700"
			data-endspeed="600"
						data-endeasing="Bounce.easeOut"
						data-captionhidden="on"
						style="z-index: 24"><img src="<?php echo base_url(); ?>__statics/js/slider/images/apple.png" alt="">
					</div>
				</li>
				<!-- SLIDE  -->
		
			</ul>
			<div class="tp-bannertimer"></div>
		</div>
	</section>

   

	


    <!-- Banner Area End -->

    <!-- intro Area Start-->
    <div class="container" >
        <div class="intro-area-2" >
            <div class="row justify-content-center">
                <div class="col-lg-12">
                    <div class="intro-slider owl-carousel">

					  <?php 
					   $modul = $this->db->get("modul")->result();
					     foreach($modul as $rm){
					  ?>
                      
                        <div class="item">
                            <div class="single-intro-wrap">
                                <div class="thumb">
                                <a href="<?php echo site_url("course/modul/".$rm->slug.""); ?>"><img src="<?php echo base_url(); ?>__statics/img/intro/<?php echo $rm->cover; ?>" alt="img"></a>
                                </div>
                                <div class="wrap-details">
                                    <h6><a href="<?php echo site_url("course/modul/".$rm->slug.""); ?>"><?php echo $rm->nama; ?></a></h6>
                                    <p>Tersedia Gratis</p>
                                </div>
                            </div>
                        </div>
                        
					  <?php 
						 }
						?>
                        
                    </div>
                </div>
            </div>
        </div>            
    </div>



	<div class="potential-area-2 pt-2  ">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 align-self-center">
                    <div class="thumb mb-lg-0 mb-4 me-xl-5 me-lg-3 me-0">
                        <img src="<?php echo base_url(); ?>__statics/img/direktur.jpg" alt="img">
                    </div>
                </div>
                <div class="col-lg-6 align-self-center">
                    <div class="section-title mb-0">
                        <h2>Upgrade Your Skills.</h2>
                        <p>Kita sedang memasuki era revolusi industri 4.0 Era disrupsi. Era keberlimpahan informasi. Sisi lain, kita juga masih berada pada masa pandemi Covid-19 yang mencekam. Fenomena Post-Truth, hoaks, dan perlawanan terhadap ilmu pengetahuan yang baku juga menyeruak.
							<br><br>

						   Pada saat yang sama, peserta didik kita adalah generasi Z yang friendly dengan gadget. Pada umumnya, mereka terkonek dengan internet (IoT). Mereka belajar dan tumbuh dengan caranya sendiri (search of identity).</p>
						   <br>
						  <span><u>Dr. Muhammad Zain M.Ag - Direktur GTK Madrasah Kemenag RI </u></span>
                        <ul class="list-inner">
                            <li><i class="fa fa-check"></i>Meningkatkan skill  mengajar di era digital. </li>
                            <li><i class="fa fa-check"></i>Mendapatkan pengalaman pelatihan dibimbing tim ahli </li>
                            <li><i class="fa fa-check"></i>Gratis dapat diakses kapanpun dan dimanapun</li>
                            <li><i class="fa fa-check"></i>Sertifikat Pelatihan dari Direktorat GTK Madrasah </li>
                        </ul>
                        
                        <a class="btn btn-base-light-border" href="<?= site_url('login/oauth_simpatika') ?>">Pendaftaran </a>
                    </div>
                </div>                
            </div>
        </div>            
    </div>


    <!-- trending courses Area Start-->
    <section class="trending-courses-area ">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-title">
                        <h2>Pelatihan Online GTK </h2>
                    </div>
                </div>
                <div class="col-lg-12">
                    <ul class="edl-nav nav nav-pills">
                        <li class="nav-item">
                            <button class="nav-link active" id="pills-0-tab" data-bs-toggle="pill" data-bs-target="#pills-0">Semua Kategori </button>
                        </li>
						<?php 
						 $kat = $this->db->get("kategori")->result();
						   foreach($kat as $row){
							?>
							
							<li class="nav-item">
								<button class="nav-link" id="pills-<?php echo $row->id; ?>-tab" data-bs-toggle="pill" data-bs-target="#pills-<?php echo $row->id; ?>"><?php echo $row->nama; ?> </button>
							</li>
							<?php
						   }
						 ?>

					
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane fade show active" id="pills-0">
                            <div class="course-slider owl-carousel">

							<?php 
								$kat = $this->db->get("pelatihan")->result();
                                $cat = array("Beginner"=>"red","Profesional"=>"green");
								foreach($kat as $row){
							?>
                                <div class="item">
                                    <div class="single-course-wrap">
                                        <div class="thumb">
                                            <a href="#" class="cat cat-<?php echo $cat[$row->jenis]; ?>"><?php echo ucwords($row->jenis); ?></a>
                                            <img src="<?php echo base_url(); ?>__statics/upload/pelatihan/<?php echo $row->cover; ?>" alt="img">
                                        </div>
                                        <div class="wrap-details">
                                            <h6><a href="<?php echo site_url('pelatihan/detail/'.str_replace("_","-",$row->slug).''); ?>"><?php echo $row->nama; ?></a></h6>
                                            <div class="user-area">
                                                <div class="user-details">
                                                    <img src="<?php echo base_url(); ?>__statics/img/author/1.png" alt="img">
                                                    <a href="#">GTK Madrasah </a>
                                                </div>
                                                <div class="user-rating">
                                                    <span><i class="fa fa-star"></i>
                                                        4.9</span>
                                                </div>
                                            </div>
                                            <div class="price-wrap">
                                                <div class="row align-items-center">
                                                    <div class="col-6">
                                                        <a href="#"><?php echo $this->Reff->get_kondisi(array("id"=>$row->kategori_id),"kategori","nama"); ?></a>
                                                    </div>
                                                    <div class="col-6 text-end">
                                                        <div class="price">Gratis</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

								<?php 
								}
								?>

								


                               


                            </div>
                        </div>
                       
                       
                       
                       
                        
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- trending courses Area End -->

	<div class="text-center pd-top-135">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-8">
                    <div class="section-title">
                        <h2>Apa yang akan didapatkan? </h2>
                        <p>Pelatihan Online GTK Madrasah ditujukkan untuk Pendidik dan Tenaga Kependidikan yang terdaftar disimpatika .</p>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-lg-4 col-sm-6">
                    <div class="single-intro-wrap-2">
                        <div class="thumb">
                            <img src="<?php echo base_url(); ?>__statics/img/intro/03.png" alt="img">
                        </div>
                        <div class="wrap-details">
                            <h4><a href="#">Gratis </a></h4>
                            <p>Pendaftaran dan Pelatihan  Gratis sampai kapanpun dan dapat diakses  dimanapun </p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6">
                    <div class="single-intro-wrap-2">
                        <div class="thumb">
                            <img src="<?php echo base_url(); ?>__statics/img/intro/brain.png" alt="img">
                        </div>
                        <div class="wrap-details">
                            <h4><a href="#">Mudul Lengkap </a></h4>
                            <p>Modul Pelatihan yang lengkap dan sesuai kebutuhan demi menunjang peningkatan kemampuan.</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6">
                    <div class="single-intro-wrap-2">
                        <div class="thumb">
                            <img src="<?php echo base_url(); ?>__statics/img/intro/02.png" alt="img">
                        </div>
                        <div class="wrap-details">
                            <h4><a href="#">e-Sertifikat</a></h4>
                            <p>Setelah mengikuti Pelatihan Online, GTK Madrasah akan memberikan Sertifikat yang menandakan lulus pelatihan</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>            
    </div>



    <section class="trending-courses-area ">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-title">
                        <h2>Tips dan Trik </h2>
                    </div>
                </div>
                <div class="col-lg-12">
                    <ul class="edl-nav nav nav-pills">
                        <li class="nav-item">
                            <button class="nav-link active" id="pills-0-tab" data-bs-toggle="pill" data-bs-target="#pills-0">Semua Kategori </button>
                        </li>
						

					
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane fade show active" id="pills-0">
                            <div class="course-slider owl-carousel">

							
                                <div class="item">
                                    <div class="single-course-wrap">
                                        <div class="thumb">
                                            <a href="#" class="cat cat-blue">Tips & Trik</a>
                                            <img src="<?php echo base_url(); ?>__statics/img/tutorial/screen1.png" alt="img">
                                        </div>
                                        <div class="wrap-details">
                                            <h6><a href="https://www.youtube.com/watch?v=z0UzhNzlmJE" target="_blank">Tutorial Screen Mirorring Laptop Ke LCD</a></h6>
                                            <div class="user-area">
                                                <div class="user-details">
                                                    <img src="<?php echo base_url(); ?>__statics/img/author/1.png" alt="img">
                                                    <a href="#">GTK Madrasah </a>
                                                </div>
                                                <div class="user-rating">
                                                    <span><i class="fa fa-star"></i>
                                                        4.9</span>
                                                </div>
                                            </div>
                                            <div class="price-wrap">
                                                <div class="row align-items-center">
                                                    <div class="col-6">
                                                        <a href="#">Pembelajaran</a>
                                                    </div>
                                                    <div class="col-6 text-end">
                                                        <div class="price">Gratis</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="item">
                                    <div class="single-course-wrap">
                                        <div class="thumb">
                                            <a href="#" class="cat cat-blue">Tips & Trik</a>
                                            <img src="<?php echo base_url(); ?>__statics/img/tutorial/screen2.png" alt="img">
                                        </div>
                                        <div class="wrap-details">
                                            <h6><a href="https://www.youtube.com/watch?v=KqR0TPswJBo" target="_blank">Tutorial Screen Mirorring dari HP Ke Laptop</a></h6>
                                            <div class="user-area">
                                                <div class="user-details">
                                                    <img src="<?php echo base_url(); ?>__statics/img/author/1.png" alt="img">
                                                    <a href="#">GTK Madrasah </a>
                                                </div>
                                                <div class="user-rating">
                                                    <span><i class="fa fa-star"></i>
                                                        4.9</span>
                                                </div>
                                            </div>
                                            <div class="price-wrap">
                                                <div class="row align-items-center">
                                                    <div class="col-6">
                                                        <a href="#">Pembelajaran</a>
                                                    </div>
                                                    <div class="col-6 text-end">
                                                        <div class="price">Gratis</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="item">
                                    <div class="single-course-wrap">
                                        <div class="thumb">
                                            <a href="#" class="cat cat-blue">Tips & Trik</a>
                                            <img src="<?php echo base_url(); ?>__statics/img/tutorial/screen2.png" alt="img">
                                        </div>
                                        <div class="wrap-details">
                                            <h6><a href="https://www.youtube.com/watch?v=qb3AmSKvKt8" target="_blank">Tutorial Screen Mirorring Menggunakan Aplikasi</a></h6>
                                            <div class="user-area">
                                                <div class="user-details">
                                                    <img src="<?php echo base_url(); ?>__statics/img/author/1.png" alt="img">
                                                    <a href="#">GTK Madrasah </a>
                                                </div>
                                                <div class="user-rating">
                                                    <span><i class="fa fa-star"></i>
                                                        4.9</span>
                                                </div>
                                            </div>
                                            <div class="price-wrap">
                                                <div class="row align-items-center">
                                                    <div class="col-6">
                                                        <a href="#">Pembelajaran</a>
                                                    </div>
                                                    <div class="col-6 text-end">
                                                        <div class="price">Gratis</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="item">
                                    <div class="single-course-wrap">
                                        <div class="thumb">
                                            <a href="#" class="cat cat-blue">Tips & Trik</a>
                                            <img src="<?php echo base_url(); ?>__statics/img/tutorial/screen1.png" alt="img">
                                        </div>
                                        <div class="wrap-details">
                                            <h6><a href="https://youtu.be/fNvEOf_evXI" target="_blank">Tutorial Screen Mirorring dari HP Ke LCD</a></h6>
                                            <div class="user-area">
                                                <div class="user-details">
                                                    <img src="<?php echo base_url(); ?>__statics/img/author/1.png" alt="img">
                                                    <a href="#">GTK Madrasah </a>
                                                </div>
                                                <div class="user-rating">
                                                    <span><i class="fa fa-star"></i>
                                                        4.9</span>
                                                </div>
                                            </div>
                                            <div class="price-wrap">
                                                <div class="row align-items-center">
                                                    <div class="col-6">
                                                        <a href="#">Pembelajaran</a>
                                                    </div>
                                                    <div class="col-6 text-end">
                                                        <div class="price">Gratis</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

								


                               


                            </div>
                        </div>
                       
                       
                       
                       
                        
                    </div>
                </div>
            </div>
        </div>
    </section>



    <!-- enllor courses Area Start-->
    <section class="enllor-courses-area pd-top-120 pd-bottom-140">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-title">
                        <h2>Pelatihan Populer</h2>
                    </div>
                </div>
                <div class="col-lg-12">
                    <ul class="edl-nav nav nav-pills">
					   <?php 
						 $kat = $this->db->get("kategori")->result();
						   foreach($kat as $row){
							?>
							
							<li class="nav-item">
								<button class="nav-link" id="pills-<?php echo $row->id; ?>-tab" data-bs-toggle="pill" data-bs-target="#populer-<?php echo $row->id; ?>"><?php echo $row->nama; ?> </button>
							</li>
							<?php
						   }
						 ?>
                    </ul>
                    <div class="tab-content">
					<?php 
						 $kat = $this->db->get("kategori")->result();
						   foreach($kat as $rowkat){
							?>

                        <div class="tab-pane fade show active" id="populer-<?php echo $rowkat->id; ?>">
                            <div class="course-slider owl-carousel">

								<?php 
									$kat = $this->db->get_where("pelatihan",array("kategori_id"=>$rowkat->id))->result();
									foreach($kat as $row){
								?>
									<div class="item">
										<div class="single-course-wrap">
											<div class="thumb">
												<a href="#" class="cat cat-red"><?php echo ucwords($row->jenis); ?></a>
												<img src="<?php echo base_url(); ?>__statics/upload/pelatihan/<?php echo $row->cover; ?>" alt="img">
											</div>
											<div class="wrap-details">
												<h6><a href="<?php echo site_url('pelatihan/detail/'.str_replace("_","-",$row->slug).''); ?>"><?php echo $row->nama; ?></a></h6>
												<div class="user-area">
													<div class="user-details">
														<img src="<?php echo base_url(); ?>__statics/img/author/1.png" alt="img">
														<a href="#">GTK Madrasah </a>
													</div>
													<div class="user-rating">
														<span><i class="fa fa-star"></i>
															4.9</span>(98)
													</div>
												</div>
												<div class="price-wrap">
													<div class="row align-items-center">
														<div class="col-6">
															<a href="#"><?php echo $this->Reff->get_kondisi(array("id"=>$row->kategori_id),"kategori","nama"); ?></a>
														</div>
														<div class="col-6 text-end">
															<div class="price">Gratis</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>

									<?php 
									}
									?>
                              
                            </div>
                        </div>

						<?php 
						   }
						   ?>
                   
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- enllor courses Area End -->

    <!-- testimonial courses Area Start-->
    <section class="testimonial-courses-area pd-bottom-150">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-title">
                        <h2> Testimoni Peserta Pelatihan </h2>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="testimonial-slider owl-carousel">

					<?php 
					  $testimoni = $this->db->query("select * from testimoni order by tanggal DESC")->result();
					    foreach($testimoni as $rowtes){
					?>
                        <div class="item">
                            <div class="single-testimonial-wrap">
                                <div class="thumb">
                                    <img src="<?php echo base_url(); ?>__statics/img/quote.png" alt="img">
                                </div>
                                <div class="wrap-details">
                                    <h5><a href="#"><?php echo $rowtes->status; ?></a></h5>
                                    <p><?php echo $rowtes->testimoni; ?></p>
                                    <span><?php echo $this->Reff->get_kondisi(array("id"=>$rowtes->peserta_id),"peserta","nama"); ?> <br> <?php echo $this->Reff->formattimestamp($rowtes->tanggal); ?> </span>
                                   
                                </div>
                            </div>
                        </div>
					<?php 
						}
					?>
                       
                        
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- testimonial courses Area End -->

   
    <!-- client Area End -->

    <!-- about Area Start-->
    <section class="about-area ">
        <div class="container">
            <div class="row justify-content-center">
              
                <div class="col-lg-12">
                    <div class="single-about-wrap">
                        <div class="thumb">
                            <img src="<?php echo base_url(); ?>__statics/img/intro/brain.png" alt="img">
                        </div>
                        <div class="wrap-details">
                            <h3><a href="#"> Daftar Menjadi Peserta </a></h3>
                            <p>Peserta pelatihan online GTK Madrasah tidak dipungut biaya dalam mengikuti pelatihan, jika dinyatakan lulus pelatihan, Anda akan mendapatkan sertifikat </p>
                            <a class="btn btn-base-light-border" href="<?= site_url('login/oauth_simpatika') ?>">Pendaftaran Peserta </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>