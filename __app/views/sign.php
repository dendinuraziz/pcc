<section class="breadcrumb-area" style="background-color: #F9FAFD;">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-12 align-self-center">
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page"> Masuk Pelatihan</li>
                    </ul>
                    <h2>Untuk dapat mengikuti pelatihan silahkan login menggunakan akun SIMPATIKA </h2>
                </div>
            </div>
        </div>
    </section>
    <!-- breabcrumb Area End -->
    
    <div class="signin-area pd-top-100 pd-bottom-100">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-5">
                         <a class="btn btn-danger" href="<?= site_url('login/oauth_simpatika') ?>"><i class="fa fa-lock"></i> MASUK SIMPATIKA </a>
                </div>
            </div>
        </div>
    </div>