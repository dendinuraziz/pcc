<section class="courses-details-area pd-top-65 pd-bottom-130">
        
            <div class="container">
                <div class="row g-5">
                <div class="potential-area-2 pt-4 pd-bottom-140">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12 ">
                            <div class="container-fluid ">
                        <div class="card">
                        <div class="card-body">
                            <div class="row" id="wrap">
                            <div class="col-xxl-3 box-col-12">
                                <div class="md-sidebar mb-3">
                                <div class="md-sidebar-aside job-left-aside custom-scrollbar">
                                    <div id="external-events">
                                    <h4> Time Line Pembangunan ZI</h4>
                                    <div id="external-events-list">
                                        <div class="fc-event fc-h-event fc-daygrid-event fc-daygrid-block-event">
                                          <div class="fc-event-main"> Penilaian Mandiri (PMPZI) </div>
                                        </div>
                                       
                                    
                                    </div>
                                    <hr>
                                    <div id="external-events-list">
                                        <div class="fc-event fc-h-event fc-daygrid-event fc-daygrid-block-event">
                                          <div class="fc-event-main"> Submit Penilaian PMPZI Unit Kerja ke Menteri Agama </div>
                                        </div>
                                       
                                    
                                    </div>

                                    <hr>
                                    <div id="external-events-list">
                                        <div class="fc-event fc-h-event fc-daygrid-event fc-daygrid-block-event">
                                          <div class="fc-event-main"> Penilaian Pendahuluan  </div>
                                        </div>
                                       
                                    
                                    </div>

                                    <hr>
                                    <div id="external-events-list">
                                        <div class="fc-event fc-h-event fc-daygrid-event fc-daygrid-block-event">
                                          <div class="fc-event-main"> Pengusulan Calon Pilot Project WBK/WBBM kepada Tim Penilai Internal (TPI)/Inspektorat Jenderal </div>
                                        </div>
                                       
                                    
                                    </div>

                                    <hr>
                                    <div id="external-events-list">
                                        <div class="fc-event fc-h-event fc-daygrid-event fc-daygrid-block-event">
                                          <div class="fc-event-main"> Penilaian Satker Calon Pilot Project </div>
                                        </div>
                                       
                                    
                                    </div>

                                    <hr>
                                    <div id="external-events-list">
                                        <div class="fc-event fc-h-event fc-daygrid-event fc-daygrid-block-event">
                                          <div class="fc-event-main"> Penyampaian Laporan Hasil Penilaian PMPZI kepada Menteri </div>
                                        </div>
                                       
                                    
                                    </div>

                                    <hr>
                                    <div id="external-events-list">
                                        <div class="fc-event fc-h-event fc-daygrid-event fc-daygrid-block-event">
                                          <div class="fc-event-main"> Penetapan Pilot Project  </div>
                                        </div>
                                       
                                    
                                    </div>

                                    <hr>
                                    <div id="external-events-list">
                                        <div class="fc-event fc-h-event fc-daygrid-event fc-daygrid-block-event">
                                          <div class="fc-event-main"> Submit Hasil Penilaian PMPZI kepada Kemen PANRB </div>
                                        </div>
                                       
                                    
                                    </div>
                                    <p>
                                    
                                    </p>
                                    </div>
                                </div>
                                </div>
                            </div>
                            <div class="col-xxl-9 box-col-12">
                                <div class="calendar-default" id="calendar-container">
                                <div id="calendar"></div>
                                </div>
                                <img src="<?php echo base_url(); ?>__statics/img/jadwal.png" class="img-responsive" alt="img">
                            </div>
                            </div>
                        </div>
                  </div>
          

                </div>
                            </div>
                                       
                        </div>
                    </div>            
                </div>

                </div>
                </div>
 </section>
