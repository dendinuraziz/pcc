<section class="breadcrumb-area" style="background-color: #F9FAFD;">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-12 align-self-center">
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">PCC</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Daftar Piloting Project ZI</li>
                    </ul>
                    <h2>Daftar Piloting Project ZI</h2>
                </div>
            </div>
        </div>
    </section>


<section class="courses-details-area pd-top-65 pd-bottom-130">
        
            <div class="container">
                <div class="row g-5">
                <div class="potential-area-2 pt-4 pd-bottom-140">
                    <div class="container">
                        <div class="row">
                        <div class="col-lg-12">

                          <?php 
                            $tahun = $this->input->get_post("tahun");
                             $data = $this->db->get_where("piloting_zi",array("kategori"=>$tahun))->result();
                              foreach($data as $d){
                            ?>
                             <div class="col-12" data-sal-delay="150" data-sal="slide-up" data-sal-duration="800">
                                <div class="edu-event-list event-list-2">
                                    <div class="inner">
                                        
                                        <div class="content">
                                           
                                            <h4 class="title"><a href="<?php echo base_url(); ?>dokumen/<?php echo $d->kategori; ?>/<?php echo $d->file; ?>" target="_blank"> <?php echo $d->nama; ?> </a></h4>
                                            
                                           
                                            <div class="read-more-btn">
                                                <a class="btn " href="<?php echo base_url(); ?>dokumen/<?php echo $d->kategori; ?>/<?php echo $d->file; ?>" target="_blank">Download <?php echo $d->nama; ?> <i class="icon-4"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php 
                              }
                              ?>

                           
                            
                            
                    </div>

                        </div>
                    </div>            
                </div>

                </div>
                </div>
 </section>