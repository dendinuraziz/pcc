<section class="breadcrumb-area" style="background-color: #F9FAFD;">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-12 align-self-center">
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Pelatihan</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Pencarian</li>
                    </ul>
                    <h2>Hasil Pencarian <b><u><i><?php echo $_GET['q']; ?></i></u></b> </h2>
                </div>
            </div>
        </div>
    </section>
    <!-- breabcrumb Area End -->

    <section class="trending-courses-area pd-top-135 pd-bottom-130">
        <div class="container">
            <div class="row">
               
            <?php 
					
                    if(count($data) >0){
                        $cat = array("0"=>"red","1"=>"green");
                        $status = array("0"=>"Belum Aktif","1"=>"Aktif");
					foreach($data as $row){
					?>
                        <div class="col-xl-3 col-md-6">
                            <div class="single-course-wrap">
                                <div class="thumb">
                                <a href="<?php echo site_url('pelatihan/detail/'.$row->slug); ?>" class="cat cat-<?php echo $cat[$row->nonaktif]; ?>"><?php echo $status[$row->nonaktif]; ?></a>
                                    <img src="<?php echo base_url(); ?>__statics/upload/pelatihan/<?php echo $row->cover; ?>" alt="img">
                                </div>
                                <div class="wrap-details">
                                <h6><a href="<?php echo site_url('pelatihan/detail/'.$row->slug); ?>"><?php echo $row->nama; ?></a></h6>
                                    <div class="user-area">
                                        <div class="user-details">
                                            <img src="<?php echo base_url(); ?>__statics/img/author/1.png" alt="img">
                                            <a href="#">GTK Madrasah</a>
                                        </div>
                                        <div class="user-rating">
                                            <span><svg class="svg-inline--fa fa-star fa-w-18" aria-hidden="true" focusable="false" data-prefix="fa" data-icon="star" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512" data-fa-i2svg=""><path fill="currentColor" d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z"></path></svg><!-- <i class="fa fa-star"></i> Font Awesome fontawesome.com -->
                                                4.9</span>(76)
                                        </div>
                                    </div>
                                    <div class="price-wrap">
                                        <div class="row align-items-center">
                                            <div class="col-6">
                                                <a href="#"><?php echo $this->Reff->get_kondisi(array("id"=>$row->kategori_id),"kategori","nama"); ?></a>
                                            </div>
                                            <div class="col-6 text-end">
                                                <div class="price">Gratis</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                      <?php 
                         }
                        }else{

                            ?> <center>Keyword  <b><?php echo $_GET['q']; ?></b> belum tersedia </center><?php 
                        }
                       ?>

 
                <div class="col-lg-12 text-center">
                    <nav aria-label="Page navigation example">
                        <ul class="pagination">
                          <li class="page-item"><a class="page-link" href="#">Previous</a></li>
                          <li class="page-item"><a class="page-link" href="#">1</a></li>
                          <li class="page-item"><a class="page-link" href="#">2</a></li>
                          <li class="page-item"><a class="page-link" href="#">3</a></li>
                          <li class="page-item"><a class="page-link" href="#">Next</a></li>
                        </ul>
                      </nav>
                </div>
            </div>
        </div>
    </section>

