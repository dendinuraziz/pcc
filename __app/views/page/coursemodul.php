<section class="breadcrumb-area" style="background-color: #F9FAFD;">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-12 align-self-center">
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Pelatihan</a></li>
                        <li class="breadcrumb-item active" aria-current="page"><?php echo $kategori->nama; ?></li>
                    </ul>
                    <h2>Pelatihan <?php echo $kategori->nama; ?></h2>
                </div>
            </div>
        </div>
    </section>
    <!-- breabcrumb Area End -->

    <!-- trending courses Area Start-->
    <section class="trending-courses-area pd-top-65 pd-bottom-130">
        <div class="container">
            <div class="row">
                <div class="col-lg-3">
                    <div class="dmne-sidebar">
                       

                        <div class="widget widget-select-inner">
                            <h4 class="widget-title">Modul</h4>
                            <ul>
                            <?php 
                                $modul = $this->db->get("modul")->result();
                                    foreach($modul as $rm){
                                ?>
                                <li>
                                    <div class="single-form-check form-check">
                                        <input class="form-check-input" disabled <?php echo ($rm->id==$kategori->id) ? "checked":""; ?>  type="checkbox" value="" id="modul<?php echo $rm->id; ?>">
                                        <label class="form-check-label" for="modul<?php echo $rm->id; ?>">
                                            <?php echo $rm->nama; ?>
                                        </label>
                                    </div>
                                </li>
                                <?php 
                                    }
                                ?>
                                
                            </ul>
                        </div>

                        <div class="widget widget-select-inner">
                            <h4 class="widget-title">Kategori </h4>
                            <ul>
                                <?php 
                                $modul = $this->db->get("kategori")->result();
                                    foreach($modul as $rm){
                                ?>
                                <li>
                                    <div class="single-form-check form-check">
                                        <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault<?php echo $rm->id; ?>">
                                        <label class="form-check-label"  for="flexCheckDefault<?php echo $rm->id; ?>">
                                            <?php echo $rm->nama; ?>
                                        </label>
                                    </div>
                                </li>
                               <?php 
                                    }
                                ?>
                            </ul>
                        </div>
                        <div class="widget widget-select-inner">
                            <h4 class="widget-title">Level</h4>
                            <ul>
                                <li>
                                    <div class="single-form-check form-check">
                                        <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault14">
                                        <label class="form-check-label" for="flexCheckDefault14">
                                            Beginner
                                        </label>
                                    </div>
                                </li>
                                <li>
                                    <div class="single-form-check form-check">
                                        <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault15">
                                        <label class="form-check-label" for="flexCheckDefault15">
                                            Intermediate
                                        </label>
                                    </div>
                                </li>
                                <li>
                                    <div class="single-form-check form-check">
                                        <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault16">
                                        <label class="form-check-label" for="flexCheckDefault16">
                                            Expert
                                        </label>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        
                        
                    </div>
                </div>
                <div class="col-lg-9">
                    <div class="row">

                    <?php 
					$kat = $this->db->get_where("v_pelatihan",array("modul"=>$kategori->slug))->result();
                    if(count($kat) >0){
                        $cat = array("0"=>"red","1"=>"green");
                        $status = array("0"=>"Belum Aktif","1"=>"Aktif");
					foreach($kat as $row){
					?>
                        <div class="col-xl-4 col-md-6">
                            <div class="single-course-wrap">
                                <div class="thumb">
                                <a href="<?php echo site_url('pelatihan/detail/'.$row->slug); ?>" class="cat cat-<?php echo $cat[$row->nonaktif]; ?>"><?php echo $status[$row->nonaktif]; ?></a>
                                    <img src="<?php echo base_url(); ?>__statics/upload/pelatihan/<?php echo $row->cover; ?>" alt="img">
                                </div>
                                <div class="wrap-details">
                                <h6><a href="<?php echo site_url('pelatihan/detail/'.$row->slug); ?>"><?php echo $row->nama; ?></a></h6>
                                    <div class="user-area">
                                        <div class="user-details">
                                            <img src="<?php echo base_url(); ?>__statics/img/author/1.png" alt="img">
                                            <a href="#">GTK Madrasah</a>
                                        </div>
                                        <div class="user-rating">
                                            <span><svg class="svg-inline--fa fa-star fa-w-18" aria-hidden="true" focusable="false" data-prefix="fa" data-icon="star" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512" data-fa-i2svg=""><path fill="currentColor" d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z"></path></svg><!-- <i class="fa fa-star"></i> Font Awesome fontawesome.com -->
                                                4.9</span>(76)
                                        </div>
                                    </div>
                                    <div class="price-wrap">
                                        <div class="row align-items-center">
                                            <div class="col-6">
                                                <a href="#"><?php echo $this->Reff->get_kondisi(array("id"=>$row->kategori_id),"kategori","nama"); ?></a>
                                            </div>
                                            <div class="col-6 text-end">
                                                <div class="price">Gratis</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                      <?php 
                         }
                        }else{

                            ?> <center>Pelatihan untuk <b><?php echo $kategori->nama; ?></b> belum tersedia </center><?php 
                        }
                       ?>

                      
                    </div>
                </div>
                <div class="col-lg-12 text-center">
                    <nav aria-label="Page navigation example">
                        <ul class="pagination">
                          <li class="page-item"><a class="page-link" href="#">Previous</a></li>
                          <li class="page-item"><a class="page-link" href="#">1</a></li>
                          <li class="page-item"><a class="page-link" href="#">2</a></li>
                          <li class="page-item"><a class="page-link" href="#">3</a></li>
                          <li class="page-item"><a class="page-link" href="#">Next</a></li>
                        </ul>
                      </nav>
                </div>
            </div>
        </div>
    </section>