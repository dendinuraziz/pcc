<section class="breadcrumb-area" style="background-color: #F9FAFD;">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-12 align-self-center">
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Sign In</li>
                    </ul>
                    <h2>Silahkan masuk menggunakan akun SIMPATIKA </h2>
                </div>
            </div>
        </div>
    </section>
    <!-- breabcrumb Area End -->
    
    <div class="signin-area pd-top-130 pd-bottom-100">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-5">
                    <form class="single-signin-form-wrap">
                        <div class="single-input-wrap">
                            <input type="text" placeholder="Email simpatika">
                        </div>
                       
                        <div class="single-input-wrap">
                            <input type="password" placeholder="Password Simpatika ">
                        </div>
                        <div class="btn-wrap">
                            <button class="btn btn-base w-100">Masuk Sekarang</button>
                        </div>
                        <div class="bottom-content">
                            <a href="#">Forgottem Your Password</a>
                            <a class="strong" href="signup.html">Signup</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
</div>