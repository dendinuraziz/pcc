<style>

.edu-event-area {
    position: relative;
}

.edu-event-area .shape-group li {
    position: absolute;
    z-index: -1;
}

.edu-event-area .shape-group li.shape-1 {
    top: 81px;
    left: -81px;
}

.edu-event-area .shape-group li.shape-2 {
    top: -44px;
    left: -190px;
    z-index: -1;
}

.edu-event-area .shape-group li.shape-2 span {
    display: block;
    height: 400px;
    width: 400px;
    border: 1px solid var(--color-border);
    border-radius: 50%;
}

.event-area-2 {
    padding: 115px 0 120px;
}

@media only screen and (min-width: 768px) and (max-width: 991px) {
    .event-area-2 {
        padding: 55px 0 100px;
    }
}

@media only screen and (max-width: 767px) {
    .event-area-2 {
        padding: 35px 0 80px;
    }
}

.event-area-3 {
    padding: 220px 0 230px;
    z-index: 1;
}

.event-area-3::before {
    content: '';
    position: absolute;
    left: 0;
    top: 0;
    width: 100%;
    height: 100%;
    background-image: url(../images/bg/bg-image-5.svg);
    background-repeat: no-repeat;
    background-size: cover;
    z-index: -1;
}

@media only screen and (min-width: 768px) and (max-width: 991px) {
    .event-area-3 {
        padding: 200px 0;
    }
}

@media only screen and (max-width: 767px) {
    .event-area-3 {
        padding: 200px 0;
    }
}

.event-area-3 .shape-group li {
    z-index: -1;
}

.event-area-3 .shape-group li img {
    opacity: .3;
}

.event-area-3 .shape-group li.shape-1 {
    right: -77px;
    top: 108px;
    left: inherit;
}

.event-area-3 .shape-group li.shape-2 {
    top: inherit;
    bottom: -71px;
    left: -90px;
}

.event-area-4 .edu-event.event-style-1 .content {
    background-color: var(--color-white);
    box-shadow: var(--shadow-dark);
}

.event-area-4 .edu-event.event-style-1 .content::after {
    display: none;
}

.event-area-4 .edu-event.event-style-1 .content .event-date {
    background: var(--color-white);
    box-shadow: 0px 20px 70px 0px rgba(20, 20, 20, 0.15);
}

.event-area-4 .edu-event.event-style-1 .content .event-date .day {
    color: var(--color-primary);
}

.event-area-4 .edu-event.event-style-1 .content .event-date .month {
    color: var(--color-heading);
}

.event-area-4 .edu-event.event-style-1 .content .title {
    font-weight: var(--p-bold);
}

.event-area-4 .edu-event.event-style-1 .content .event-meta li {
    color: var(--color-heading);
}

.event-area-4 .event-view-all-btn a.btn-transparent {
    color: var(--color-primary);
}

.event-details {
    margin-bottom: 60px;
}

.event-details .main-thumbnail {
    margin-bottom: 70px;
}

.event-details .main-thumbnail img {
    border-radius: 10px;
}

.event-details .details-content ul {
    list-style-type: disc;
    margin-bottom: 60px;
}

.event-details .details-content ul li {
    color: var(--color-heading);
}

.event-details .details-content .event-meta {
    display: -webkit-box;
    display: -webkit-flex;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-align: center;
    -webkit-align-items: center;
    -ms-flex-align: center;
    align-items: center;
    margin: -10px -20px;
    -webkit-flex-wrap: wrap;
    -ms-flex-wrap: wrap;
    flex-wrap: wrap;
}

.event-details .details-content .event-meta li {
    margin-right: 20px;
    margin-left: 20px;
    color: var(--color-heading);
    display: -webkit-box;
    display: -webkit-flex;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-align: center;
    -webkit-align-items: center;
    -ms-flex-align: center;
    align-items: center;
    position: relative;
}

.event-details .details-content .event-meta li:after {
    content: "";
    height: 19px;
    width: 1px;
    background-color: #dbdbdb;
    position: absolute;
    top: 4px;
    right: -20px;
}

.event-details .details-content .event-meta li:last-child:after {
    display: none;
}

.event-details .details-content .event-meta li i {
    padding-right: 10px;
    font-size: 20px;
    color: var(--color-primary);
}

.event-details .details-content .gmap_canvas {
    margin-top: 40px;
}

.event-details .details-content .gmap_canvas #gmap_canvas {
    height: 370px;
    width: 100%;
    border-radius: 5px;
}

.event-speaker .heading-title {
    margin-bottom: 30px;
}
</style>
<section class="courses-details-area pd-top-65 pd-bottom-130">
        
            <div class="container">
                <div class="row g-5">
                   
                    <div class="col-lg-12 order-lg-1 col-pr--35">
                        <div class="row g-5">
                        <div class="col-12" data-sal-delay="150" data-sal="slide-up" data-sal-duration="800">
                                <div class="edu-event-list event-list-2">
                                    <div class="inner">
                                        
                                        <div class="content">
                                           
                                            <h4 class="title"><a href="#"><?php echo ucwords(strtolower("1.	Perpres Nomor 81 tentang Tahun 2010 Tentang Grand Design Reformasi Birokrasi 2010 - 2025")); ?> </a></h4>
                                            <span class="event-location"><i class="fa fa-file"></i> Dasar Hukum</span>
                                            <p><?php echo ucwords(strtolower("1.	Perpres Nomor 81 tentang Tahun 2010 Tentang Grand Design Reformasi Birokrasi 2010 - 2025")); ?></p>
                                            <div class="read-more-btn">
                                                <a class="btn btn-base-light-border" href="<?php echo base_url(); ?>dokumen/permenpanRB.pdf" target="_blank">Download  <i class="icon-4"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="col-12" data-sal-delay="150" data-sal="slide-up" data-sal-duration="800">
                                <div class="edu-event-list event-list-2">
                                    <div class="inner">
                                        
                                        <div class="content">
                                           
                                            <h4 class="title"><a href="#"><?php echo ucwords(strtolower("2. PERATURAN MENTERI PENDAYAGUNAAN APARATUR NEGARA DAN REFORMASI BIROKRASI REPUBLIK INDONESIA NOMOR 90 TAHUN 2021")); ?> </a></h4>
                                            <span class="event-location"><i class="fa fa-file"></i> Dasar Hukum</span>
                                            <p><?php echo ucwords(strtolower("PERATURAN MENTERI PENDAYAGUNAAN APARATUR NEGARA DAN REFORMASI BIROKRASI REPUBLIK INDONESIA NOMOR 90 TAHUN 2021 TENTANG PEMBANGUNAN DAN EVALUASI ZONA INTEGRITAS MENUJU WILAYAH BEBAS DARI KORUPSI DAN WILAYAH BIROKRASI BERSIH DAN MELAYANI DI INSTANSI PEMERINTAH")); ?></p>
                                            <div class="read-more-btn">
                                                <a class="btn btn-base-light-border" href="<?php echo base_url(); ?>dokumen/permenpanRB.pdf" target="_blank">Download  <i class="icon-4"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-12" data-sal-delay="150" data-sal="slide-up" data-sal-duration="800">
                                <div class="edu-event-list event-list-2">
                                    <div class="inner">
                                        
                                        <div class="content">
                                           
                                            <h4 class="title"><a href="#"><?php echo ucwords(strtolower("3.	Permen PANRB Nomor 3 Tahun 2023  tentang Perubahan atas Peraturan Menteri Pendayagunaan Aparatur Negara dan Reformasi Birokrasi Nomor 25 Tahun 2020 tentang Road Map Reformasi Birokrasi 2020-2024")); ?> </a></h4>
                                            <span class="event-location"><i class="fa fa-file"></i> Dasar Hukum</span>
                                            <p><?php echo ucwords(strtolower("3.	Permen PANRB Nomor 3 Tahun 2023  tentang Perubahan atas Peraturan Menteri Pendayagunaan Aparatur Negara dan Reformasi Birokrasi Nomor 25 Tahun 2020 tentang Road Map Reformasi Birokrasi 2020-2024")); ?></p>
                                            <div class="read-more-btn">
                                                <a class="btn btn-base-light-border" href="<?php echo base_url(); ?>dokumen/permenpanRB.pdf" target="_blank">Download  <i class="icon-4"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-12" data-sal-delay="150" data-sal="slide-up" data-sal-duration="800">
                                <div class="edu-event-list event-list-2">
                                    <div class="inner">
                                        
                                        <div class="content">
                                           
                                            <h4 class="title"><a href="#"><?php echo ucwords(strtolower("4.	Permen PANRB Nomor 26 Tahun 2020  tentang Pedoman Evaluasi Pelaksanaan Reformasi Birokrasi")); ?> </a></h4>
                                            <span class="event-location"><i class="fa fa-file"></i> Dasar Hukum</span>
                                            <p><?php echo ucwords(strtolower("4.	Permen PANRB Nomor 26 Tahun 2020  tentang Pedoman Evaluasi Pelaksanaan Reformasi Birokrasi")); ?></p>
                                            <div class="read-more-btn">
                                                <a class="btn btn-base-light-border" href="<?php echo base_url(); ?>dokumen/permenpanRB.pdf" target="_blank">Download  <i class="icon-4"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-12" data-sal-delay="150" data-sal="slide-up" data-sal-duration="800">
                                <div class="edu-event-list event-list-2">
                                    <div class="inner">
                                        
                                        <div class="content">
                                           
                                            <h4 class="title"><a href="#"><?php echo ucwords(strtolower("5.	KMA Nomor 633 Tahun 2020 Pedoman Pelaksanaan Reformasi Birokrasi pada Kementerian Agama")); ?> </a></h4>
                                            <span class="event-location"><i class="fa fa-file"></i> Dasar Hukum</span>
                                            <p><?php echo ucwords(strtolower("5.	KMA Nomor 633 Tahun 2020 Pedoman Pelaksanaan Reformasi Birokrasi pada Kementerian Agama")); ?></p>
                                            <div class="read-more-btn">
                                                <a class="btn btn-base-light-border" href="<?php echo base_url(); ?>dokumen/permenpanRB.pdf" target="_blank">Download  <i class="icon-4"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                           
                            


                        </div>
                    </div>
                </div>


            </div>
        </div>
        <br>
        <br>
        <br>
</section>