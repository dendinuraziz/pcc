<section class="courses-details-area pd-top-65 pd-bottom-130">
        
            <div class="container">
                <div class="row g-5">
                        
                    <!--Section: FAQ-->
<section>
  <h3 class="text-center mb-4 pb-2 text-primary fw-bold">Frequently Asked Question (FAQ)</h3>
  <p class="text-center mb-5">
  Frequently Asked Question (FAQ) terkait Pembangunan Zona Integritas (ZI)
  </p>

  <div class="row">
    <div class="col-md-6 col-lg-4 mb-4">
      <h6 class="mb-3 text-primary"><i class="far fa-paper-plane text-primary pe-2"></i>  Apa itu ZI?</h6>
      <p>
      Zona Integrias (ZI) adalah instansi pemerintah yang pimpinan dan jajarannya telah berkomitmen untuk mewujudkan Wilayah Bebas dari Korupsi/Wilayah Birokrasi Bersih dan Melayani melalui reformasi birokrasi, khususnya dalam hal mewujudkan pemerintahan yang bersih dan akuntabel serta pelayanan publik yang prima.
      </p>
    </div>
    <div class="col-md-6 col-lg-4 mb-4">
      <h6 class="mb-3 text-primary"><i class="far fa-paper-plane text-primary pe-2"></i>  	Apa itu WBK?</h6>
      <p>
      Wilayah Bebas dari Korupsi (WBK) adalah predikat yang diberikan kepada unit/satuan kerja yang telah berhasil melaksanakan reformasi birokrasi dengan baik, yang telah memenuhi sebagian besar kriteria proses perbaikan pada komponen pengungkit serta mewujudkan pemerintahan yang bersih dan akuntabel serta pelayanan publik yang prima.
      </p>
    </div>
    <div class="col-md-6 col-lg-4 mb-4">
      <h6 class="mb-3 text-primary"><i class="far fa-paper-plane text-primary pe-2"></i> 	Apa itu WBBM? </h6>
      <p>
      Wilayah Birokrasi Bersih dan Melayani (WBBM) adalah predikat yang diberikan kepada unit/satuan kerja yang telah berhasil melaksanakan reformasi birokrasi dengan sangat baik, dengan telah memenuhi sebagian besar kriteria proses perbaikan pada komponen pengungkit serta mewujudkan pemerintahan yang bersih dan akuntabel serta pelayanan publik yang prima.
      </p>
    </div>

    <div class="col-md-6 col-lg-4 mb-4">
      <h6 class="mb-3 text-primary"><i class="far fa-paper-plane text-primary pe-2"></i> 	Apa tujuan ZI? </h6>
      <p>
      ZI bertujuan mewujudkan tata kelola yang baik serta memberikan layanan publik yang prima dan berintegritas. Dengan kata lain, ZI berupaya mencegah terjadinya praktek Korupsi, Kolusi dan Nepotisme (KKN) dan mewujudkan pelayanan prima.
      </p>
    </div>

    <div class="col-md-6 col-lg-4 mb-4">
      <h6 class="mb-3 text-primary"><i class="far fa-paper-plane text-primary pe-2"></i> 	Apakah ZI dan RB itu sama? </h6>
      <p>
      Secara prinsip, ZI dan RB sama yakni sebuah upaya meningkatkan kualitas tata kelola instansi pemerintah. Tetapi secara ruang lingkup ZI merupakan miniatur implementasi RB di tingkat satuan/unit kerja. Sementara, RB bertujuan menciptakan birokrasi pemerintah yang profesional dengan karakteristik, berintegrasi, berkinerja tinggi, bebas dan bersih KKN, mampu melayani publik, netral, sejahtera, berdedikasi, dan memegang teguh nilai-nilai dasar dan kode etik aparatur negara.
      </p>
    </div>

    <div class="col-md-6 col-lg-4 mb-4">
      <h6 class="mb-3 text-primary"><i class="far fa-paper-plane text-primary pe-2"></i> 	Apakah pembangunan ZI adalah kewajiban setiap satuan kerja? </h6>
      <p>
      Ya, karena pada dasarnya setiap instansi pemerintah wajib menerapkan tata kelola yang baik serta memberikan layanan publik yang prima dan berintegritas. Namun demikian, tujuan tersebut tidak mungkin dicapai sekaligus secara bersamaan di seluruh intansi pemerintah yang jumlahnya sangat banyak, sehingga perlu melakukan piloting (percontohan) pada beberapa instansi/satuan kerja/unit kerja.
      </p>
    </div>

    <div class="col-md-6 col-lg-4 mb-4">
      <h6 class="mb-3 text-primary"><i class="far fa-paper-plane text-primary pe-2"></i> 	Benarkah ZI hanya pemenuhan bukti dokumen (evidence)? </h6>
      <p>
      Tidak, karena pada hakikatnya ZI bertujuan mewujudkan tata kelola yang baik serta memberikan layanan publik yang prima dan berintegritas. Adapun, bukti dokumen (evidence) hanyalah gambaran terhadap implementasi ZI.
      </p>
    </div>

    <div class="col-md-6 col-lg-4 mb-4">
      <h6 class="mb-3 text-primary"><i class="far fa-paper-plane text-primary pe-2"></i> 	Apakah ada pedoman pembangunan ZI pada Kementerian Agama? </h6>
      <p>
      Di lingkup nasional, pembangunan ZI perdoman pada Peraturan Menteri PANRB Nomor 90 Tahun 2021 tentang Pembangunan Dan Evaluasi Zona Integritas Menuju Wilayah Bebas Dari Korupsi Dan Wilayah Birokrasi Bersih Dan Melayani Di Instansi Pemerintah. Sementara, di lingkup Kementerian Agama pembangunan ZI berpedoman pada Keputusan Menteri Agama (KMA) Nomor 633 Tahun 2020 tentang Pedoman Pelaksanaan Reformasi Birokrasi pada Kementerian Agama.
      </p>
    </div>

    <div class="col-md-6 col-lg-4 mb-4">
      <h6 class="mb-3 text-primary"><i class="far fa-paper-plane text-primary pe-2"></i> 	Apakah bukti dokumen (evidence) ZI harus sama persis dengan Kamus Evidence PMPZI? </h6>
      <p>
      Tidak harus. Kamus evidence berfungsi sebagai panduan dalam memenuhi bukti dokumen (evidence). Bukti dokumen (evidence) disesuaikan dengan kondisi di masing-masing unit kerja/satuan kerja.
      </p>
    </div>

    <div class="col-md-6 col-lg-4 mb-4">
      <h6 class="mb-3 text-primary"><i class="far fa-paper-plane text-primary pe-2"></i> 	Apa manfaat ZI bagi pegawai? </h6>
      <p>
      ZI merupakan miniatur pelaksanaan RB di tingkat satuan kerja/unit kerja sehingga capaian ZI akan berpengaruh pada indeks RB. Penentuan nominal pembayaran tunjangan kinerja pegawai didasarkan pada indeks RB tingkat instansi/kementerian.
      </p>
    </div>

   
  </div>
</section>
<!--Section: FAQ-->


                    </div>
                </div>
 </section>
