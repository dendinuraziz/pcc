<section class="breadcrumb-area" style="background-color: #F9FAFD;">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-12 align-self-center">
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">PCC</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Daftar Piloting Project ZI</li>
                    </ul>
                    <h2>Daftar Piloting Project ZI</h2>
                </div>
            </div>
        </div>
    </section>


<section class="courses-details-area pd-top-65 pd-bottom-130">
        
            <div class="container">
                <div class="row g-5">
                <div class="potential-area-2 pt-4 pd-bottom-140">
                    <div class="container">
                        <div class="row">
                        <div class="col-lg-12">
                    <div class="category-service">
                        <div class="item">
                            <div class="single-service-wrap">
                                <a href="<?php echo site_url('web/sk?tahun=2020'); ?>">
                                <h6>SK Piloting ZI 2020</h6>
                                <p>2</p>
                                </a>
                            </div>
                        </div>
                        <div class="item">
                            <div class="single-service-wrap">
                            <a href="<?php echo site_url('web/sk?tahun=2021'); ?>">
                                <h6>SK Piloting ZI 2021</h6>
                                <p>2</p>
                                </a>
                            </div>
                        </div>
                       
                        <div class="item">
                            
                            <div class="single-service-wrap">
                            <a href="<?php echo site_url('web/sk?tahun=2022'); ?>">
                                <h6>SK Piloting ZI 2022</h6>
                                <p>2</p>
                                </a>
                            </div>
                        </div>
                       
                        <div class="item">
                            <div class="single-service-wrap">
                            <a href="<?php echo site_url('web/sk?tahun=2023'); ?>">
                                <h6>SK Piloting ZI 2023</h6>
                                <p>2</p>
                                </a>
                            </div>
                        </div>
                       
                       
                    </div>           
                    </div>

                        </div>
                    </div>            
                </div>

                </div>
                </div>
 </section>