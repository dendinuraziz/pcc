<!DOCTYPE html>
<html lang="zxx">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?php echo $title; ?> </title>
   
    <link rel="icon" href="<?php echo base_url(); ?>__statics/img/kemenag.png">

    
    <link rel="stylesheet" href="<?php echo base_url(); ?>__statics/css/animate.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>__statics/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>__statics/css/magnific.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>__statics/css/nice-select.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>__statics/css/owl.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>__statics/css/slick-slide.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>__statics/css/fontawesome.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>__statics/css/style.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>__statics/css/responsive.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>__statics/js/calendar/calendar.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>__statics/js/alert/alert.css">
    <script src="<?php echo base_url(); ?>__statics/js/jquery.3.6.min.js"></script>
	<script src="<?php echo base_url(); ?>__statics/js/alert/alert.js"></script>
    <script src="<?php echo base_url(); ?>__statics/js/calendar/fullcalendar.min.js"></script>
    <script src="<?php echo base_url(); ?>__statics/js/calendar/fullcalendar-custom.js"></script>
    <script type="text/javascript" crossorigin="use-credentials" src="https://alter1-pendidikan.kemenag.go.id/v1/accounts/sso/signal"></script>
    <!--Google Fonts-->
    <link href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;500;600;700;800&display=swap" rel="stylesheet">


</head>
<body class='sc5'>
    <!-- preloader area start -->
    <div class="preloader" id="preloader">
        <div class="preloader-inner">
            <div id="wave1">
            </div>
            <div class="spinner">
                <div class="dot1"></div>
                <div class="dot2"></div>
            </div>
        </div>
    </div> 
    <!-- preloader area end -->
    <div class="body-overlay" id="body-overlay"></div>

    <!-- search popup area start -->
    <div class="search-popup" id="search-popup">
        <form action="home.html" class="search-form">
            <div class="form-group">
                <input type="text" class="form-control" placeholder="Search.....">
            </div>
            <button type="submit" class="submit-btn"><i class="fa fa-search"></i></button>
        </form>
    </div>
    <!-- //. search Popup -->

    <!-- navbar start -->
    <header class="navbar-area">
        <nav class="navbar navbar-expand-lg">
            <div class="container nav-container">
                <div class="responsive-mobile-menu">
                    <button class="menu toggle-btn d-block d-lg-none" data-target="#themefie_main_menu" 
                    aria-expanded="false" aria-label="Toggle navigation">
                        <span class="icon-left"></span>
                        <span class="icon-right"></span>
                    </button>
                </div>
                <div class="logo">
                    <a class="main-logo" href="">
                        <img src="<?php echo base_url(); ?>__statics/img/logo.png"   alt="img">
                        <img src="<?php echo base_url(); ?>__statics/img/logo-zi.png"  class="" alt="img">
                    
                    </a>
                   
                </div>
                <div class="nav-right-part nav-right-part-mobile">
                    <ul>
                        <li><a class="search header-search" href="#"><i class="fa fa-search"></i></a></li>
                    </ul>
                </div>
                <div class="collapse navbar-collapse" id="themefie_main_menu">
                    
                    <div class="single-input-wrap">
                      <form action="<?php echo site_url('search'); ?>" class="search-form">
                        <input type="text" name="q" placeholder="Cari  disini " value="<?php echo isset($_GET['q']) ? $_GET['q']:""; ?>">
                        <button type="submit"><i class="fa fa-search"></i></button>
                     </form>
                    </div>
                    <ul class="navbar-nav menu-open text-end">
                        <li>
                            <a href="<?php echo base_url(); ?>"><i class="fa fa-home"></i>  </a>
                        </li>   
                        <li class="menu-item-has-children">
                            <a href="#">Pendis Care Center (PCC) </a>
                            <ul class="sub-menu">
                               
                                <li> <a href="<?php echo site_url("web/dasarhukum"); ?>">Dasar Hukum</a></li>
                                <li> <a href="<?php echo site_url("web/piloting"); ?>">Daftar Pilot Project ZI</a></li>
                                <li> <a href="<?php echo site_url("web/timeline"); ?>">Time Line Pembangunan </a></li>
                                <li> <a href="<?php echo site_url("web/kamus"); ?>">Kamus Evidence</a></li>
                                <li> <a href="<?php echo site_url("web/template"); ?>">Template Evidence</a></li>
                                <li> <a href="<?php echo base_url(); ?>#tips">Tips dan Trik ZI</a></li>
                                <li> <a href="<?php echo site_url("web/faq"); ?>">Frequently Asked Question (FAQ)</a></li>
                                <li> <a href="<?php echo site_url("web/kontak"); ?>">Kontak Center</a></li>
                               
                            </ul>
                        </li>                    
                        
                       
                        
                    </ul>
                </div>

                <div class="nav-right-part nav-right-part-desktop">
                    <ul>
                   
                            <li><a href="https://emis.kemenag.go.id/login?continue=https://pendis.kemenag.go.id" class="btn btn-base-light scroll-to"><i class="fa fa-lock"></i> Login SSO   </a></li>
                          
                       
                        
                       
                    </ul>
                </div>


               
            </div>
        </nav>
    </header>
  <br>
  <br>
  <br>
  
    <div class="category-navbar navbar-area d-xl-block d-none">
        <nav class="navbar navbar-expand-lg">
            <div class="container nav-container">
                <div class="collapse navbar-collapse">
                    <ul class="navbar-nav menu-open">
						
                                 <li> <a href="<?php echo base_url(); ?>#tutorial" class="scroll-to">Tutorial </a></li>
                                 <li> <a href="<?php echo site_url("web/dasarhukum"); ?>">Dasar Hukum</a></li>
                                <li> <a href="<?php echo site_url("web/piloting"); ?>">Daftar Pilot Project ZI</a></li>
                                <li> <a href="<?php echo site_url("web/timeline"); ?>">Linimasa Pembangunan </a></li>
                                <li> <a href="<?php echo site_url("web/kamus"); ?>"> Eviden</a></li>
                                <li> <a href="<?php echo base_url(); ?>#tips">Tips dan Trik </a></li>
                                <li> <a href="<?php echo site_url("web/faq"); ?>">FAQ</a></li>
                                <li> <a href="<?php echo site_url("web/kontak"); ?>">Kontak Center</a></li>
                               
							
                        
                        
                       
                    </ul>
                </div>
            </div>
        </nav>
    </div>
    <!-- navbar end -->

    <?php  $this->load->view($konten); ?>

    <!-- footer area start -->
    <footer class="footer-area">
       
        <!--Footer bottom-->
        <div class="container">
            <div class="footer-bottom">
                <div class="row">
                    <div class="col-xl-7 align-self-center">
                        <div class="d-md-flex align-items-center mb-4 mb-xl-0">
                            <div class="logo d-inline-block">
                                <img src="<?php echo base_url(); ?>__statics/img/logo.png" width="50px" alt="img">
                            </div>
                            <div class="copyright-area">
                                <p>© 2023 - Kementerian Agama RI</p>       
                            </div>
                        </div>                        
                    </div>
                    <div class="col-xl-5 align-self-center text-xl-end">
                        <ul class="social-area d-inline-block">
                            <li><a class="active" href="#"><i class="fab fa-facebook-f"></i></a></li>
                            <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
                            <li><a href="#"><i class="fab fa-instagram"></i></a></li>
                            <li><a href="#"><i class="fab fa-dribbble"></i></a></li>
                            <li><a href="#"><i class="fab fa-behance"></i></a></li>
                        </ul>
                       
                    </div>
                </div>                
            </div>
        </div>
        <!--Footer bottom-->
    </footer>
    <!-- footer area end -->    

    <!-- back-to-top end -->
    <div class="back-to-top">
        <span class="back-top"><i class="fas fa-angle-double-up"></i></span>
    </div>

    

    <!-- all plugins here -->
   
    <script src="<?php echo base_url(); ?>__statics/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>__statics/js/imageloded.min.js"></script>
    <script src="<?php echo base_url(); ?>__statics/js/counterup.js"></script>
    <script src="<?php echo base_url(); ?>__statics/js/waypoint.js"></script>
    <script src="<?php echo base_url(); ?>__statics/js/magnific.min.js"></script>
    <script src="<?php echo base_url(); ?>__statics/js/isotope.pkgd.min.js"></script>
    <script src="<?php echo base_url(); ?>__statics/js/nice-select.min.js"></script>
    <script src="<?php echo base_url(); ?>__statics/js/fontawesome.min.js"></script>
    <script src="<?php echo base_url(); ?>__statics/js/ripple.js"></script>
    <script src="<?php echo base_url(); ?>__statics/js/owl.min.js"></script>
    <script src="<?php echo base_url(); ?>__statics/js/slick-slider.min.js"></script>
    <script src="<?php echo base_url(); ?>__statics/js/wow.min.js"></script>
    <!-- main js  -->
    <script src="<?php echo base_url(); ?>__statics/js/main.js"></script>

    <script type="text/javascript">

		var revapi;

		jQuery(document).ready(function() {

			   revapi = jQuery('.tp-banner').revolution(
				{
					delay:9000,
					startwidth:1170,
					startheight:500,
					hideThumbs:10,
					lazyLoad:"on"

				});


           
                


		});	//ready


        

        console.log($EMIS);
	</script>

    
</body>
</html>