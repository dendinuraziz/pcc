   <div class="app-main__inner">
                    <div class="app-page-title">
                        <div class="page-title-wrapper">
                            <div class="page-title-heading">
                                <div class="page-title-icon">
                                    <i class="fa fa-laptop icon-gradient bg-mean-fruit">
                                    </i>
                                </div>
                                <div> Dashboard - Content Management System Pelatihan
                                    <div class="page-title-subheading">Selamat Datang <?php echo $_SESSION['admin']; ?>, Anda login sebagai <?php echo $_SESSION['status']; ?>
                                    </div>
                                </div>
                            </div>
                            <div class="page-title-actions">
                                <button type="button" data-toggle="tooltip" title="Example Tooltip" data-placement="bottom" class="btn-shadow mr-3 btn btn-dark">
                                    <i class="fa fa-star"></i>
                                </button>
                                <div class="d-inline-block dropdown">
                                    <button type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn-shadow dropdown-toggle btn btn-info">
                                        <span class="btn-icon-wrapper pr-2 opacity-7">
                                            <i class="fa fa-business-time fa-w-20"></i>
                                        </span>
                                        GTK Madrasah
                                    </button>
                                    
                                </div>
                            </div>    </div>
                    </div>            
					
					
                    <div class="tabs-animation">
                        <div class="mb-3 card">
                            <div class="card-header-tab card-header">
                                <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                                    <i class="header-icon lnr-charts icon-gradient bg-happy-green"> </i>
                                    Jumlah Pelatihan 
                                </div>
                                <div class="btn-actions-pane-right text-capitalize">
                                    <button class="btn-wide btn-outline-2x mr-md-2 btn btn-outline-focus btn-sm">View All</button>
                                </div>
                            </div>
                            <div class="no-gutters row">
							
							
                                <div class="col-sm-6 col-md-6 col-xl-6">
                                    <div class="card no-shadow rm-border bg-transparent widget-chart text-left">
                                        <div class="icon-wrapper rounded-circle">
                                            <div class="icon-wrapper-bg opacity-10 bg-warning"></div>
                                            <i class="fa fa-laptop text-dark opacity-8" style="color:white"></i></div>
                                        <div class="widget-chart-content">
                                            <div class="widget-subheading">Jumlah Pelatihan</div>
                                            <div class="widget-numbers">
                                                <?php 
                                                  $jmlPelatihan = $this->db->query("select count(id) as jml  from pelatihan")->row();
                                                  echo $jmlPelatihan->jml; 
                                                ?>
                                            </div>
                                            <div class="widget-description opacity-8 text-focus">
                                                <div class="d-inline text-danger pr-1">
                                                    <i class="fa fa-angle-down"></i>
                                                    <span class="pl-1"></span>
                                                </div>
                                               
                                            </div>
                                        </div>
                                    </div>
                                    <div class="divider m-0 d-md-none d-sm-block"></div>
                                </div>
								
								<div class="col-sm-6 col-md-6 col-xl-6">
                                    <div class="card no-shadow rm-border bg-transparent widget-chart text-left">
                                        <div class="icon-wrapper rounded-circle">
                                            <div class="icon-wrapper-bg opacity-10 bg-warning"></div>
                                            <i class="fa fa-laptop text-dark opacity-8" style="color:white"></i></div>
                                        <div class="widget-chart-content">
                                            <div class="widget-subheading"> Jumlah Peserta </div>
                                            <div class="widget-numbers"> <?php 
                                                  $jmlPelatihan = $this->db->query("select count(id) as jml from peserta")->row();
                                                  echo $jmlPelatihan->jml; 
                                                ?></div>
                                            <div class="widget-description opacity-8 text-focus">
                                                <div class="d-inline text-danger pr-1">
                                                    <i class="fa fa-angle-down"></i>
                                                    <span class="pl-1"></span>
                                                </div>
                                               
                                            </div>
                                        </div>
                                    </div>
                                    <div class="divider m-0 d-md-none d-sm-block"></div>
                                </div>
                               
                             
							
							
							
                            </div>
                            
                        </div>


                        <?php            
				
                $service = new Google_Drive();   
                if(!$service->statusApi()){
                    $authlink = $service->createAuthUrl();
                    $buttonlink =  "<a href=\"" . $authlink . "\" target=\"_googleAuthLogin\" class=\"btn btn-danger \">\n   <i class=\"fa fa-ban\"></i>\n Get Token GDrive </a>";
                }else{
                    $buttonlink = "";
                }  
                
                
                echo "  <div class=\"row\">";
                  if($buttonlink==""){
                      $dataakun = $service->getOuthData();
                      if(empty($dataakun)){
                          $dataakun['picture'] = "https://www.gstatic.com/images/branding/product/1x/drive_48dp.png";
                          $dataakun['name'] = "Google Akun";
                      }
                    echo "<div class=\"col-md-12\">
                      <div class=\"card\">
                      <div class=\"card-header card-header-success\">
                      <h4 class=\"card-title\">Pengaturan Upload Goggle Drive</h4>
                      <p class=\"card-category\"> Form ini digunakan untuk mengaktifkan upload gdrive  </p>
                      </div>	                  
                      <div class=\"card-body\">
                      <div class=\"col-md-12\">
                      <div class=\"row\"> 
                      <div class=\"col-md-3\">
                      <div  style=\"max-width: 130px;max-height: 130px;margin: 0px auto 0;border-radius: 50%; 
                    overflow: hidden;padding: 0;box-shadow: 0 16px 38px -12px rgba(0,0,0,.56), 0 4px 25px 0 rgba(0,0,0,.12), 0 8px 10px -5px rgba(0,0,0,.2);\">
                                  <a ><img  style=\"width: 100%;height: auto;\" src=\"".$dataakun['picture']."\" >
                                  </a>
                               </div>
                                  <br><h4 style=\"text-align: center;\">".$dataakun['name']."</h4>
                                 </div>";	  
                                 
                      echo "<div class=\"col-md-9\"><p class=\"card-category\"> Upload Google Drive sudah aktif  </p>";
                      echo "<p class=\"card-category\"> Untuk memindahkan file bahan ajar dan tugas-tugas dari server lokal ke GDrive, Silahkan Klik Migrasi File <br> Namun Anda tidak wajib melakukan migrasi file, File-file yang diupload seterusnya akan diarahkan ke google drive <br><b> Jika penyimpanan file ingin menggunakan lokal storage, silahkan nonaktifkan penyimpanan google drive </b> </p>";
                      echo "<a href=\"" . site_url("schoolmadrasah/hapusGdrive") . "\" class=\"btn btn-danger \" onclick=\"return confirm('Jika Anda mengganti akun, Anda akan kehilangan akses terhadap akun gsuite unlimited default dari pusat, apakah Anda yakin ?') \" >\n   <i class=\"fa fa-trash\"></i>\n Nonaktifkan Penyimpanan Google Drive</a>";   
                      echo "<a href=\"" . site_url("GSync") . "\" target=\"_migrasiGD\" class=\"btn btn-success pull-right\">\n   <i class=\"fa fa-send\"></i>\n Migrasi File </a>";  
                    echo"               </div>\r\n</div>\r\n</div>\r\n  </div>\r\n              </div>\r\n            </div></div>\r\n         \r\n         \r\n  "; 
                      
                  }else{
                  echo "<div class=\"col-md-12\">
                      <div class=\"card\">
                      <div class=\"card-header card-header-success\">
                      <h4 class=\"card-title\">Pengaturan Upload GDrive</h4>
                      <p class=\"card-category\"> Form ini digunakan untuk mengaktifkan upload gdrive  </p>
                      </div>
                      <div class=\"card-body\"> ";
                      echo "<p > Untuk mendapatkan token, silahkan klik  <b> Get Token GDrive </b>. Kemudian pilih akun gmail yang akan di integrasikan. Setelah muncul kode token, Isikan ke form berikut dan Klik Simpan </p>";
                echo "  <form  method=\"post\" action=\"";
                echo site_url("schoolmadrasah/authapisave");
                echo "\">\r\n                    <div class=\"row\">\r\n                      <div class=\"col-md-12\">\r\n                        <div class=\"form-group\">\r\n 
                <input type=\"text\" class=\"form-control\" name=\"userauth\" placeholder=\"Masukkan Token Gdive\"  title=\"Masukkan NSM \" data-placement=\"right\">
                \r\n                    \r\n                    \r\n                    \r\n                        </div>\r\n                      </div>\r\n                     \r\n                      \r\n                    </div>\r\n                      \r\n            \r\n                  \r\n".$buttonlink."                    <button type=\"submit\" class=\"btn btn-success pull-right\">Simpan Token</button>\r\n                    <div class=\"clearfix\"></div>\r\n                  </form>\r\n ";
                echo"               </div>\r\n              </div>\r\n            </div></div>\r\n         \r\n         \r\n  "; 
                  }
                
                                    ?>
						
						
                      
					  
                    </div>
                </div>
          

              