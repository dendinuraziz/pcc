<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Language" content="en">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title><?php echo $title; ?> </title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no"
    />
    <meta name="description" content="This is an example dashboard created using build-in elements and components.">

    
    <meta name="msapplication-tap-highlight" content="no">
	<link rel="shortcut icon" href="<?php echo base_url()."__statics/img/logo.png"; ?>">
	<link href="<?php echo base_url(); ?>__statics/admin/main.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>__statics/fa/css/all.css" rel="stylesheet" type="text/css">
  
	<link href="<?php echo base_url(); ?>__statics/js/alertify/css/alertify.css" rel="stylesheet">
	<link rel="stylesheet" href="<?php echo base_url(); ?>__statics/js/alert/alert.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>__statics/js/bootstrap-toastr/toastr.min.css"/>
  
	<script src="<?php echo base_url(); ?>__statics/js/jquery.min.js"></script>
	

	<script src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js" type="text/javascript"></script>
	<script src="https://cdn.datatables.net/buttons/1.7.1/js/dataTables.buttons.min.js" type="text/javascript"></script>
	<script src="https://cdn.datatables.net/1.10.25/js/dataTables.bootstrap4.min.js" type="text/javascript"></script>
	<script src="https://cdn.datatables.net/buttons/1.7.1/js/buttons.bootstrap4.min.js" type="text/javascript"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" type="text/javascript"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js" type="text/javascript"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js" type="text/javascript"></script>
	<script src="https://cdn.datatables.net/buttons/1.7.1/js/buttons.html5.min.js" type="text/javascript"></script>
	<script src="https://cdn.datatables.net/buttons/1.7.1/js/buttons.print.min.js" type="text/javascript"></script>
	<script src="https://cdn.datatables.net/buttons/1.7.1/js/buttons.colVis.min.js" type="text/javascript"></script>
	<script src="<?php echo base_url(); ?>__statics/js/alert/alert.js"></script>
    <link href="<?php echo base_url(); ?>__statics/js/upload/css/fileinput.css" media="all" rel="stylesheet" type="text/css"/>
    <script> var base_url = "<?php echo base_url(); ?>"; </script>
    <script src="<?php echo base_url(); ?>__statics/js/proses.js"></script>	
    <script src="<?php echo base_url(); ?>__statics/js/upload/js/fileinput.js" type="text/javascript"></script>
    
	<script type="text/javascript" src="<?php echo base_url(); ?>__statics/ckeditor/ckeditor.js" ></script>  
</head>
<body>
<div class="app-container app-theme-white body-tabs-shadow fixed-header fixed-sidebar">
    <div class="app-header header-shadow">
        <div class="app-header__logo">
            <div class="logo-src"><img src="<?php echo base_url(); ?>__statics/img/logo.png" alt="" style="height:30px" class="img-responsive"> </div><b >CMS PANEL</b>
            <div class="header__pane ml-auto">
                <div>
                    <button type="button" class="hamburger close-sidebar-btn hamburger--elastic" data-class="closed-sidebar">
                        <span class="hamburger-box">
                            <span class="hamburger-inner"></span>
                        </span>
                    </button>
                </div>
            </div>
        </div>
        <div class="app-header__mobile-menu">
            <div>
                <button type="button" class="hamburger hamburger--elastic mobile-toggle-nav">
                    <span class="hamburger-box">
                        <span class="hamburger-inner"></span>
                    </span>
                </button>
            </div>
        </div>
        <div class="app-header__menu">
            <span>
                <button type="button" class="btn-icon btn-icon-only btn btn-primary btn-sm mobile-toggle-header-nav">
                    <span class="btn-icon-wrapper">
                        <i class="fa fa-ellipsis-v fa-w-6"></i>
                    </span>
                </button>
            </span>
        </div>    
		<?php $this->load->view("navbar"); ?>
    </div>    
	<div class="ui-theme-settings">
        <button type="button" id="TooltipDemo" class="btn-open-options btn btn-warning">
            <i class="fa fa-cog fa-w-16 fa-spin fa-2x"></i>
        </button>
     </div> 

	 <div class="app-main">
            <div class="app-sidebar sidebar-shadow">
                <div class="app-header__logo">
                    <div class="logo-src"></div>
                    <div class="header__pane ml-auto">
                        <div>
                            <button type="button" class="hamburger close-sidebar-btn hamburger--elastic" data-class="closed-sidebar">
                                <span class="hamburger-box">
                                    <span class="hamburger-inner"></span>
                                </span>
                            </button>
                        </div>
                    </div>
                </div>
                <div class="app-header__mobile-menu">
                    <div>
                        <button type="button" class="hamburger hamburger--elastic mobile-toggle-nav">
                            <span class="hamburger-box">
                                <span class="hamburger-inner"></span>
                            </span>
                        </button>
                    </div>
                </div>
                <div class="app-header__menu">
                    <span>
                        <button type="button" class="btn-icon btn-icon-only btn btn-primary btn-sm mobile-toggle-header-nav">
                            <span class="btn-icon-wrapper">
                                <i class="fa fa-ellipsis-v fa-w-6"></i>
                            </span>
                        </button>
                    </span>
                </div>    
				
				
				<?php $this->load->view("menu"); ?>
		        </div>   
				
				
				<div class="app-main__outer">
				
				 <div id="konten"><?php $this->load->view($konten); ?></div>
				 
				 
                   <div class="app-wrapper-footer">
                    
                </div>   
			</div>
    </div>
</div>

<div class="app-drawer-overlay d-none animated fadeIn"></div>
<script type="text/javascript" src="<?php echo base_url(); ?>__statics/admin/js.js"></script>
<script src="<?php echo base_url(); ?>__statics/js/alertify/alertify.js"></script>
<script src="<?php echo base_url(); ?>__statics/js/jquery.blockui.min.js"></script>
<script src="<?php echo base_url(); ?>__statics/js/alertify/alertify.js"></script>
<script src="<?php echo base_url(); ?>__statics/js/bootstrap-toastr/toastr.min.js"></script>
<script type="text/javascript">
  
	
		$(document).off("click",".menuajax").on("click",".menuajax",function (event, messages) {
		
	//	$(".menuajax").click(function(event){
	           event.preventDefault();
			   var url = $(this).attr("href");
			    var title = $(this).attr("title");
			  
			 
				   
				   
			    $("li").siblings().removeClass('active');
			    $(this).parent().addClass('active');
			    
				
			  	  
			   $("#konten").html('....');
		      loading();
			  $.post(url,{ajax:"yes"},function(data){
				  
				
				  history.replaceState(title, title, url);
				  $('title').html(title);
				
				  $("#konten").html(data);
				 
				 
				    jQuery.unblockUI({ });
				 
			  })
		  })
  </script>
</body>

</html>
