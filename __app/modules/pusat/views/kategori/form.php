
  <script src="<?php echo base_url(); ?>__statics/ckfinder/ckfinder.js"></script>


<div class="app-main__inner">
                    <div class="app-page-title">
                        <div class="page-title-wrapper">
                            <div class="page-title-heading">
                                <div class="page-title-icon">
                                    <i class="fa fa-users icon-gradient bg-tempting-azure">
                                    </i>
                                </div>
                                <div><?php echo $title; ?>
								<div class="page-title-subheading">
									
                                    </div>
                                    
                                </div>
                            </div>
                            
						</div>
                    </div>      
					<div id="showform"></div>

					<div class="mb-3 card">
					
					
					 
                                            <div class="card-body">

											<div class="d-block text-left ">
						<button class="mb-2 mr-2 btn-pill btn-transition btn btn-outline-primary menuajax " href="<?php echo site_url("pusat/kategori"); ?>"  title="Kategori Pelatihan"> <i class="fa fa-history"> </i> Kembali</button>

						
					</div>


                                                <ul class="tabs-animated-shadow tabs-animated nav">
                                                    <li class="nav-item">
                                                        <a role="tab" class="nav-link  active" id="tab-c-0" data-toggle="tab" href="#tab-animated-0">
                                                            <span>Form Kategori </span>
                                                        </a>
                                                    </li>
                                                    
                                                    
                                                </ul>
                                                <div class="tab-content">
                                                    <div class="tab-pane active " id="tab-animated-0" role="tabpanel">
                                                        
													<form id="simpangambar" name="simpangambar" method="post" enctype="multipart/form-data" url="<?php echo site_url("pusat/saveKategori"); ?>">
														<div class="form-row">
															<div class="col-md-12">
																<div class="position-relative form-group">
																	<label for="exampleEmail11" class="">Nama Kategori</label>
																	<input name="id"  value="<?php echo isset($data) ? $data->nama:""; ?>"  type="hidden" class="form-control">
																	<input name="f[nama]" id="namaPelatihan"  placeholder="Masukkan Nama Kategori disini " value="<?php echo isset($data) ? $data->nama:""; ?>"  type="text" class="form-control">
																</div>
															</div>
															
														</div>

														<div class="form-row">
															<div class="col-md-12">
																<div class="position-relative form-group">
																	<label for="exampleEmail11" class="">Slug </label>
																	<input name="f[slug]" id="slug" readonly  value="<?php echo isset($data) ? $data->slug:""; ?>"   type="text" class="form-control">
																</div>
															</div>
															
														</div>
													
														

														<div class="d-block text-right card-footer">
															<button class="mr-2 btn btn-link btn-sm">Cancel</button>
															<button class="btn btn-success btn-lg"><i class="fa fa-save"></i> Simpan Data</button>
														</div>
													     	
													</form>


                                                    </div>
                                                   
                                                   
                                                </div>
                                            </div>
                                        </div>
					
				  
				  </div>


                  
</div>
 		
  <script type="text/javascript">
			   $(document).on("input","#namaPelatihan",function(){
				  
					var nilai = $(this).val();
					

					
				var slug = nilai.toLowerCase()
							.replace(/ /g, '-')
							.replace(/[^\w-]+/g, '');
						$("#slug").val(slug);

				  
				});
				
																
</script>