    
<form class="" action="<?php echo site_url('pusat/saveAktivitas'); ?>" method="post">

												<div class="form-row">
															<div class="col-md-12">
																<div class="position-relative form-group">
																	<label for="exampleEmail11" class=""> Topik </label>
                                                                    
																	<input   value="<?php echo $topik->nama; ?>" type="text" readonly disabled class="form-control">
																</div>
															</div>
															
														</div>


														<div class="form-row">
															<div class="col-md-12">
																<div class="position-relative form-group">
																	<label for="exampleEmail11" class="">Nama Aktivitas</label>
                                                                    <input type="hidden" name="id" value="<?php echo isset($aktivitas) ?  $aktivitas->id:""; ?>">
                                                                    <input type="hidden" name="silabus_id" value="<?php echo $topik->id; ?>">
                                                                    <input type="hidden" name="pelatihan_id" value="<?php echo $topik->pelatihan_id; ?>">
																	<input name="f[nama]" id="namaAktivitas" required  placeholder="Contoh : Selamat Datang di E-learning" value="<?php echo isset($aktivitas) ?  $aktivitas->nama:""; ?>" type="text" class="form-control">
																</div>
															</div>
															
														</div>

														<div class="form-row">
															<div class="col-md-12">
																<div class="position-relative form-group">
																	<label for="exampleEmail11" class="">Slug </label>                                                                   
																	<input name="f[slug]" id="slugAktivitas" required readonly  value="<?php echo isset($aktivitas) ?  $aktivitas->slug:""; ?>" type="text" class="form-control">
																</div>
															</div>
															
														</div>

														<div class="form-row">
															<div class="col-md-12">
																<div class="position-relative form-group">
																	<label for="exampleEmail11" class="">Kunci Aktivitas Sesudah </label>
																	<select name="f[before]" required  class="form-control">
																		<option value="0"> Default </option>
																		<?php 
																		 $subsilabus = $this->db->query("select id,nama from v_silabus where pelatihan_id='{$topik->pelatihan_id}' order by id asc")->result();
																			foreach($subsilabus as $subsil){
																				?><option value="<?php echo $subsil->id; ?>" <?php if(isset($aktivitas)){ echo ($subsil->id==$aktivitas->before) ? "selected":""; }else{  echo ($subsil->id==$kunci) ? "selected":""; } ?>> <?php echo $subsil->id; ?> - <?php echo $subsil->nama; ?> </option><?php 

																			}
																		?>
																	</select>
																</div>
															</div>
															
														</div>

														<div class="form-row">
															<div class="col-md-12">
																<div class="position-relative form-group">
																	<label for="exampleEmail11" class="">Urutan</label>
																	<input name="f[urutan]" id="slug" required   value="<?php echo isset($aktivitas) ?  $aktivitas->urutan: $urutan; ?>" type="text" class="form-control">
																</div>
															</div>
															
														</div>

														<div class="form-row">
															<div class="col-md-12">
																<div class="position-relative form-group">
																	<label for="exampleEmail11" class="">Durasi (Dalam Menit)</label>
																	<input name="f[durasi]"  required   value="<?php echo isset($aktivitas) ?  $aktivitas->durasi: ""; ?>" type="text" class="form-control">
																</div>
															</div>
															
														</div>

                                                        <div class="d-block text-right card-footer">
															<button class="mr-2 btn btn-link btn-sm" data-dismiss="modal" aria-label="Close">Cancel</button>
															<button class="btn btn-success btn-lg"><i class="fa fa-save"></i> Simpan Perubahan</button>
														</div>

														<script>
															$(document).on("input","#namaAktivitas",function(){
				  
																	var nilai = $(this).val();
																	

																	
																var slug = nilai.toLowerCase()
																			.replace(/ /g, '-')
																			.replace(/[^\w-]+/g, '');
																		$("#slugAktivitas").val(slug);

																	
																});
														</script>
</form>