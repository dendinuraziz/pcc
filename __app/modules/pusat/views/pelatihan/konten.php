
  <script src="<?php echo base_url(); ?>__statics/ckfinder/ckfinder.js"></script>


<div class="app-main__inner">
<div class="d-block text-right ">
						<a class="mb-2 mr-2 btn-pill btn-transition btn btn-outline-primary  " href="<?php echo site_url("pusat/edit/".$aktivitas->pelatihan_id.""); ?>"  title="Daftar Pelatihan"> <i class="fa fa-history"> </i> Kembali</a>

						
					</div>
                    <div class="app-page-title">
                        <div class="page-title-wrapper">
                            <div class="page-title-heading">
                                <div class="page-title-icon">
                                    <i class="fa fa-users icon-gradient bg-tempting-azure">
                                    </i>
                                </div>
                                <div>Konten AKtivitas
								<div class="page-title-subheading">
									 <?php echo $aktivitas->nama; ?>
                                    </div>
                                    
                                </div>
                            </div>
                            
						</div>
                    </div>      
					<div id="showform"></div>

					<div class="mb-3 card">
					
				
					 
                                            <div class="card-body">
												
                                                <ul class="tabs-animated-shadow tabs-animated nav">
                                                    <li class="nav-item">
                                                        <a role="tab" class="nav-link active" id="tab-c-0" data-toggle="tab" href="#tab-animated-0">
                                                            <span>Informasi Aktivitas </span>
                                                        </a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a role="tab" class="nav-link " id="tab-c-1" data-toggle="tab" href="#kontenAktivitasData">
                                                            <span>Konten</span>
                                                        </a>
                                                    </li>
                                                    
                                                </ul>
												<form method="post" id="simpanKonten" action="<?php echo site_url('pusat/saveKonten'); ?>" enctype="multipart/form-data" >
                                                <div class="tab-content">
												
                                                    <div class="tab-pane active " id="tab-animated-0" role="tabpanel">
                                                        
													
														<div class="form-row">
															<div class="col-md-12">
																<div class="position-relative form-group">
																	<label for="exampleEmail11" class="">Aktivitas</label>
																	<input   value="<?php echo $aktivitas->nama; ?>" type="text" readonly disabled class="form-control">
																	<input id="id" name="id"  value="<?php echo isset($detail) ?  $detail->id:""; ?>" type="hidden" class="form-control">
																	<input name="f[pelatihan_id]" id="pelatihan_id"  value="<?php echo $aktivitas->pelatihan_id; ?>" type="hidden" class="form-control">
																	<input name="f[subsilabus_id]"  value="<?php echo $aktivitas->id; ?>" type="hidden" class="form-control">
																	<input name="f[slug]"  value="<?php echo $aktivitas->slug; ?>" type="hidden" class="form-control">
																</div>
															</div>
															
														</div>

														<div class="form-row">
															<div class="col-md-6">
																<div class="position-relative form-group">
																	<label for="exampleEmail11" class="">Slug Pelatihan</label>
																	<input name="f[slug]" readonly   value="<?php echo $aktivitas->slug; ?>" type="text" class="form-control">
																</div>
															</div>

															

															<div class="col-md-6">
																<div class="position-relative form-group">
																	<label for="exampleCity" class="">Jenis Konten</label>
																	<select class="form-control" name="f[konten_belajar]" id="konten_belajar">
																	    <option value="">- Pilih Jenis -</option>
																	
																			
																			<?php 
																			 $kategori = $this->db->get("konten_belajar")->result();
																			   foreach($kategori as $rk){

																				?><option value="<?php echo $rk->id; ?>" <?php if(isset($detail)){ echo ($rk->id==$detail->konten_belajar) ? "selected":""; } ?>><?php echo $rk->nama; ?></option><?php 
																			   }
																			 ?>
																			
																			
																	
																		
																	</select>
																</div>
															</div>
															
														</div>
													
														

														<div class="form-row">
															<div class="col-md-12">
																<div class="position-relative form-group">
																	<label for="exampleEmail11" class="">Deskripsi Pelatihan</label>
																	<textarea id="deskripsi" name="f[deskripsi]"><?php echo isset($detail) ?  $detail->deskripsi:""; ?></textarea>
																</div>
															</div>
															
														</div>

														

														


                                                    </div>
                                                    <div class="tab-pane " id="kontenAktivitasData" role="tabpanel">
														<?php 
														
														if(isset($detail)){
															if($detail->konten_belajar==1 or $detail->konten_belajar==9 or $detail->konten_belajar==10){
																$this->load->view("pelatihan/konten/area");
															}else if($detail->konten_belajar==4 or $detail->konten_belajar==5 or $detail->konten_belajar==6){
																$this->load->view("pelatihan/konten/link");

															}else{

																$this->load->view("pelatihan/konten/upload");
															}
															

														}
														
														?>
														
                                                    
                                                    </div>

													    <div class="d-block text-right card-footer">
															<button class="mr-2 btn btn-link btn-sm">Cancel</button>
															<button class="btn btn-success btn-lg"><i class="fa fa-save"></i> Simpan Perubahan</button>
														</div>
													     	
													
                                                   
                                                </div>
												</form>
                                            </div>
                                        </div>
					
				  
				  </div>


                  
</div>
 		
  <script type="text/javascript">
		
				var base_url="<?php echo base_url(); ?>";
	
				var editor = CKEDITOR.replace( 'deskripsi', {
				removeButtons : 'Print,NewPage,Save,Cut,Copy,PasteText,Undo,Redo,SelectAll,SpellChecker,RemoveFormat,CreateDiv,Outdent,Indent,BidiRtl,BidiLtr,Anchor,PageBreak,Styles,Format,JustifyBlock,JustifyCenter,HorizontalRule,Subscript',
																	          removePlugins: 'about,showblocks,forms,preview,find,smiley,scayt,specialchar,templates,flash',
																			 
																			  uploadUrl: base_url+'__statics/ckdrive/core/connector/php/connector.php?command=QuickUpload&type=Files&responseType=json&show=1',
																			  filebrowserBrowseUrl: base_url+'__statics/ckdrive/ckfinder.html',
																			  filebrowserImageBrowseUrl: base_url+'__statics/ckdrive/ckfinder.html?type=Images&show=1',
																			  filebrowserUploadUrl: base_url+'__statics/ckdrive/core/connector/php/connector.php?command=QuickUpload&type=Files&show=1',
																			  filebrowserImageUploadUrl: base_url+'__statics/ckdrive/core/connector/php/connector.php?command=QuickUpload&type=Images&show=1',
																			  height: 300,
																			  removeDialogTabs: 'image:advanced;link:advanced',
																			   allowedContent :true
																} );
			$(document).off("change","#konten_belajar").on("change","#konten_belajar",function(){
				$('#simpanKonten').trigger('submit');
				var id = $(this).val();

				$.post("<?php echo site_url('pusat/konten_belajar'); ?>",{id:id},function(data){

					$("#kontenAktivitasData").html(data);
				})


			});



$(document).on('submit', 'form#simpanKonten', function (event, messages) {
	 event.preventDefault();
       var form   = $(this);
       var urlnya = $(this).attr("action");
	
	   var formData = new FormData(this);
   
	  loading();
        $.ajax({
            type: "POST",
            url: urlnya,
            
			data: formData, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
			contentType: false,       // The content type used when sending data to the server.
			cache: false,             // To unable request pages to be cached
			processData:false,    
			success: function (response, status, xhr) {
                
				$("#id").val(response);

				jQuery.unblockUI({ });
            }
        });

        return false;
    });	
			


																
</script>