

<div class="app-main__inner">
                    <div class="app-page-title">
                        <div class="page-title-wrapper">
                            <div class="page-title-heading">
                                <div class="page-title-icon">
                                    <i class="fa fa-users icon-gradient bg-tempting-azure">
                                    </i>
                                </div>
                                <div><?php echo $title; ?>
                                    <div class="page-title-subheading">
									 Daftar Pelatihan
                                    </div>
                                </div>
                            </div>
                            
						</div>
                    </div>      
					<div id="showform"></div>
					<div class="main-card mb-3 card">
					    
                        <div class="card-body">
						<a class="mb-2 mr-2 btn-icon btn-shadow btn-dashed btn btn-outline-primary" href="<?php echo site_url("pusat/tambahPelatihan"); ?>"><i class="fa fa-plus"> </i> Tambah Pelatihan </a>
						
						    <div class="row right" >
							 
							   
							  
							
							
							  <div class="col-md-3">
							  <div class="input-group">
									<input type="text" id="key" class="form-control" placeholder="Cari Nama Pelatihan">
									
							  </div>
							  
							  
							  </div>
							  <div class="col-md-2">
							  
									
										<button class="btn btn-primary btn-icon" id="pencarian" type="button"><span class="fa fa-search"></span> Tampilkan </button>
							
							  </div>
							  
							 
							 </div>
						<hr>	 
							 
				  <div class="row">
				  
				  
				   <div class="col-md-12">
				     <div class="table-responsive">
					   <table class="table table-hover table-bordered table-striped" id="madrasahsasaran" style="font-size:12px">
					    <thead>
						  <tr>
						    <th> No </th>
						    <th> Kategori </th>
						    <th> Nama Pelatihan </th>
						    <th> Jenis </th>
						    <th> Modul </th>
						   
						    <th> Aktivasi </th>
						    <th> Peserta </th>
						    <th> Action </th>
						   
						 </tr>
						</thead>
					   
					   </table>
					 
					 
					 </div>
				   
				   </div>
				  
				  
				  </div>
                  
                    </div>
                </div>
				
  <script type="text/javascript">

  
$(document).off("click",".aktivasi").on("click",".aktivasi",function(){
	            
				
				var tmujian_id = $(this).attr("tmujian_id");
				if($(this).is(":checked")){
							 var status =1;
							 
						}else{
	   
							var status =0;
							
				 }
  
				alertify.confirm("Anda akan mengaktifkan ujian ini ? ",function(){
  
			   
	   
				  $.post("<?php echo site_url('pusat/aktivasi'); ?>",{tmujian_id:tmujian_id,status:status},function(){
	   
					alertify.success("Berhasil dilakukan perubahan aktivasi");
					//location.reload();
				  })
  
  
				})
				   
				 
	 });



  var dataTable = $('#madrasahsasaran').DataTable( {
						"processing": true,
						"language": {
						"processing": '<div class="preloader pl-size-l"><div class="spinner-layer pl-red-grey"><div class="circle-clipper left"> <div class="circle"></div></div><div class="circle-clipper right"><div class="circle"></div></div></div></div>',
						  "oPaginate": {
							"sFirst": "Halaman Pertama",
							"sLast": "Halaman Terakhir",
							 "sNext": "Selanjutnya",
							 "sPrevious": "Sebelumnya"
							 },
						"sInfo": "Total Data :  _TOTAL_ dan ini (_START_ - _END_)",
						 "sInfoEmpty": "Tidak ada data yang di tampilkan",
						   "sZeroRecords": "Data Masih Belum Tersedia",
						   "sLengthMenu": "&nbsp;&nbsp; Menampilkan   _MENU_ Data"
				    },
					
					"serverSide": true,
					"searching": false,
					"responsive": false,
					"lengthMenu": [[10,25, 50,100,200,300,500,1000, 800000000], [10,25, 50,100,200,300,500,1000,"All"]],
					 
					 "sPaginationType": "full_numbers",
					 "dom": 'Blfrtip',
					"buttons": [
					
                         	
						    {
							extend: 'excelHtml5',
							
							className:'btn btn-primary '
							
							}
					],
					
					"ajax":{
						url :"<?php echo site_url("pusat/gridPelatihan"); ?>", 
						type: "post", 
						"data": function ( data ) {
							data.provinsi_id= $("#provinsi_id").val();
						data.kota_id= $("#kota_id").val();
						data.status= $("#status").val();
						data.level_id= $("#level_id").val();
						data.verval= $("#verval").val();
						data.key= $("#key").val();
						data.kolom= $("#kolom").val();
						
					
				
                    }
						
					},
					"rowCallback": function( row, data ) {
						
						
					}
				} );
				
			   $("#pencarian").click(function(){
				dataTable.ajax.reload(null,false);
				});
				
	


</script>