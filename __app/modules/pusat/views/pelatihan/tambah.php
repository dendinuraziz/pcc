
  <script src="<?php echo base_url(); ?>__statics/ckfinder/ckfinder.js"></script>


<div class="app-main__inner">
                    <div class="app-page-title">
                        <div class="page-title-wrapper">
                            <div class="page-title-heading">
                                <div class="page-title-icon">
                                    <i class="fa fa-users icon-gradient bg-tempting-azure">
                                    </i>
                                </div>
                                <div>Tambah  Pelatihan
								<div class="page-title-subheading">
									
                                    </div>
                                    
                                </div>
                            </div>
                            
						</div>
                    </div>      
					<div id="showform"></div>

					<div class="mb-3 card">
					
					
					 
                                            <div class="card-body">

											<div class="d-block text-left ">
						<button class="mb-2 mr-2 btn-pill btn-transition btn btn-outline-primary menuajax " href="<?php echo site_url("pusat/pelatihan"); ?>"  title="Daftar Pelatihan"> <i class="fa fa-history"> </i> Kembali</button>

						
					</div>


                                                <ul class="tabs-animated-shadow tabs-animated nav">
                                                    <li class="nav-item">
                                                        <a role="tab" class="nav-link  active" id="tab-c-0" data-toggle="tab" href="#tab-animated-0">
                                                            <span>Informasi Pelatihan </span>
                                                        </a>
                                                    </li>
                                                    
                                                    
                                                </ul>
                                                <div class="tab-content">
                                                    <div class="tab-pane active " id="tab-animated-0" role="tabpanel">
                                                        
													<form id="simpangambar" name="simpangambar" method="post" enctype="multipart/form-data" url="<?php echo site_url("pusat/savePelatihan"); ?>">
														<div class="form-row">
															<div class="col-md-12">
																<div class="position-relative form-group">
																	<label for="exampleEmail11" class="">Nama Pelatihan</label>
																	<input name="f[nama]" id="namaPelatihan"  placeholder="Masukkan Nama Pelatihan disini "  type="text" class="form-control">
																</div>
															</div>
															
														</div>

														<div class="form-row">
															<div class="col-md-12">
																<div class="position-relative form-group">
																	<label for="exampleEmail11" class="">Slug Pelatihan</label>
																	<input name="f[slug]" id="slug" readonly    type="text" class="form-control">
																</div>
															</div>
															
														</div>
													
														<div class="form-row">
															<div class="col-md-2">
																<div class="position-relative form-group">
																	<label for="exampleCity" class="">Kategori</label>
																	<select class="multiselect-dropdown form-control" name="f[kategori_id]">

																		<optgroup label="Kategori Pelatihan">
																			
																			<?php 
																			 $kategori = $this->db->get("kategori")->result();
																			   foreach($kategori as $rk){

																				?><option value="<?php echo $rk->id; ?>" ><?php echo $rk->nama; ?></option><?php 
																			   }
																			 ?>
																			
																			
																		</optgroup>
																		
																	</select>
																</div>
															</div>
															<div class="col-md-2">
																<div class="position-relative form-group">
																	<label for="exampleState" class="">ID Modul</label>
																	<select class="multiselect-dropdown form-control" name="f[modul_id]">

																		<optgroup label="Modul Pelatihan">
																			
																			<?php 
																			 $kategori = $this->db->get("modul")->result();
																			   foreach($kategori as $rk){

																				?><option value="<?php echo $rk->id; ?>" ><?php echo $rk->nama; ?></option><?php 
																			   }
																			 ?>
																			
																			
																		</optgroup>
																		
																	</select>
																</div>
															</div>
															
															<div class="col-md-1">
																<div class="position-relative form-group">
																	<label for="exampleState" class="">Modul</label>
																	<select class="multiselect-dropdown form-control" name="f[modul]">

																		<optgroup label="Modul Pelatihan">
																			
																			<?php 
																			 $kategori = $this->db->get("modul")->result();
																			   foreach($kategori as $rk){

																				?><option value="<?php echo $rk->slug; ?>" ><?php echo $rk->nama; ?></option><?php 
																			   }
																			 ?>
																			
																			
																		</optgroup>
																		
																	</select>
																</div>
															</div>


															<div class="col-md-3">
																<div class="position-relative form-group">
																	<label for="exampleZip" class="">Kemampuan </label>
																	<select class="multiselect-dropdown form-control" name="f[jenis]">

																		<optgroup label="Kemampuan">
																			
																			<?php 
																			$kategori = array("Beginner","Profesional");

																			foreach($kategori as $rk){

																				?><option value="<?php echo $rk; ?>" ><?php echo $rk; ?></option><?php 
																			}
																			?>
																			
																			
																		</optgroup>

																		</select>
																</div>
															</div>

															<div class="col-md-3">
																<div class="position-relative form-group">
																	<label for="exampleZip" class="">Tag </label>
																	<select multiple="multiple" class="multiselect-dropdown form-control" name="modul">

																		<optgroup label="Tag Pelatihan">
																			
																			<?php 
																			$kategori = $this->db->get("modul")->result();
																			foreach($kategori as $rk){

																				?><option value="<?php echo $rk->id; ?>" ><?php echo $rk->nama; ?></option><?php 
																			}
																			?>
																			
																			
																		</optgroup>

																		</select>
																</div>
															</div>

															<div class="col-md-2">
																<div class="position-relative form-group">
																	<label for="exampleZip" class="">Passing Grade </label>
																	<input name="f[grade]"  placeholder="Masukkan Passing Grade " value="" type="number" class="form-control">
													
																</div>
															</div>

															




														</div>

														<div class="form-row">
															<div class="col-md-12">
																<div class="position-relative form-group">
																	<label for="exampleEmail11" class="">Deskripsi Pelatihan</label>
																	<textarea id="deskripsi" name="f[desk]"></textarea>
																</div>
															</div>
															
														</div>

														<div class="form-row">
															<div class="col-md-12">
																<div class="position-relative form-group">
																	<label for="exampleEmail11" class="">Cover </label>
																	<input name="file"  type="file" class="form-control">
																</div>
															</div>
															
														</div>

														<div class="d-block text-right card-footer">
															<button class="mr-2 btn btn-link btn-sm">Cancel</button>
															<button class="btn btn-success btn-lg"><i class="fa fa-save"></i> Simpan Pelatihan</button>
														</div>
													     	
													</form>


                                                    </div>
                                                   
                                                   
                                                </div>
                                            </div>
                                        </div>
					
				  
				  </div>


                  
</div>
 		
  <script type="text/javascript">
			   $(document).on("input","#namaPelatihan",function(){
				  
					var nilai = $(this).val();
					

					
				var slug = nilai.toLowerCase()
							.replace(/ /g, '-')
							.replace(/[^\w-]+/g, '');
						$("#slug").val(slug);

				  
				});
				var base_url="<?php echo base_url(); ?>";
	
				var editor = CKEDITOR.replace( 'deskripsi', {
				removeButtons : 'Print,NewPage,Save,Cut,Copy,PasteText,Undo,Redo,SelectAll,SpellChecker,RemoveFormat,CreateDiv,Outdent,Indent,BidiRtl,BidiLtr,Anchor,PageBreak,Styles,Format,JustifyBlock,JustifyCenter,HorizontalRule,Subscript',
																	          removePlugins: 'about,showblocks,forms,preview,find,smiley,scayt,specialchar,templates,flash',
																			 
																			  uploadUrl: base_url+'__statics/ckdrive/core/connector/php/connector.php?command=QuickUpload&type=Files&responseType=json&show=1',
																			  filebrowserBrowseUrl: base_url+'__statics/ckdrive/ckfinder.html',
																			  filebrowserImageBrowseUrl: base_url+'__statics/ckdrive/ckfinder.html?type=Images&show=1',
																			  filebrowserUploadUrl: base_url+'__statics/ckdrive/core/connector/php/connector.php?command=QuickUpload&type=Files&show=1',
																			  filebrowserImageUploadUrl: base_url+'__statics/ckdrive/core/connector/php/connector.php?command=QuickUpload&type=Images&show=1',
																			  height: 300,
																			  removeDialogTabs: 'image:advanced;link:advanced',
																			   allowedContent :true
																} );
																
</script>