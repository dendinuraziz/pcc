<?php 
 $topik = $this->db->query("select * from silabus where pelatihan_id='{$pelatihan->id}' order by urutan asc")->result();
 foreach($topik as $rt){
?>

<div class="main-card mb-3 card">
                                        <div class="card-header">
                                            <div class="col-md-6">
                                            <input type="text" class="form-control" value="<?php echo $rt->nama; ?>" size="30">
                                            </div>
                                            
                                            <div class="btn-actions-pane-right actions-icon-btn">
                                                <a class="btn-icon btn-icon-only btn btn-link editTopik" data-toggle="modal"  data-target=".bd-example-modal-lg"  topik_id="<?php echo $rt->id; ?>">
                                                    <i class="fa fa-edit" style="color:blue"></i>
                                                </a>
                                                <a class="btn-icon btn-icon-only btn btn-link hapusTopik"  topik_id="<?php echo $rt->id; ?>">
                                                    <i class="fa fa-trash" style="color:red"></i>
                                                </a>
                                                
                                            </div>
                                        </div>
                                        <div class="card-body">
                                           <table class="table table-hover table-bordered table-striped">
                                               <thead>
                                                   <tr>
                                                       <th>No</th>
                                                       <th>#</th>
                                                       <th>Judul Aktivitas</th>
                                                       <th>Jenis Konten</th>
                                                      
                                                       <th>Before</th>
                                                       <th>Durasi</th>
                                                       <th>Tindakan</th>
                                                   </tr>
                                               </thead>

                                               <tbody>
                                                   <?php 
                                                    $aktivitas = $this->db->query("select * from subsilabus where silabus_id='{$rt->id}' order by urutan asc")->result();
                                                    $no=1;  
                                                    foreach($aktivitas as $ak){
                                                        $jenisKonten = $this->Reff->get_kondisi(array("subsilabus_id"=>$ak->id),"konten","konten_belajar");
                                                        
                                                    ?>
                                                    <tr>
                                                        <td><?php echo $ak->urutan; ?></td>
                                                        <td><?php echo $ak->id; ?></td>
                                                        <td><?php echo $ak->nama; ?></td>
                                                        <td><?php echo $this->Reff->get_kondisi(array("id"=>$jenisKonten),"konten_belajar","nama"); ?></td>
                                                        
                                                        <td><?php echo $ak->before; ?></td>
                                                        <td><?php echo $ak->durasi; ?> menit</td>
                                                        <td>
                                                          
                                                        
                                                           <a class="btn-icon  btn-link" href="<?php echo site_url("pusat/konten/".$ak->id.""); ?>" title="Konten Aktivitas"><i class="fa fa-file"></i>  </a>
                                                           <a class="btn-icon  btn-link editAktivitas" href="#" data-toggle="modal"  data-target=".bd-example-modal-lg"  subsilabus_id="<?php echo $ak->id; ?>" silabus_id="<?php echo $rt->id; ?>"><i class="fa fa-edit"></i>  </a>
                                                           <a class="btn-icon  btn-link hapusAktivitas" href="#" subsilabus_id="<?php echo $ak->id; ?>"><i class="fa fa-trash" style="color:danger"></i>  </a>
                                                        </td>
                                                    </tr>
                                                    <?php 
                                                      }
                                                    ?>

                                               </tbody>
                                           </table>
                                        </div>
                                        <div class="d-block text-right card-footer">
                                          <button class="mb-2 mr-2 btn-icon btn btn-link" data-toggle="modal"  data-target=".bd-example-modal-lg" data-toggle="modal"  data-target=".bd-example-modal-lg"  id="TambahAktivitas" silabus_id="<?php echo $rt->id; ?>"><i class="fa fa-plus"></i> Tambah Aktivitas  </button>
                                        </div>
</div>
<?php 
 }
 ?>

<button class="mb-2 mr-2 btn-icon btn btn-link" data-toggle="modal"  data-target=".bd-example-modal-lg"  id="TambahTopik"><i class="fa fa-plus"></i> Tambah Topik  </button>

          
<style> 
.modal-dialog-center {
    margin-top: 10%;
}
</style>

<div class="modal fade bd-example-modal-lg " tabindex="7" data-backdrop="false"  role="dialog" aria-labelledby="myLargeModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-center">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Aktivitas <?php echo $pelatihan->nama; ?> </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="loadBodyTopik">
                Mohon Tunggu..
            </div>
           
        </div>
    </div>
</div>

<script>
 $(document).off("click","#TambahTopik").on("click","#TambahTopik",function(){

      var pelatihan_id = "<?php echo $pelatihan->id; ?>";
      $.post("<?php echo site_url('pusat/formtopik'); ?>",{pelatihan_id:pelatihan_id},function(data){

        $("#loadBodyTopik").html(data);
      })
 });

 $(document).off("click",".editTopik").on("click",".editTopik",function(){

      var id           = $(this).attr("topik_id");
      var pelatihan_id = "<?php echo $pelatihan->id; ?>";
      $.post("<?php echo site_url('pusat/formtopik'); ?>",{pelatihan_id:pelatihan_id,id:id},function(data){

        $("#loadBodyTopik").html(data);
      })
 });

    $(document).off("click",".hapusTopik").on("click",".hapusTopik",function(){

        var id           = $(this).attr("topik_id");
        var pelatihan_id = "<?php echo $pelatihan->id; ?>";
        alertify.confirm("Apakah Anda yakin menghapus topik ini ?",function(){

            $.post("<?php echo site_url('pusat/hapusTopik'); ?>",{pelatihan_id:pelatihan_id,id:id},function(data){
            
                location.reload();
          
            })

        })
        
    });

    // Aktivitas

    $(document).off("click","#TambahAktivitas").on("click","#TambahAktivitas",function(){

        var pelatihan_id = "<?php echo $pelatihan->id; ?>";
        var silabus_id   = $(this).attr("silabus_id");
        $.post("<?php echo site_url('pusat/formaktivitas'); ?>",{pelatihan_id:pelatihan_id,silabus_id:silabus_id},function(data){

        $("#loadBodyTopik").html(data);
        })
        });

        $(document).off("click",".editAktivitas").on("click",".editAktivitas",function(){
        var silabus_id   = $(this).attr("silabus_id");
        var id           = $(this).attr("subsilabus_id");
        var pelatihan_id = "<?php echo $pelatihan->id; ?>";
        $.post("<?php echo site_url('pusat/formaktivitas'); ?>",{pelatihan_id:pelatihan_id,id:id,silabus_id:silabus_id},function(data){

             $("#loadBodyTopik").html(data);
        })
        });

        $(document).off("click",".hapusAktivitas").on("click",".hapusAktivitas",function(){

        var id           = $(this).attr("subsilabus_id");
        var pelatihan_id = "<?php echo $pelatihan->id; ?>";
        alertify.confirm("Apakah Anda yakin menghapus Aktivitas ini ?",function(){

            $.post("<?php echo site_url('pusat/hapusAktivitas'); ?>",{id:id},function(data){
            
                location.reload();
            
            })

        })
        
        });
</script>
		
