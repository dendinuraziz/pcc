

<div class="app-main__inner">
                    <div class="app-page-title">
                        <div class="page-title-wrapper">
                            <div class="page-title-heading">
                                <div class="page-title-icon">
                                    <i class="fa fa-users icon-gradient bg-tempting-azure">
                                    </i>
                                </div>
                                <div><?php echo $title; ?>
                                    <div class="page-title-subheading">
									 Peserta yang mengikuti Pelatihan <?php echo $pelatihan->nama; ?>
                                    </div>
                                </div>
                            </div>
                            
						</div>
                    </div>      
					<div id="showform"></div>
					<div class="main-card mb-3 card">
					    
                        <div class="card-body">
						<div class="d-block text-right ">
						<a class="mb-2 mr-2 btn-pill btn-transition btn btn-outline-primary  " href="<?php echo site_url("pusat/pelatihan"); ?>"  title="Daftar Pelatihan"> <i class="fa fa-history"> </i> Kembali</a>

						
					</div>
						    <div class="row right" >
							 
							   
							  
							  
							
							  <div class="col-md-3">
							  <div class="input-group">
									<input type="hidden" id="pelatihan_id" class="form-control" value="<?php echo $pelatihan->id; ?>">
									<input type="text" id="key" class="form-control" placeholder="Cari Nama Peserta">
									
							  </div>
							  
							  
							  </div>
							  <div class="col-md-2">
							  
									
										<button class="btn btn-primary btn-icon" id="pencarian" type="button"><span class="fa fa-search"></span> Tampilkan </button>
							
							  </div>
							  
							 
							 </div>
						<hr>	 
							 
				  <div class="row">
				  
				  
				   <div class="col-md-12">
				     <div class="table-responsive">
					   <table class="table table-hover table-bordered table-striped" id="madrasahsasaran" style="font-size:12px">
					    <thead>
						  <tr>
						    <th> No </th>
						    <th> PTK ID </th>
						    <th> Nama  </th>
						    <th> Tgl Lahir </th>
						    <th> Jabatan </th>						   
						    <th> Instansi </th>
						    <th> Provinsi </th>
						    <th> Kab/Kota </th>
						    <th> Progress  </th>
						    <th> Skor  </th>
						    <th> Sertifikat  </th>
						    <th> Hasil Pelatihan  </th>
						   
						 </tr>
						</thead>
					   
					   </table>
					 
					 
					 </div>
				   
				   </div>
				  
				  
				  </div>
                  
                    </div>
                </div>
				
  <script type="text/javascript">
  var madrasahsasaran = $('#madrasahsasaran').DataTable( {
						"processing": true,
						"language": {
						"processing": '<div class="preloader pl-size-l"><div class="spinner-layer pl-red-grey"><div class="circle-clipper left"> <div class="circle"></div></div><div class="circle-clipper right"><div class="circle"></div></div></div></div>',
						  "oPaginate": {
							"sFirst": "Halaman Pertama",
							"sLast": "Halaman Terakhir",
							 "sNext": "Selanjutnya",
							 "sPrevious": "Sebelumnya"
							 },
						"sInfo": "Total Data :  _TOTAL_ dan ini (_START_ - _END_)",
						 "sInfoEmpty": "Tidak ada data yang di tampilkan",
						   "sZeroRecords": "Data Masih Belum Tersedia",
						   "sLengthMenu": "&nbsp;&nbsp; Menampilkan   _MENU_ Data"
				    },
					
					"serverSide": true,
					"searching": false,
					"responsive": false,
					"lengthMenu": [[10,25, 50,100,200,300,500,1000, 800000000], [10,25, 50,100,200,300,500,1000,"All"]],
					 
					 "sPaginationType": "full_numbers",
					 "dom": 'Blfrtip',
					"buttons": [
					
                         	
						    {
							extend: 'excelHtml5',
							
							className:'btn btn-primary '
							
							}
					],
					
					"ajax":{
						url :"<?php echo site_url("pusat/gridPesertaPelatihan"); ?>", 
						type: "post", 
						"data": function ( data ) {
						data.pelatihan_id= $("#pelatihan_id").val();
						
						data.key= $("#key").val();
						data.kolom= $("#kolom").val();
						
					
				
                    }
						
					},
					"rowCallback": function( row, data ) {
						
						
					}
				} );
				
			   $("#pencarian").click(function(){
				  madrasahsasaran.ajax.reload(null,false);
				});
				
	


</script>