


<div class="form-row">
															<div class="col-md-12">
																<div class="position-relative form-group">
																	<label for="exampleEmail11" class="">Upload File  </label>
																	<input  name="file" id="file" type="file"  class="form-control">
																</div>
															</div>
</div>

                                                            
<script type="text/javascript">
            $(document).ready(function() {

              $("#file").fileinput({
                uploadUrl: "<?php echo site_url("pusat/kontenUpload"); ?>",
                overwriteInitial: false,
                maxFileSize: 100000000,
                browseClass: "btn btn-success",
                browseLabel: " Konten",
                browseIcon: "<i class=\"fa fa-file-signature\"></i> Ambil File ",
                removeClass: "btn btn-danger",
                removeLabel: "Delete",

                removeIcon: "<i class=\"fa fa-trash\"></i>  Hapus File",
                uploadClass: "btn btn-info",
                uploadLabel: "Upload",
                uploadIcon: "<i class=\"fa fa-upload\"></i>  Upload Semua File",
                initialPreviewAsData: true,
                uploadExtraData: function() {
                  return {
                    konten_id: $("#id").val(),
                    pelatihan_id: $("#pelatihan_id").val()
                   
                  };
                },
                initialPreview: [
                  <?php

                       $lihat = "";
                    if (isset($detail)) {
                      if(!empty($detail->konten)){

                      $lihat .= ",'https://drive.google.com/file/d/" . $detail->konten . "/preview'";
                      echo substr($lihat, 1);
                      }
                     }
                

                  


                  ?>


                ],
                initialPreviewConfig: [

                  <?php
                    $lihat = "";
                  if (isset($detail)) {

                    if(!empty($detail->konten)){

                      $lihat .= ',{caption: "", size:200, width: "100%", url: "' . site_url("pusat/hapusUpload") . '", key: "' . $detail->id . '"}';
                      echo substr($lihat, 1);
                    }
                  }

                 


                  ?>



                ]




              });





            });
          </script>
										
