		<div class="scrollbar-sidebar">
                    <div class="app-sidebar__inner">
                        <ul class="vertical-nav-menu">
                            <li class="app-sidebar__heading">CMS Pelatihan  </li>
                            <li  >
                                <a href="<?php echo site_url("pusat"); ?>" class="menuajax" title="Dashboard">
                                    <i class="metismenu-icon fa fa-laptop"></i>
                                    Dashboard 
                                    
                                </a>
                               
                            </li>

                            <li>
                                <a href="#">
                                    <i class="metismenu-icon fa fa-database"></i>
                                    Referensi Pelatihan   
                                    <i class="metismenu-state-icon fa fa-angle-left"></i>
                                </a>
                                <ul class="mm-collapse ">
                                    <li>
                                        <a href="<?php echo site_url("pusat/kategori"); ?>" class="menuajax" title="Kategori Pelatihan">
                                            <i class="metismenu-icon"></i>
                                            Kategori Pelatihan
                                        </a>
                                    </li>
                                   
                                    <li>
                                        <a href="<?php echo site_url("banksoal/buatsoal"); ?>" class="menuajax" title="Daftar Peserta">
                                            <i class="metismenu-icon"></i>
                                            Modul  Pelatihan 
                                        </a>
                                    </li>


                                   
                                    
                                    
                                </ul>
                            </li>
                           
						    <li class="mm-active">
                                <a href="#">
                                    <i class="metismenu-icon fa fa-book"></i>
                                    Pelatihan   
                                    <i class="metismenu-state-icon fa fa-angle-left"></i>
                                </a>
                                <ul class="mm-collapse mm-show">
                                    <li>
                                        <a href="<?php echo site_url("pusat/pelatihan"); ?>" class="menuajax" title="Daftar Pelatihan">
                                            <i class="metismenu-icon"></i>
                                            Daftar Pelatihan
                                        </a>
                                    </li>
                                   
                                    <li>
                                        <a href="<?php echo site_url("banksoal/buatsoal"); ?>" class="menuajax" title="Daftar Peserta">
                                            <i class="metismenu-icon"></i>
                                            Bank Soal 
                                        </a>
                                    </li>


                                    <li>
                                        <a href="<?php echo site_url("pusat/peserta"); ?>" class="menuajax" title="Daftar Peserta">
                                            <i class="metismenu-icon"></i>
                                            Daftar Peserta
                                        </a>
                                    </li>
                                    
                                    
                                </ul>
                            </li>
							
							
							  
                            
                            
							  <li class="app-sidebar__heading">Akun  </li>
							
							   <li class="mm-active" >
                                <a href="<?php echo site_url("pusat/akun"); ?>" class="menuajax">
                                    <i class="metismenu-icon fa fa-lock"></i>
                                    Akun Anda 
                                    
                                </a>
                               
                            </li>
							 <li>
                                <a href="<?php echo site_url("login/logout"); ?>">
                                    <i class="metismenu-icon fa fa-angle-right"></i>
                                    Logout
                                    
                                </a>
                                
                            </li>
                            
                        </ul>
                    </div>
                </div>
                
      
    