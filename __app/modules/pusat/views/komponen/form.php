

<div class="app-main__inner">
                    <div class="app-page-title">
                        <div class="page-title-wrapper">
                            <div class="page-title-heading">
                                <div class="page-title-icon">
                                    <i class="fa fa-users icon-gradient bg-tempting-azure">
                                    </i>
                                </div>
                                <div><?php echo $title; ?>
                                    <div class="page-title-subheading">
									 <?php echo $data->title; ?>
                                    </div>
                                </div>
                            </div>
                            
						</div>
                    </div>      
					<div id="showform"></div>
					<div class="main-card mb-3 card">
					    <div class="card-header"><?php echo $data->title; ?></div>
                        <div class="card-body">
                            
                            <form method="post" url="<?php echo site_url("pusat/save_komponen"); ?>" id="simpannorespon" action="javascript:void(0)">
				<div class="kt-portlet__body">
					<div class="form-group form-group-last">
						<div class="alert alert-secondary" role="alert">
							<div class="alert-icon"><i class="flaticon-warning kt-font-brand"></i></div>
						  	<div class="alert-text">
								Silahkan isi pada form dibawah ini 
							</div>
						</div>
					</div>
					<div class="form-group">
						
						<input type="hidden" name="id" value="<?php echo $data->id; ?>">
						 <textarea id="editor"   name="isi"><?php echo $data->isi; ?></textarea>
															 <script type="text/javascript">
																
																 var base_url = "<?php echo base_url(); ?>";
																 var editor = CKEDITOR.replace( 'editor', {
																	         
																			  uploadUrl: base_url+'__statics/ckdrive/core/connector/php/connector.php?command=QuickUpload&type=Files&responseType=json&show=1',
																			  filebrowserBrowseUrl: base_url+'__statics/ckdrive/ckfinder.html',
																			  filebrowserImageBrowseUrl: base_url+'__statics/ckdrive/ckfinder.html?type=Images&show=1',
																			  filebrowserUploadUrl: base_url+'__statics/ckdrive/core/connector/php/connector.php?command=QuickUpload&type=Files&show=1',
																			  filebrowserImageUploadUrl: base_url+'__statics/ckdrive/core/connector/php/connector.php?command=QuickUpload&type=Images&show=1',
																			  height: 400,
																			   allowedContent :true,
																			  
																} );
																
																															
																										
																															
																
																 </script>
					</div>
					
				</div>
				<div class="kt-portlet__foot">
					<div class="kt-form__actions">
						<center><button type="submit" class="btn btn-primary">Simpan </button>
						<button type="reset" class="btn btn-secondary">Cancel</button>
						</center>
					</div>
				</div>
			</form>
			
			
                            
                        </div>
                        
                    </div>
</div>