<?php

class M_model extends CI_Model {
 
    public function __construct() {
        parent::__construct();
		date_default_timezone_set("Asia/Jakarta");
    }

   
   public function gridPelatihan($paging){
      
	   
	    $key   = $this->input->get_post("key");
		$this->db->select("*");
	
		
        $this->db->from('pelatihan');
        
	
	    if(!empty($key)){  $this->db->where("UPPER(nama) LIKE '%".strtoupper($key)."%'");    }
		
		
         if($paging==true){
				     $this->db->limit($_REQUEST['length'],$_REQUEST['start']);
					 $this->db->order_by("nama","ASC");
		
			 }
      
		
		
		return $this->db->get();
		
	}


	public function gridPeserta($paging){
      
	   
	    $key   = $this->input->get_post("key");
		$this->db->select("*");
	
		
        $this->db->from('peserta');
        
	
	    if(!empty($key)){  $this->db->where("UPPER(nama) LIKE '%".strtoupper($key)."%'");    }
		
		
         if($paging==true){
				     $this->db->limit($_REQUEST['length'],$_REQUEST['start']);
					 $this->db->order_by("nama","ASC");
		
			 }
      
		
		
		return $this->db->get();
		
	}

	public function gridPesertaPelatihan($paging){
      
	   
	    $key   = $this->input->get_post("key");
	    $pelatihan_id   = $this->input->get_post("pelatihan_id");
		$this->db->select("a.pelatihan_id,a.peserta_id,a.list_silabus,a.mulai,a.selesai,a.status,a.device,a.skor,a.sertifikat,a.lulus,b.*");
	
		
        $this->db->from('tr_pelatihan a');
        $this->db->join('peserta b',"ON a.peserta_id=b.id");
        $this->db->where('pelatihan_id',$pelatihan_id);
        
	
	    if(!empty($key)){  $this->db->where("UPPER(nama) LIKE '%".strtoupper($key)."%'");    }
		
		
         if($paging==true){
				     $this->db->limit($_REQUEST['length'],$_REQUEST['start']);
					 $this->db->order_by("nama","ASC");
		
			 }
      
		
		
		return $this->db->get();
		
	}

	public function progress($pelatihan_id,$peserta_id){

        $data = $this->db->query("select * from tr_pelatihan where pelatihan_id='{$pelatihan_id}' and peserta_id='{$peserta_id}'")->row();
        if(!is_null($data)){
		$list_silabus = json_decode($data->list_silabus,true);
        $no=0;
		foreach($list_silabus as $key => $value)
		{
			if($list_silabus[$key]['result']=="Y"){
				$no++;
			}
			
		}
        return $no;
     }else{
         return 0;
     }

    }

	public function gridKategori($paging){
      
	   
	    $key   = $this->input->get_post("key");
		$this->db->select("*");
	
		
        $this->db->from('kategori');
        
	
	    if(!empty($key)){  $this->db->where("UPPER(nama) LIKE '%".strtoupper($key)."%'");    }
		
		
         if($paging==true){
				     $this->db->limit($_REQUEST['length'],$_REQUEST['start']);
					 $this->db->order_by("nama","ASC");
		
			 }
      
		
		
		return $this->db->get();
		
	}
	
	
}
