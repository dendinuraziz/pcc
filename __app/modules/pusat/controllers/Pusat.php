<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pusat extends CI_Controller {

	 public function __construct()
      {
        parent::__construct();
		$this->load->model('M_model',"mp");
         if(!$this->session->userdata("pusatIdAdmin")){
			    
				echo $this->Reff->sessionhabis();
				exit();
			
		  }
		
	  }
		
	
	function _template($data)
	{
	 
     $this->load->view('pusat/page_header',$data);
	    

	  
	}
	
	
	
	
	public function index(){
    		
       $ajax                = $this->input->get_post("ajax",true);	
	   $data['title']       = "CMS Pelatihan - Dashboard Pusat";
      
		 
	     if(!empty($ajax)){
			 
			 
			 $this->load->view('default',$data);
			
		
		 }else{
			 
			
		      
		       $data['konten']  = "default";
			 
			 
			 $this->_template($data);
		 }
	
      
	}
	
	
	
	public function pelatihan(){
    		
       $ajax                = $this->input->get_post("ajax",true);	
	   $data['title']       = "CMS  - Daftar Pelatihan";
      	 if(!empty($ajax)){
			 $this->load->view('pelatihan/pelatihan',$data);
		 }else{
			$data['konten']  = "pelatihan/pelatihan";
			$this->_template($data);
		 }
	
      
	}
		
  public function gridPelatihan(){
		  error_reporting(0);
		  $iTotalRecords = $this->mp->gridPelatihan(false)->num_rows();
		  
		  $iDisplayLength = intval($_REQUEST['length']);
		  $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
		  $iDisplayStart = intval($_REQUEST['start']);
		  $sEcho = intval($_REQUEST['draw']);
		  
		  $records = array();
		  $records["data"] = array(); 

		  $end = $iDisplayStart + $iDisplayLength;
		  $end = $end > $iTotalRecords ? $iTotalRecords : $end;
		  
		  $datagrid = $this->mp->gridPelatihan(true)->result_array();
		   
		   $i= ($iDisplayStart +1);
		   foreach($datagrid as $val) {
			$checked ="";
			if($val['nonaktif']==1){

				$checked ="checked";
			}
				   $peserta = $this->db->query("select count(id) as jml from tr_pelatihan where pelatihan_id='{$val['id']}'")->row();
				  
				$no = $i++;
				$records["data"][] = array(
					$no,					
					$this->Reff->get_kondisi(array("id"=>$val['kategori_id']),"kategori","nama"),
					$val['nama'],
					$val['jenis'],
					$val['modul'],
					'<input type="checkbox" tmujian_id="'.$val["id"].'" class="aktivasi" '.$checked.' >',
					'<a class="mb-2 mr-2 btn-icon btn-icon-only btn-shadow btn-outline-2x btn btn-outline-primary" title="Daftar Peserta" href="'.site_url("pusat/pesertaPelatihan/".$val['id']."").'"><i class="fa fa-users"> '.$peserta->jml.' Peserta </i></a>',
					
					'<a class="mb-2 mr-2 btn-icon btn-icon-only btn-shadow btn-outline-2x btn btn-outline-primary" title="Daftar Pelatihan" href="'.site_url("pusat/edit/".$val['id']."").'"><i class="fa fa-file"> </i></a>
					<a class="mb-2 mr-2 btn-icon btn-icon-only btn-shadow btn-outline-2x btn btn-outline-danger"><i class="fa fa-trash"> </i></a>'
					
                   

				  );
			  }
		
		  $records["draw"] = $sEcho;
		  $records["recordsTotal"] = $iTotalRecords;
		  $records["recordsFiltered"] = $iTotalRecords;
		  
		  echo json_encode($records);
	}
	public function aktivasi(){

		$this->db->where("id",$_POST['tmujian_id']);
		$this->db->set("nonaktif",$_POST['status']);
		$this->db->update("pelatihan");

		echo "sukses";
	}

	public function tambahPelatihan(){
    		
		$ajax                = $this->input->get_post("ajax",true);	
		$data['title']       = "CMS  - Tambah Pelatihan";
		
			if(!empty($ajax)){
			  $this->load->view('pelatihan/tambah',$data);
		  }else{
			 $data['konten']  = "pelatihan/tambah";
			 $this->_template($data);
		  }
	 
	   
	 }




	public function edit($pelatihan_id){
    		
		$ajax                = $this->input->get_post("ajax",true);	
		$data['title']       = "CMS  - Edit Pelatihan";
		$data['pelatihan']	 = $this->db->get_where("pelatihan",array("id"=>$pelatihan_id))->row();
			if(!empty($ajax)){
			  $this->load->view('pelatihan/edit',$data);
		  }else{
			 $data['konten']  = "pelatihan/edit";
			 $this->_template($data);
		  }
	 
	   
	 }

	 public function savePelatihan(){

		
			$id = $this->input->post("id",true);
			$f  = $this->input->post("f",true);

		            $config['upload_path'] = './__statics/upload/pelatihan';
					
					$config['allowed_types'] = "jpg|jpeg|png|JPG|JPEG|png"; 	
					$config['overwrite'] = true; 	
					$filenamepecah			 = explode(".",$_FILES['file']['name']);
					$imagenew				 = time().".".pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
					
					$config['file_name'] = $imagenew;

					
					$this->load->library('upload', $config);

							if ( ! $this->upload->do_upload('file'))
							{
								if(empty($id)){
									header('Content-Type: application/json');
									echo json_encode(array('error' => true, 'msg' => $this->upload->display_errors()));

								}else{

									$this->db->where("id",$id);
									
									$this->db->update("pelatihan",$f);
								}
								
							
							
							}else{

								if(empty($id)){
									$this->db->set("cover",$imagenew);
									$this->db->insert("pelatihan",$f);

								}else{
									$this->db->set("cover",$imagenew);
									$this->db->where("id",$id);
									$this->db->update("pelatihan",$f);
								}


							}


	 }

	 public function formtopik(){

		$pelatihan_id    	  = $this->input->get_post("pelatihan_id",true);	
		$id		         	  = $this->input->get_post("id",true);	
		$data['pelatihan_id'] = $pelatihan_id;
		$silabus	          = $this->db->query("select max(urutan) as urut from silabus where pelatihan_id='{$pelatihan_id}'")->row();
		$data['urutan']	      = $silabus->urut+1;
		if(!empty($id)){
			$data['topik']	      = $this->db->get_where("silabus",array("id"=>$id))->row();
		}		

		$this->load->view("pelatihan/formtopik",$data);
	 }

	 public function saveTopik(){
		$pelatihan_id    	  = $this->input->get_post("pelatihan_id",true);	
		$id		         	  = $this->input->get_post("id",true);	
		$f		         	  = $this->input->get_post("f",true);	
		if(!empty($id)){
			$this->db->where("id",$id);
			$this->db->update("silabus",$f);

		}else{

			$this->db->set("pelatihan_id",$pelatihan_id);
			$this->db->set("induk","Ya");
			$this->db->insert("silabus",$f);
		}

		redirect(site_url("pusat/edit/".$pelatihan_id.""));

	 }

	 public function hapusTopik(){

		$pelatihan_id    	  = $this->input->get_post("pelatihan_id",true);	
		$id		         	  = $this->input->get_post("id",true);	
		$this->db->where("id",$id);
		$this->db->delete("silabus");

		$this->db->where("silabus_id",$id);
		$this->db->delete("subsilabus");

		echo "sukses";
	 }

	 // Aktivitas

	 public function formaktivitas(){

		$pelatihan_id    	  = $this->input->get_post("pelatihan_id",true);	
		$silabus_id    	      = $this->input->get_post("silabus_id",true);	
		$id		         	  = $this->input->get_post("id",true);	
		$data['topik']        = $this->db->get_where("silabus",array("id"=>$silabus_id))->row();
		$silabus	          = $this->db->query("select max(id) as kunci from subsilabus where silabus_id='{$silabus_id}'")->row();
		$data['kunci']	      = $silabus->kunci;
		$subsilabus	          = $this->db->query("select max(urutan) as urut from subsilabus where silabus_id='{$silabus_id}'")->row();
		$data['urutan']	      = $subsilabus->urut+1;

		if(!empty($id)){
			$data['aktivitas']	      = $this->db->get_where("subsilabus",array("id"=>$id))->row();
		}		

		$this->load->view("pelatihan/formaktivitas",$data);
	 }

	 public function saveAktivitas(){
		$pelatihan_id      	  = $this->input->get_post("pelatihan_id",true);	
		$silabus_id      	  = $this->input->get_post("silabus_id",true);	
		$id		         	  = $this->input->get_post("id",true);	
		$f		         	  = $this->input->get_post("f",true);	
		if(!empty($id)){
			$this->db->where("id",$id);
			$this->db->update("subsilabus",$f);

		}else{

			$this->db->set("silabus_id",$silabus_id);
			$this->db->insert("subsilabus",$f);
		}

		redirect(site_url("pusat/edit/".$pelatihan_id.""));

	 }

	 public function hapusAktivitas(){

		$pelatihan_id    	  = $this->input->get_post("pelatihan_id",true);	
		$id		         	  = $this->input->get_post("id",true);	
		

		$this->db->where("id",$id);
		$this->db->delete("subsilabus");

		echo "sukses";
	 }


	 public function konten($subsilabus_id){
    		
		$ajax                = $this->input->get_post("ajax",true);	
		$data['title']       = "CMS  - Konten Pelatihan";
		$data['aktivitas']	 = $this->db->get_where("v_silabus",array("id"=>$subsilabus_id))->row();
		$detail 			 = $this->db->get_where("konten",array("subsilabus_id"=>$subsilabus_id))->row();
		if(!is_null($detail)){
				$data['detail'] = $detail;

		}
			if(!empty($ajax)){
			  $this->load->view('pelatihan/konten',$data);
		  }else{
			 $data['konten']  = "pelatihan/konten";
			 $this->_template($data);
		  }
	 
	   
	 }

	 public function konten_belajar(){
		$id   = $this->input->get_post("id",true);	
		if($id==1 or $id==9 or $id==10){
			$this->load->view("pelatihan/konten/area");
		}else if($id==4 or $id==5 or $id==6){
			$this->load->view("pelatihan/konten/link");

		}else{
			$this->load->view("pelatihan/konten/upload");
		}

	 }

	 public function saveKonten(){

			
	
		$id		         	  = $this->input->get_post("id",true);	
		$f		         	  = $this->input->get_post("f",true);	
		if(!empty($id)){

			$this->db->where("id",$id);
			$this->db->update("konten",$f);
			echo $id;

		}else{

		
			$this->db->insert("konten",$f);
			echo $this->db->insert_id();
		}

		//redirect(site_url("pusat/konten/".$f['subsilabus_id'].""));


	 }


	//  public function kontenUpload(){


	// 	$konten_id    =  $this->input->get_post("konten_id");
	// 	$pelatihan_id =  $this->input->get_post("pelatihan_id");

	// 	$config['upload_path'] = './__statics/upload/pelatihan';
	// 				$folder   = '__statics/upload/pelatihan';
	// 				$config['allowed_types'] = "jpg|jpeg|png|JPG|JPEG|"; 	
	// 				$config['overwrite'] = true; 	
	// 				$filenamepecah			 = explode(".",$_FILES['file']['name']);
	// 				$imagenew				 = str_replace(array(" ","-","/","?"),"_",$namapersyaratan).$_SESSION['tmsiswa_id'].$trpersyaratan_id.".".pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
					
	// 				$config['file_name'] = $imagenew;
					
	// 				$this->load->library('upload', $config);

	// 						if ( ! $this->upload->do_upload('file'))
	// 						{
	// 							header('Content-Type: application/json');
	// 							echo json_encode(array('error' => true, 'msg' => $this->upload->display_errors()));
										
								
							
	// 						}


	//  }

	 public function kontenUpload(){

		$konten_id    =  $this->input->get_post("konten_id");
		$pelatihan_id =  $this->input->get_post("pelatihan_id");
		$pelatihan 	  = $this->db->get_where("pelatihan",array("id"=>$pelatihan_id))->row();
		

		   $service = new Google_Drive();
			$foldernama = $pelatihan->slug;
			$foldernama = str_replace("/",DIRECTORY_SEPARATOR,$foldernama);
			$folderId = $pelatihan->folder;
			   if(!$folderId){
				   $folderId = $service->getFileIdByName( BACKUP_FOLDER );
				   if( !$folderId ) {
					   $folderId = $service->createFolder( BACKUP_FOLDER );					
				   }
				   $folderId = $service->createMultiFolder($foldernama,$folderId);
				   $this->db->set("folder",$folderId);
				  
				   $this->db->where("id",$pelatihan_id);
				   $this->db->update("pelatihan");

			   }
			   $fileId = $service->createFileFromPath( $_FILES["file"]['tmp_name'], $_FILES["file"]['name'], $folderId );
			   $service->setPublic($fileId);

			   if (empty($fileId)) {
					   header("Content-Type: application/json");
					   echo json_encode(array('error' =>'Proses upload gagal dilakukan, silahkan coba kembali',"uploaded"=>"error"));
				   
			   }else{

					$this->db->set("konten",$fileId);
					$this->db->where("id",$konten_id);
					$this->db->update("konten");
					header('Content-Type: application/json');
						echo json_encode(array('success' => true, 'message' => "Upload Sukses"));


			   }
	 }


	 public function hapusUpload(){
		$this->db->set("konten","");
		$this->db->where("id",$_POST['key']);
		$this->db->update("konten");
		header('Content-Type: application/json');
						echo json_encode(array('success' => true, 'message' => "Upload Sukses"));

	 }
	
	
	
		public function akun(){
		  
		
		  $ajax                = $this->input->get_post("ajax",true);	
		  $data['title']       = "CMS  - Daftar Peserta";
		  $data['data'] = $this->db->query("select * from pusat where id='".$_SESSION['pusatIdAdmin']."'")->row();
			
		    if(!empty($ajax)){
				$this->load->view('akun',$data);
			}else{
			   $data['konten']  = "akun";
			   $this->_template($data);
			}


			 
			 
			
		  
		}
	
		public function save_akun(){
		 
		  $this->db->set("password",$_POST['password']);
		  
		
		  $this->db->update("pusat");
		  
		  redirect(base_url()."pusat/akun");
		  
		}
		
		
	// Me 
	
	


	// Peserta 

	public function peserta(){
    		
		$ajax                = $this->input->get_post("ajax",true);	
		$data['title']       = "CMS  - Daftar Peserta";
			if(!empty($ajax)){
			  $this->load->view('peserta/page',$data);
		  }else{
			 $data['konten']  = "peserta/page";
			 $this->_template($data);
		  }
	 
	   
	 }
		 
   public function gridPeserta(){
		 
		   $iTotalRecords = $this->mp->gridPeserta(false)->num_rows();
		   
		   $iDisplayLength = intval($_REQUEST['length']);
		   $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
		   $iDisplayStart = intval($_REQUEST['start']);
		   $sEcho = intval($_REQUEST['draw']);
		   
		   $records = array();
		   $records["data"] = array(); 
 
		   $end = $iDisplayStart + $iDisplayLength;
		   $end = $end > $iTotalRecords ? $iTotalRecords : $end;
		   
		   $datagrid = $this->mp->gridPeserta(true)->result_array();
			
			$i= ($iDisplayStart +1);
			foreach($datagrid as $val) {
				 
				
				   
				 $no = $i++;
				 $records["data"][] = array(
					 $no,					
					 $val['ptk_id'],
					 $val['nama'],
					 $val['tgl_lahir'],
					 $val['jabatan'],
					 $val['instansi'],
					 $val['m_provinsi'],
					 $val['m_kota']
					 
					 
 
				   );
			   }
		 
		   $records["draw"] = $sEcho;
		   $records["recordsTotal"] = $iTotalRecords;
		   $records["recordsFiltered"] = $iTotalRecords;
		   
		   echo json_encode($records);
	 }
	 
	

	 public function pesertaPelatihan($pelatihan_id){
    		
		$ajax                = $this->input->get_post("ajax",true);	
		$data['pelatihan']	 = $this->db->get_where("pelatihan",array("id"=>$pelatihan_id))->row();
		$data['title']       = "CMS  - Peserta ".$data['pelatihan']->nama;
		
			if(!empty($ajax)){
			  $this->load->view('pelatihan/peserta',$data);
		  }else{
			 $data['konten']  = "pelatihan/peserta";
			 $this->_template($data);
		  }
	 
	   
	 }
	 public function gridPesertaPelatihan(){
		 
		$iTotalRecords = $this->mp->gridPesertaPelatihan(false)->num_rows();
		
		$iDisplayLength = intval($_REQUEST['length']);
		$iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
		$iDisplayStart = intval($_REQUEST['start']);
		$sEcho = intval($_REQUEST['draw']);
		
		$records = array();
		$records["data"] = array(); 

		$end = $iDisplayStart + $iDisplayLength;
		$end = $end > $iTotalRecords ? $iTotalRecords : $end;
		
		$datagrid = $this->mp->gridPesertaPelatihan(true)->result_array();
		 $statusLulus = array("0"=>"Belum Selesai","1"=>"Lulus","2"=>"Tidak Lulus");
		
		 $i= ($iDisplayStart +1);
		 foreach($datagrid as $val) {
			  
			     $materi  = $this->db->query("select count(id) as jml from v_silabus where pelatihan_id='{$val['pelatihan_id']}'")->row();
                 $progress= $this->mp->progress($val['pelatihan_id'],$val['peserta_id']);
                 $persen  = number_format(($progress/$materi->jml) * 100,2);
			 
				
			  $no = $i++;
			  $records["data"][] = array(
				  $no,					
				  $val['ptk_id'],
				  $val['nama'],
				  $val['tgl_lahir'],
				  $val['jabatan'],
				  $val['instansi'],
				  $val['m_provinsi'],
				  $val['m_kota'],
				  
				  $progress."/".$materi->jml."(".$persen."%)",
				  $val['skor'],
				  ($val['sertifikat']==1) ? "Sudah Unduh" :"Belum Unduh",
				  $statusLulus[$val['lulus']]
				  
				  

				);
			}
	  
		$records["draw"] = $sEcho;
		$records["recordsTotal"] = $iTotalRecords;
		$records["recordsFiltered"] = $iTotalRecords;
		
		echo json_encode($records);
  }

  
  // Kategori Pelatihan 

	public function kategori(){
    		
		$ajax                = $this->input->get_post("ajax",true);	
		$data['title']       = "CMS  - Kategori Pelatihan";
			if(!empty($ajax)){
			  $this->load->view('kategori/page',$data);
		  }else{
			 $data['konten']  = "kategori/page";
			 $this->_template($data);
		  }
	 
	   
	 }
		 
   public function gridKategori(){
		 
		   $iTotalRecords = $this->mp->gridKategori(false)->num_rows();
		   
		   $iDisplayLength = intval($_REQUEST['length']);
		   $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
		   $iDisplayStart = intval($_REQUEST['start']);
		   $sEcho = intval($_REQUEST['draw']);
		   
		   $records = array();
		   $records["data"] = array(); 
 
		   $end = $iDisplayStart + $iDisplayLength;
		   $end = $end > $iTotalRecords ? $iTotalRecords : $end;
		   
		   $datagrid = $this->mp->gridKategori(true)->result_array();
			
			$i= ($iDisplayStart +1);
			foreach($datagrid as $val) {
				 
				
				   
				 $no = $i++;
				 $records["data"][] = array(
					 $no,					
					
					 $val['nama'],
					 $val['slug'],
					 	
					'<a class="mb-2 mr-2 btn-icon btn-icon-only btn-shadow btn-outline-2x btn btn-outline-primary " title="Daftar Pelatihan" href="'.site_url("pusat/formKategori?id=".$val['id']."").'"><i class="fa fa-edit"> </i></a>
					<a class="mb-2 mr-2 btn-icon btn-icon-only btn-shadow btn-outline-2x btn btn-outline-danger hapus" datanya="'.$val['id'].'" urlnya="'.site_url("pusat/hapusKategori").'"><i class="fa fa-trash"> </i></a>'
					 
					 
 
				   );
			   }
		 
		   $records["draw"] = $sEcho;
		   $records["recordsTotal"] = $iTotalRecords;
		   $records["recordsFiltered"] = $iTotalRecords;
		   
		   echo json_encode($records);
	 }


	 public function formKategori(){
    		
		$ajax                = $this->input->get_post("ajax",true);	
		$id                  = $this->input->get_post("id",true);	
		$data['title']       = "CMS  - Form Kategori";

		 if(!empty($id)){
			 $data['data'] = $this->db->get_where("kategori",array("id"=>$id))->row();
		 }
		
			if(!empty($ajax)){
			  $this->load->view('kategori/form',$data);
		  }else{
			 $data['konten']  = "kategori/form";
			 $this->_template($data);
		  }
	 
	   
	 }


	 public function saveKategori(){
		
		$id		         	  = $this->input->get_post("id",true);	
		$f		         	  = $this->input->get_post("f",true);	
		if(!empty($id)){
			$this->db->where("id",$id);
			$this->db->update("kategori",$f);

		}else{

			
			$this->db->insert("kategori",$f);
		}

		echo "sukses";

	 }
}
