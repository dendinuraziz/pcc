<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<script src="https://kit.fontawesome.com/64d58efce2.js" crossorigin="anonymous"></script>
	<link rel="icon" href="<?php echo base_url(); ?>__statics/img/logo.png">
	<link rel="stylesheet" href="<?php echo base_url(); ?>__statics/login/style.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>__statics/js/alert/alert.css">



	<title><?php echo $title; ?> </title>
</head>

<body>
	<div class="container">
		<div class="forms-container">
			<div class="signin-signup">
				<form id="login" action="javascript:void(0)" url="<?php echo site_url("login/do_login"); ?>" method="post" class="sign-in-form">
					<h2 class="title">LOGIN PANITIA </h2>
					<div class="input-field">
						<i class="fas fa-user"></i>
						<input type="text" name="email" placeholder="Masukkan  Username" />
					</div>
					<div class="input-field">
						<i class="fas fa-lock"></i>
						<input type="password" name="password" placeholder="Masukkan Password" />
					</div>

					<input type="submit" id="btn-login" value="Login" class="btn solid" />


					<img src="<?php echo base_url(); ?>__statics/img/loading.gif" width="60px" style="display:none" class="loading">

					<!-- <p>Lupas Password ? <br> <a href="#" id="lupapassword" style="color:black"> Klik disini untuk reset password </a> </p> -->


				</form>


			</div>
		</div>

		<div class="panels-container">
			<div class="panel left-panel">
				<div class="content">
					<h3><img src="<?php echo base_url(); ?>__statics/img/logowhite.png" style="height:90px"></h3>

					<p>
						Untuk Melakukan Pendaftaran Seleksi silahkan login menggunakan akun SIMPATIKA.
					</p>

					<button class="btn transparent" onclick="window.location.href='<?php echo site_url(); ?>'">
						Login Sebagai Peserta
					</button>
				</div>
				<img src="<?php echo base_url(); ?>__statics/login/img/undraw_feeling_proud_qne1.svg" class="image" alt="" style="width:550px" />
			</div>
			<div class="panel right-panel">
				<div class="content">
					<h3>Sudah memiliki akun ?</h3>

					<p>
						Jika Anda sudah memiliki akun, silahkan login kedalam aplikasi.
					</p>
					<button class="btn transparent" id="sign-in-btn">
						Login
					</button>
					<button class="btn transparent" onclick="window.location.href='<?php echo site_url(); ?>'">
						Portal
					</button>
				</div>
				<img src="<?php echo base_url(); ?>__statics/login/img/register.svg" class="image" alt="" style="width:450px" />
			</div>
		</div>
	</div>
	<script src="<?php echo base_url(); ?>__statics/js/jquery.min.js"></script>
	<script src="<?php echo base_url(); ?>__statics/js/alert/alert.js"></script>
	<script src="<?php echo base_url(); ?>__statics/login/app.js"></script>
</body>

</html>
<?php
if ($title == "Pembuatan Akun") {
?>
	<script>
		$("#sign-up-btn").trigger("click");
	</script>
<?php
}
?>



<script type="text/javascript">
	var base_url = "<?php echo base_url(); ?>";



	function sukses(param) {
		let timerInterval;
		Swal.fire({
			type: 'success',
			title: 'Berhasil ',
			showConfirmButton: false,
			html: 'Pembuatan Akun berhasil, Dalam <strong></strong> detik<br>Anda akan dialihkan kehalaman login',

			timer: 3000,
			onBeforeOpen: () => {
				Swal.showLoading();
				timerInterval = setInterval(() => {
					Swal.getContent().querySelector('strong')
						.textContent = Swal.getTimerLeft()
				}, 100)
			},
			onClose: () => {
				clearInterval(timerInterval)
			}
		}).then((result) => {
			if (
				/* Read more about handling dismissals below */
				result.dismiss === Swal.DismissReason.timer

			) {

				location.href = base_url + param;
			}
		})
	}


	function sukses2(param) {
		let timerInterval;
		Swal.fire({
			type: 'success',
			title: 'Berhasil ',
			showConfirmButton: false,
			html: 'Proses authentikasi berhasil, Dalam <strong></strong> detik<br>Anda akan dialihkan kedalam aplikasi',

			timer: 1000,
			onBeforeOpen: () => {
				Swal.showLoading();
				timerInterval = setInterval(() => {
					Swal.getContent().querySelector('strong')
						.textContent = Swal.getTimerLeft()
				}, 100)
			},
			onClose: () => {
				clearInterval(timerInterval)
			}
		}).then((result) => {
			if (
				/* Read more about handling dismissals below */
				result.dismiss === Swal.DismissReason.timer

			) {

				location.href = base_url + param;
			}
		})
	}

	function gagal(param) {
		let timerInterval;
		Swal.fire({
			type: 'warning',
			title: 'Gagal, Perhatikan ! ',
			showConfirmButton: true,
			html: param

		})
	}


	$(document).off('submit', 'form#regis').on('submit', 'form#regis', function(event, messages) {
		event.preventDefault();
		var form = $(this);
		var urlnya = $(this).attr("url");


		$("#btn-regis").attr("disabled", "disabled");
		$(".loading").show();
		$.ajax({
			type: "POST",
			url: urlnya,
			data: form.serialize(),
			success: function(response, status, xhr) {
				var ct = xhr.getResponseHeader("content-type") || "";
				if (ct == "application/json") {

					gagal(response.message);




				} else {


					sukses("login");






				}

				$("#btn-regis").removeAttr("disabled");
				$(".loading").hide();

			}
		});

		return false;
	});




	$(document).off('submit', 'form#login').on('submit', 'form#login', function(event, messages) {
		event.preventDefault();
		var form = $(this);
		var urlnya = $(this).attr("url");


		$("#btn-login").attr("disabled", "disabled");
		$(".loading").show();
		$.ajax({
			type: "POST",
			url: urlnya,
			data: form.serialize(),
			success: function(response, status, xhr) {
				var ct = xhr.getResponseHeader("content-type") || "";
				if (ct == "application/json") {

					gagal(response.message);




				} else {


					sukses2(response);






				}

				$("#btn-login").removeAttr("disabled");
				$(".loading").hide();

			}
		});

		return false;
	});


	$(document).off('click', '#lupapassword').on('click', '#lupapassword', function(event, messages) {

		Swal.fire({
			title: 'Demi keamanan, silahkan hubungi Helpdesk untuk mereset Password Anda',

			showCancelButton: true,
			confirmButtonText: 'Siap ',
			showLoaderOnConfirm: true,
			preConfirm: (login) => {

				return fetch('login/lupapassword?nik=' + login)
					.then(response => {


						Swal.fire({
							type: 'warning',
							title: 'Reset Password ! ',
							showConfirmButton: true,
							html: "Silahkan hubungi Helpdesk"

						});

					})
					.catch(error => {

					})
			},
			allowOutsideClick: () => !Swal.isLoading()
		}).then((result) => {
			if (result.isConfirmed) {

			}
		})


	});
</script>