<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>__statics/js/alert/alert.css">
<script src="<?php echo base_url(); ?>__statics/js/alert/alert.js"></script>

<section class="breadcrumb-area" style="background-color: #F9FAFD;">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-12 align-self-center">
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Login </li>
                    </ul>
                    <h2>Silahkan Masuk </h2>
                </div>
            </div>
        </div>
    </section>
    <!-- breabcrumb Area End -->
    
    <div class="signin-area pd-top-130 pd-bottom-100">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-5">
                <form id="login" class="single-signin-form-wrap" action="javascript:void(0)" url="<?php echo site_url("login/do_login"); ?>" method="post">
						
                   
                        
                        <div class="single-input-wrap">
                            <input type="text" name="username" placeholder="Masukkan Username">
                        </div>
                        <div class="single-input-wrap">
                            <input type="password" name="password" placeholder="Masukkan Password">
                        </div>
                        <div class="btn-wrap">
                            <button class="btn btn-base w-100">Masuk </button>
                            <div id="loading" style="display:none"> Proses autentikasi, Mohon tunggu.. </div>
                        </div>
                        
                    </form>
                </div>
            </div>
        </div>
    </div>





<script type="text/javascript">

var base_url="<?php echo base_url(); ?>";

function sukses(param){
	   let timerInterval;
	   Swal.fire({
		 type: 'success',
		 title: 'Proses Authentication Berhasil ',
		 showConfirmButton: false,
		  html: 'Dalam <strong></strong> detik<br>Anda akan dialihkan kedalam aplikasi',

		 timer: 1000,
		 onBeforeOpen: () => {
		   Swal.showLoading();
		   timerInterval = setInterval(() => {
			 Swal.getContent().querySelector('strong')
			   .textContent = Swal.getTimerLeft()
		   }, 100)
		 },
		 onClose: () => {
		   clearInterval(timerInterval)
		 }
	   }).then((result) => {
		 if (
		   /* Read more about handling dismissals below */
		   result.dismiss === Swal.DismissReason.timer
		   
		 ) {
		
		  location.href = base_url+param;
		 }
	   })
}

function gagal(param){
	   let timerInterval;
	   Swal.fire({
		 type: 'warning',
		 title: 'Proses Authentication Gagal ',
		 showConfirmButton: true,
		  html: param

	   })
}


   $(document).on('submit', 'form#login', function (event, messages) {
	event.preventDefault();
	  var form   = $(this);
	  var urlnya = $(this).attr("url");
   
	 
	
			 $("#loading").show();
			  $.ajax({
				   type: "POST",
				   url: urlnya,
				   data: form.serialize(),
				   success: function (response, status, xhr) {
					   var ct = xhr.getResponseHeader("content-type") || "";
					   if (ct == "application/json") {
					   
						   gagal(response.message);
			   
						
						   
						   
					   } else {
						 
						  
						  sukses(response);
						  
						   
						   
							
						   
						 
					   }
					   
                       $("#loading").hide();
					   
				   }
			   });
			 
	   return false;
   });
   
   
   
</script>