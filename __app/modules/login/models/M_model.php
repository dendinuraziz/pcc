<?php

class M_model extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
	}


	public function registrasi()
	{

		date_default_timezone_set("Asia/Jakarta");
		$data     = xssArray(($this->input->get_post("f")));
		$password = $this->db->escape_str($this->input->get_post("password"));

		$this->db->set("password", sha1(md5($password)));
		$this->db->set("alias", (($password)));
		$this->db->set("tgl_daftar", date("Y-m-d H:i:s"));
		$this->db->insert("peserta", $data);
		$this->Reff->log($data['nama'] . " login kedalam aplikasi pada " . formattimestamp(date("Y-m-d H:i:s")) . "", "1");

		return "peserta";
	}

	public function login()
	{

		date_default_timezone_set("Asia/Jakarta");
		$username   = xss(trim($this->input->get_post("username")));
		$password   = xss(trim($this->input->get_post("password")));

		$username   = $this->security->xss_clean(($this->db->escape_str($username)));
		$password   = $this->db->escape_str($this->security->xss_clean($password));

		
		$pusat        = $this->Reff->query("select id,nama from pusat where username='" . $username . "' and password='" . (($password)) . "'")->row();
		
		if (!is_null($pusat)) {
			$session = array(
				'pusatIdAdmin'        => $pusat->id,
				'admin'        => $pusat->nama,
				'status'        => "Administrator ",
				'is_login'        => true,
				'date_login'   => date("Y-m-d H:i:s")
			);

			//$this->session->sess_expiration = "10000000000";
			$this->session->set_userdata($session);

			return "pusat";

			//$this->Reff->log($pusat->nama . " login kedalam aplikasi pada " . formattimestamp(date("Y-m-d H:i:s")) . "", "4");
		}else{

			return "gagal";
		}
	}
}
