<?php

use GuzzleHttp\Psr7\Uri;

defined('BASEPATH') or exit('No direct script access allowed');

class Login extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_model', "mp");
	}

	public function index()
	{
		$data 			= array();
		$data['title']  = "Halaman Login";
		$this->load->view('page_login', $data);
	}

	public function admin()
	{

		$data['title']  = "Online Course - Silahkan Masuk";
		$data['konten'] = "page_login";
		$this->load->view("page_header",$data);
	}





	


	public function do_login()
	{

		$this->form_validation->set_message('required', '{field} Tidak Boleh Kosong. ');

		$this->form_validation->set_message('min_length', '{field} Minimal 6 Karakter .');
		$this->form_validation->set_message('matches', '{field} Tidak Sama dengan yang dikolom password .');
		$this->form_validation->set_message('is_unique', '{field} Sudah terdaftar , mungkin anda sudah pernah melakukan pendaftaran, silahkan klik menu login .');



		$config = array(
			array('field' => 'username', 'label' => 'Username   ', 'rules' => 'trim|required'),

			array('field' => 'password', 'label' => 'Password ', 'rules' => 'trim|required|min_length[3]'),




		);

		$this->form_validation->set_rules($config);

		if ($this->form_validation->run() == true) {


			$data = $this->mp->login();
			if ($data == "gagal") {
				header('Content-Type: application/json');
				echo json_encode(array('error' => true, 'message' => "Akun Anda tidak ditemukan, Mohon periksa Email atau Password yang Anda masukkan "));
			} else {

				echo $data;
			}
		} else {


			header('Content-Type: application/json');
			echo json_encode(array('error' => true, 'message' => validation_errors()));
		}
	}

	public function logout()
	{
		$this->session->sess_destroy();


		echo "yes";
	}


	public function notdendi()
	{


		$this->load->view("notdendi");
	}

	public function oauth_simpatika()
	{
		session_destroy();


		$this->config->load('oauth_simpatika');

		$clientId = $this->config->item('simpatika_client_id');

		$redirectUrl = $this->config->item('simpatika_redirect_url');

		$uri = "{$this->config->item('endpoint_oauth_simpatika')}/oauth/authorize?client_id=$clientId&redirect_uri=$redirectUrl&response_type=code&scope=read";

		redirect($uri);
	}

	public function callback_oauth_simpatika()
	{
		// $this->load->library('session');
		$this->config->load('oauth_simpatika');
		$this->load->helper('curl');
		$this->load->helper('simpatika');

		$getCode = $this->input->get('code');
		if ($getCode) {

			$endpoint = $this->config->item('endpoint_oauth_simpatika') . '/oauth/token';
			$payload = [
				'code' => $getCode,
				'client_id' => $this->config->item('simpatika_client_id'),
				'client_secret'   => $this->config->item('simpatika_client_secret'),
				'grant_type' => 'authorization_code',
				'redirect_url' => $this->config->item('simpatika_redirect_url')
			];
			$resToken = curl_post($endpoint, $payload);

			if ($resToken) {
				$resToken = json_decode($resToken, true);

				$endpoint = $this->config->item('endpoint_oauth_simpatika') . '/oauth/profile';

				$headers = [
					'Authorization: Bearer ' . $resToken['access_token']
				];
				$resProfile = json_decode(curl_get($endpoint, [], $headers), true);

				// print_r($resProfile);
				// die();

				if (isset($resProfile['userid'])) {


					date_default_timezone_set("Asia/Jakarta");

					// res nama, userid, email
					$data = [
						'nama' => $resProfile['nama'],
						'simpatika_id' => $resProfile['userid'],
						'email' => $resProfile['email']
					];
					// $password = $resToken['access_token'];

					$this->db->where('simpatika_id', $resProfile['userid']);
					$this->db->from('peserta');
					$isExist = $this->db->count_all_results();

					if ($isExist) {
						//update
						$this->db->where('simpatika_id', $resProfile['userid']);
						$this->db->update('peserta', $data);
					} else {
						// insert
						$res = get_simpatika_peserta($resToken['access_token']);
						foreach ($res['data'] as $key => $val) {
							$data[$key] = $val;
						}
						//	$res['data']['simpatika_id'] = $resProfile['userid'];

						//print_r($data);
						// exit();
						$this->db->set('tgl_daftar',date("Y-m-d H:i:s"));
						$this->db->insert("peserta", $data);
					}

					$this->db->where('simpatika_id', $resProfile['userid']);
					$this->db->from('peserta');

					$result = $this->db->get()->row_array();

					if (count($result) > 0) {




						$session = array(
							'peserta_id' => $result['id'],
							'simpatika_id' => $resProfile['userid'],
							'peserta_nama' => $resProfile['nama'],
							'status'  => "peserta",
							'is_login' => true,
							'date_login' => date("Y-m-d H:i:s"),
							'token_user_simpatika' =>  $resToken['access_token']
						);


						//$this->session->sess_expiration = "10000000";
						$this->session->set_userdata($session);

						//print_r($this->session->userdata());
						$this->Reff->log($resProfile['nama'] . " login kedalam aplikasi pada " . formattimestamp(date("Y-m-d H:i:s")) . "", "1");

						redirect(site_url('peserta/profile'));
					}



					// redirect(site_url('peserta/peminatan'));
				}
			}
		}
	}
}
