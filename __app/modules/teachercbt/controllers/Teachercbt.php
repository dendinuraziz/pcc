<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Teachercbt extends CI_Controller {

	 public function __construct()
      {
        parent::__construct();
		  if(!$this->session->userdata("pusatIdAdmin")){
			    
				echo $this->Reff->sessionhabis();
				exit();
			
		  }
		  $this->load->helper('my');
		  $this->load->model('M_cbt');
		date_default_timezone_set("Asia/Jakarta");
	  }
	public function simulasi()
	{
		
		$key = $this->input->get('start', true);
		$hash = $this->input->get('hash', true);
	     $id  = base64_decode($key); 
		   
		
		$ujian 		= $this->M_cbt->getUjianById($id);
		$soal 		= $this->M_cbt->getSoal($id);
		
		

		

		$arr_opsi = array("a","b","c","d","e");
		$html = '';
			
		$no = 1;
		//print_r($soal_urut_ok); exit();
		if (!empty($soal)) {
			foreach ($soal as $s) {
				
				
				$vrg = "N";
				
				
				$html .= '<input type="hidden" name="id_soal_'.$no.'" value="'.$s->id_soal.'">';
				$html .= '<input type="hidden" name="rg_'.$no.'" id="rg_'.$no.'" value="'.$vrg.'">';
				$html .= '<div class="step" id="widget_'.$no.'">';

				$html .= '<div class="papers"><div class="text-center"></div>'.$s->soal.'</div>';
				
				if($s->jenis==1){
					$html .= '<hr><div class="funkyradio">';
						for ($j = 0; $j < 5; $j++) {
							$opsi 			= "opsi_".$arr_opsi[$j];
							$file 			= "file_".$arr_opsi[$j];
							$checked 		= "";
							$pilihan_opsi 	= !empty($s->$opsi) ? $s->$opsi : "";
							if($pilihan_opsi !=""){
								$html .= '<div class="funkyradio-success" >
								<input type="radio" id="opsi_'.strtolower($arr_opsi[$j]).'_'.$s->id_soal.'"   name="jawaban_'.$no.'"  nomor="'.$no.'" value="'.strtoupper($arr_opsi[$j]).'" '.$checked.'> <label for="opsi_'.strtolower($arr_opsi[$j]).'_'.$s->id_soal.'"><div class="huruf_opsi">'.$arr_opsi[$j].'</div> <p>'.$pilihan_opsi.'</p></label></div>';
							}
						}
						
						$html .= '<hr><div class="alert alert-danger"> Kunci Jawaban : '.$s->jawaban.' </div>';

				}else if($s->jenis==9){
							$html .= '<hr><div class="funkyradio">';
								for ($j = 0; $j < 5; $j++) {
									$opsi 			= "opsi_".$arr_opsi[$j];
									$file 			= "file_".$arr_opsi[$j];
									$checked 		= "";
									$pilihan_opsi 	= !empty($s->$opsi) ? $s->$opsi : "";
									if($pilihan_opsi !=""){
									$html .= '<div class="funkyradio-success">
										<input type="radio" id="opsi_'.strtolower($arr_opsi[$j]).'_'.$s->id_soal.'" name="opsi_'.$no.'" value="'.strtoupper($arr_opsi[$j]).'" '.$checked.'> <label for="opsi_'.strtolower($arr_opsi[$j]).'_'.$s->id_soal.'"><div class="huruf_opsi">'.$arr_opsi[$j].'</div> <p>'.$pilihan_opsi.'</p></label></div>';
									}
								}
								
								$html .= '<hr><div class="alert alert-danger"> Skoring  </div>';
						
				}else if($s->jenis==2){
					$html .= '<hr><div class="funkyradio">';
					for ($j = 0; $j < 2; $j++) {
							$opsi 			= "opsi_".$arr_opsi[$j];
							$file 			= "file_".$arr_opsi[$j];
							$checked 		= "";
							$pilihan_opsi 	= !empty($s->$opsi) ? $s->$opsi : "";
							

					  $html .= '<div class="funkyradio-success">
								<input type="radio" id="opsi_'.strtolower($arr_opsi[$j]).'_'.$s->id_soal.'" name="opsi_'.$no.'" value="'.strtoupper($arr_opsi[$j]).'" '.$checked.'> <label for="opsi_'.strtolower($arr_opsi[$j]).'_'.$s->id_soal.'"><div class="huruf_opsi">'.$arr_opsi[$j].'</div> <p>'.$pilihan_opsi.'</p></label></div>';
					}
					$html .= '<hr><div class="alert alert-danger"> Kunci Jawaban : '.$s->jawaban.' </div>';
					
				}else if($s->jenis==4){
					
					$html .= '<hr><div class="row">';
					$opsi_a   = explode("[split]",$s->opsi_a);
					  
					  
					$opsi_b   = explode("[split]",$s->opsi_b);
					$opsi_c   = explode("[split]",$s->opsi_c);
					$opsi_d   = explode("[split]",$s->opsi_d);
					$opsi_e   = explode("[split]",$s->opsi_e);




					$opsi_a_per = isset($opsi_a[0]) ? $opsi_a[0]:"";
					$opsi_b_per = isset($opsi_b[0]) ? $opsi_b[0]:"";
					$opsi_c_per = isset($opsi_c[0]) ? $opsi_c[0]:"";
					$opsi_d_per = isset($opsi_d[0]) ? $opsi_d[0]:"";
					$opsi_e_per = isset($opsi_e[0]) ? $opsi_e[0]:"";

					$opsi_a_j = isset($opsi_a[1]) ? $opsi_a[1]:"";
					$opsi_b_j = isset($opsi_b[1]) ? $opsi_b[1]:"";
					$opsi_c_j = isset($opsi_c[1]) ? $opsi_c[1]:"";
					$opsi_d_j = isset($opsi_d[1]) ? $opsi_d[1]:"";
					$opsi_e_j = isset($opsi_e[1]) ? $opsi_e[1]:"";


					
					$pertanyaan    = array($opsi_a_per,$opsi_b_per,$opsi_c_per,$opsi_d_per,$opsi_e_per);
					
					$listjawaban  = array($opsi_a_j,$opsi_b_j,$opsi_c_j,$opsi_d_j,$opsi_e_j);
						
				   
					  $selek = "<select class='form-control'>";
						foreach($listjawaban as  $ij=>$jwb){

							$selek .= "<option value='".$jwb."'>".$jwb."</option>";
						}

						$selek .= "</select>";

					  $html .= '<hr>  <div class="table-responsive"> <table class="table-hover table  ">';
				  
						$noempat=1;
						  foreach($pertanyaan as $ig=>$per){

							  
  
							  if(!empty($per)){
					  
								$html .= '<tr>';
								$html .= '<td align="left" width="3px">'.$noempat++.'. </td>';
								$html .= '<td align="left">'.$per.'</td>';
								$html .= '<td align="left">'.$selek.'</td>';
							  
							   
								$html .= '<tr>';
							  }
  
  
  
						  }
						  $html .= '</table>
						  
						  <textarea class="form-control" placeholder="Masukan alasan Anda "></textarea>
						 
						  </div>';

						  $html .= '<div class="alert alert-warning">  Jawaban  '.$s->jawaban.'</div>';
					
					
					
					
					
					
				}else if($s->jenis==3){
					$html .= '<hr><div class="row">';
					$html .= '<div class="alert alert-warning"> Soal Short Answer  - Jawaban  '.$s->jawaban.'</div>';
					
					
					
			   }else if($s->jenis==5){
					$html .= '<hr><div class="row">';
					$html .= '<div class="alert alert-warning"> Soal Essay </div>';
					
					
					
				}else if($s->jenis==6){
					$arnumber = array("a"=>"1","b"=>"2","c"=>"3","d"=>"4","e"=>"5");
					$html .= '<hr><div class="funkyradio">';
					for ($j = 0; $j < 5; $j++) {
						$opsi 			= "opsi_".$arr_opsi[$j];
						$file 			= "file_".$arr_opsi[$j];
						$checked 		= "";
						$pilihan_opsi 	= !empty($s->$opsi) ? $s->$opsi : "";
						if($pilihan_opsi !=""){
						$html .= '<div class="funkyradio-success">
							<input type="checkbox" id="opsi_'.strtolower($arr_opsi[$j]).'_'.$s->id_soal.'" name="opsi_'.$no.'" value="'.strtoupper($arr_opsi[$j]).'" '.$checked.'> <label for="opsi_'.strtolower($arr_opsi[$j]).'_'.$s->id_soal.'"><div class="huruf_opsi">'.$arnumber[$arr_opsi[$j]].'</div> <p>'.$pilihan_opsi.'</p></label></div>';
						}
					}
					
					$html .= '<hr><div class="alert alert-danger"> Kunci Jawaban : '.$s->jawaban.' </div>';



							
				}else if($s->jenis==7){
					$html .= '<hr><div class="row">';
					$html .= '<hr>  <div class="table-responsive"> <table class="table-hover table  ">';
					$html .= '<tr>';
					$html .= '<td width="60%"></td>';
					$html .= ' <td align="center">'.$s->subjek1.'</td>';
					$html .= ' <td align="center">'.$s->subjek2.'</td>';
					$html .= '</tr>';
					$pilihanganda = array("A"=>"opsi_a","B"=>"opsi_b","C"=>"opsi_c","D"=>"opsi_d","E"=>"opsi_e");
						$jawabanganda = array("A"=>"file_a","B"=>"file_b","C"=>"file_c","D"=>"file_d","E"=>"file_e");
						foreach($pilihanganda as $ig=>$pg){
							$jj = $jawabanganda[$ig];

							if(!empty($s->$pg)){
					
					          $html .= '<tr>';
					          $html .= '<td>'.$s->$pg.'</td>';
					          $html .= '<td align="center"><input type="radio" id="janda'.$ig.'" name="cek'.$pg.'" value="'.$s->subjek1.'"></td>';
					          $html .= '<td align="center"><input type="radio" id="janda'.$ig.'"  name="cek'.$pg.'" value="'.$s->subjek2.'"></td>';
					         
							  $html .= '<tr>';
							}



						}
						$html .= '</table></div>';

						$html .= '<div class="alert alert-warning">  Jawaban  '.$s->jawaban.'</div>';
						
					}else if($s->jenis==8){
							
					   $html .= '<hr><div class="row">';
						$html .= ' <div class="table-responsive"> <table class="table-hover table  ">';
					
						 
						  $pertanyaan   = explode("<br />",$s->jawaban_essay);
					  
							foreach($pertanyaan as $ig=>$per){

								$pert   = explode(".",$per);
	
								if(!empty($pert[0])){
						
								  $html .= '<tr>';
								 
								  $html .= '<td align="left">'.$pert[0].'</td>';
								  $html .= '<td align="left"><input type="text" class="form-control" placeholder="Ketik Jawaban Anda disini "></td>';
								
								 
								  $html .= '<tr>';
								}
	
	
	
							}
							$html .= '</table></div>';

							$html .= '<div class="alert alert-warning">  Jawaban  '.$s->jawaban.'</div>';
					}
				
				
				
				$html .= '</div></div>';
				$no++;
			}
		}

		// Enkripsi Id Tes
		$id_tes = base64_encode($key);

		$data = [
			
			'judul'		=> 'Ujian',
			'subjudul'	=> 'Lembar Ujian',
			'soal'		=> "Simulasi",
			'no' 		=> $no,
			'html' 		=> $html,
			'id_tes'	=> $id_tes
		];
		
		 $data['ujian']	      = $this->Reff->get_where("tm_ujian",array("id"=>$id));
		 $data['title']	      = "UJIAN - ".$data['ujian']->nama;
		 
		 $this->load->view("teachercbt/header",$data);
		 $this->load->view('teachercbt/sheet');
		 //$this->load->view("teachercbt/footer",$data);
	}
	
	
	public function token($nama=null,$id=null,$token=null)
	{  
	    
		  
		  
	    		
         $id                  = base64_decode($id);
		 $data['data']	      = $this->Reff->get_where("tm_siswa",array("id"=>$_SESSION['tmsiswa_id']));
		 $data['madrasah']	  = $this->Reff->get_where("tm_madrasah",array("id"=>$data['data']->tmmadrasah_id));
		 $data['ujian'] 	  = $this->Reff->get_where("tm_ujian",array("id"=>$id));
		 $data['title']   = "Computer Based Test - ".$_SESSION['nama_siswa'];
		 
		 $this->load->view("cbt/header",$data);
		 $this->load->view('cbt/token');
		 $this->load->view("cbt/footer",$data);
	    
	

	}
	
	public function cektoken()
	{
		$id    = $this->input->post('id_ujian', true);
		$token = $this->input->post('token', true);
		$cek   = $this->M_cbt->getUjianById($id);
		
		$data['status'] = $token === $cek->token ? TRUE : FALSE;
		$this->output_json($data);
	}
	
	public function simpan_satu()
	{
	
		$id_tes = $this->input->post('id', true);
		$id_tes = base64_decode($id_tes);
		
		$input 	= $this->input->post(null, true);
		$list_jawaban 	= "";
		for ($i = 1; $i < $input['jml_soal']; $i++) {
			$_tjawab 	= "opsi_".$i;
			$_tidsoal 	= "id_soal_".$i;
			$_ragu 		= "rg_".$i;
			$jawaban_ 	= empty($input[$_tjawab]) ? "" : $input[$_tjawab];
			$list_jawaban	.= "".$input[$_tidsoal].":".$jawaban_.":".$input[$_ragu].",";
		}
		$list_jawaban	= substr($list_jawaban, 0, -1);
		$d_simpan = [
			'list_jawaban' => $list_jawaban
		];
	
		$this->db->update('h_ujian', $d_simpan,array("id"=>$id_tes));
		$this->output_json(['status'=>true]);
	}

	public function simpan_akhir()
	{
		// Decrypt Id
		$id_tes = $this->input->post('id', true);
		$id_tes = base64_decode($id_tes);
		
		// Get Jawaban
		$list_jawaban = $this->M_cbt->getJawaban($id_tes);

		// Pecah Jawaban
		$pc_jawaban = explode(",", $list_jawaban);
		
		$jumlah_benar 	= 0;
		$jumlah_salah 	= 0;
		$jumlah_ragu  	= 0;
		$nilai_bobot 	= 0;
		$total_bobot	= 0;
		$jumlah_soal	= sizeof($pc_jawaban);

		foreach ($pc_jawaban as $jwb) {
			$pc_dt 		= explode(":", $jwb);
			$id_soal 	= $pc_dt[0];
			$jawaban 	= $pc_dt[1];
			$ragu 		= $pc_dt[2];

			$cek_jwb 	= $this->M_cbt->getSoalById($id_soal);
			$total_bobot = $total_bobot + $cek_jwb->bobot;

			$jawaban == $cek_jwb->jawaban ? $jumlah_benar++ : $jumlah_salah++;
		}

		$nilai = ($jumlah_benar / $jumlah_soal)  * 100;
		$nilai_bobot = ($total_bobot / $jumlah_soal)  * 100;

		$d_update = [
			'jml_benar'		=> $jumlah_benar,
			'nilai'			=> number_format(floor($nilai), 0),
			'nilai_bobot'	=> number_format(floor($nilai_bobot), 0),
			'status'		=> 'N'
		];
        
		$this->db->update('h_ujian', $d_update,array("id"=>$id_tes));
		
		$tmujian_id    = $this->Reff->get_kondisi(array("id"=>$id_tes),"h_ujian","tmujian_id");
		$ujian         = $this->db->get_where("tm_ujian",array("id"=>$tmujian_id))->row();
		$this->output_json(['status'=>TRUE, 'data'=>$d_update, 'id'=>$id_tes,'nama'=>base64_encode($ujian->nama),'id'=>base64_encode($ujian->id),'token'=>base64_encode($ujian->token)]);
	}
	
	
	 public function output_json($data, $encode = true)
	{
        if($encode) $data = json_encode($data);
        $this->output->set_content_type('application/json')->set_output($data);
	}
	
	
	
	 
	 
	 
	 
}
