<?php
error_reporting(0);
defined('BASEPATH') OR exit('No direct script access allowed');

class Banksoal extends CI_Controller {

	 public function __construct()
      {
        parent::__construct();
		 if(!$this->session->userdata("pusatIdAdmin")){
			    
				echo $this->Reff->sessionhabis();
				exit();
		  }
		  $this->load->model('M_soal','m');
		  $this->load->helper('exportpdf_helper');  
	  }
	  
   function _template($data)
	{
	  $this->load->view('pusat/page_header',$data);	
	}
		
	public function buatsoal()
	{  
	    
		  
		  
	    		
         $ajax            = $this->input->get_post("ajax",true);	
		
		 $data['title']   = "Data  Soal ";
	     if(!empty($ajax)){
					    
			 $this->load->view('page',$data);
		
		 }else{
			 
		     $data['konten'] = "page";
			 
			 $this->_template($data);
		 }
	

	}
	
	 public function grid(){
		
		  $iTotalRecords = $this->m->grid(false)->num_rows();
		  
		  $iDisplayLength = intval($_REQUEST['length']);
		  $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
		  $iDisplayStart = intval($_REQUEST['start']);
		  $sEcho = intval($_REQUEST['draw']);
		  
		  $records = array();
		  $records["data"] = array(); 

		  $end = $iDisplayStart + $iDisplayLength;
		  $end = $end > $iTotalRecords ? $iTotalRecords : $end;
		  
		  $datagrid = $this->m->grid(true)->result_array();
		   
		   $i= ($iDisplayStart +1);
		   foreach($datagrid as $val) {
				
			 $jml_soal = $this->db->query("select count(id) as jml from tr_soal where tmujian_id='".$val['id']."'")->row();
			

			 $checked ="";
			if($val['status']==1){

				$checked ="checked";
			}
				$no = $i++;
				$records["data"][] = array(
					$no,
					$this->Reff->get_kondisi(array("id"=>$val['pelatihan_id']),"pelatihan","nama"),
					$val['nama'],
					$val['token'],
					
					$jml_soal->jml,
					$val['waktu']." Menit",
					'<input type="checkbox" tmujian_id="'.$val['id'].'" class="aktivasi" '.$checked.'>',
					
					'<a href="#"  class="btn btn-primary btn-sm buatujian"  tmujian_id="'.$val['id'].'"><i class="fa fa-edit"></i> Buat Soal </a>
					
					<a class="btn btn-success btn-sm"   href="'.site_url("teachercbt/simulasi?start=".base64_encode($val['id'])."").'" style="color:white" title="Simulasi Ujian"><i class="fa fa-retweet"></i>  </a>
					
					 
					  
					  <button type="button" class="btn btn-warning btn-sm hapus"  datanya="'.$val['id'].'"   style="color:white" urlnya="'.site_url("banksoal/truncate").'" >
					  <i class="fa fa-trash"></i>  Truncate
		              </button>
					  <button type="button" class="btn btn-danger btn-sm ubahmodal" datanya="'.$val['id'].'" urlnya="'.site_url("banksoal/form").'" target="#loadform" data-toggle="modal" data-target="#defaultModal">
					  <i class="fa fa-edit"></i> 
					  </button>
					  <button type="button" class="btn btn-danger btn-sm hapus"  datanya="'.$val['id'].'" urlnya="'.site_url("banksoal/hapus").'" >
					  <i class="fa fa-trash"></i>  
		              </button>
					'
					
				  
				    

				  );
			  }
		
		  $records["draw"] = $sEcho;
		  $records["recordsTotal"] = $iTotalRecords;
		  $records["recordsFiltered"] = $iTotalRecords;
		  
		  echo json_encode($records);
	}


	public function aktivasi(){

		$this->db->where("id",$_POST['tmujian_id']);
		$this->db->set("status",$_POST['status']);
		$this->db->update("tm_ujian");

		echo "sukses";
	}

	public function statussoal(){
		$this->db->where("id",$_POST['id']);
		$this->db->set("status",$_POST['status']);
		$this->db->update("tm_ujian");

		echo "sukses";


	}

	public function hapus(){

		$this->db->where("id",$_POST['id']);
		$this->db->delete("tm_ujian");

		$this->db->where("tmujian_id",$_POST['id']);
		$this->db->delete("tr_soal");
		echo "sukses";
   

	}
	
	public function truncate(){

           $this->db->where("tmujian_id",$_POST['id']);
		   $this->db->delete("tr_soal");
		   echo "sukses";

	}

	public function analisis(){
		$data['id'] = $_POST['id'];
		 
		$this->load->view("analisis",$data); 



	}

	public function form(){
		
		$id = $this->input->get_post("id");
		$data = array();
		   if(!empty($id)){
			   
			  $data['data']  = $this->Reff->get_where("tm_ujian",array("id"=>$id));
			   
		   }
		$this->load->view("form",$data);
		
	}
	
	
	
	public function save(){
     
        $this->form_validation->set_message('required', '{field} Wajib diisi. ');
         $this->form_validation->set_message('is_unique', '{field} Sudah terdaftar , silahkan masukan NISN lain.');
			    $this->form_validation->set_message('min_length', '%s: Minimal  %s digit');
         $this->form_validation->set_message('max_length', '%s: Maksimal  %s digit');
    
		 
			
				$config = array(
				    
				   
				    array('field' => 'f[pelatihan_id]', 'label' => 'Pelatihan  ', 'rules' => 'trim|required'),
				   
				   
			
				   
				  
				   
				   
	
					
				);
				
				$this->form_validation->set_rules($config);	
        
        if ($this->form_validation->run() == true) {
			  
			    $id = $this->input->get_post("id",true);
			    $f  = xssArray($this->input->get_post("f",true));
				
				
				 
				
							 if(empty($id)){
								  
								  $token = strtoupper($this->Reff->get_id(10)); 
								   
								   $this->db->set("token",$token);
								   $this->db->insert("tm_ujian",$f);
								   echo "Data Berhasil disimpan";	
								 
								
								
							 }else{
								
								$this->db->where("id",$id);
								$this->db->update("tm_ujian",$f);
								echo "Data Berhasil disimpan";	
								 
							 }
							 
						
							
						
		
		
							
			     
			    
												 
	     } else {
	         
           
                header('Content-Type: application/json');
                echo json_encode(array('error' => true, 'message' => validation_errors()));
            
        }

		
	
	
	}

	public function cetak(){
	    
	    	 		 
			
		$data['soal']      = $this->db->query("select * from   tm_ujian where id='".($_GET['tmujian_id'])."'")->row();
			
		$pdf_filename = $data['soal']->nama."-Paket_".$data['soal']->paket.".pdf";	 
	
		$user_info = $this->load->view('cetak', $data, true);
	
		 $output = $user_info;
		
		 echo $output;
	
		//generate_pdf($output, $pdf_filename,TRUE);
		


   }

   public function tarikSoal(){
	$soalBank = $this->db->query("SELECT * from tr_soalbank where tmujian_id=2")->result();
	  foreach($soalBank as $row){

		$this->db->set("tmujian_id",44);
		$this->db->set("jenis",1);
		$this->db->set("soal",$row->soal);
		$this->db->set("opsi_a",$row->opsi_a);
		$this->db->set("opsi_b",$row->opsi_b);
		$this->db->set("opsi_c",$row->opsi_c);
		$this->db->set("opsi_d",$row->opsi_d);
		$this->db->set("opsi_e",$row->opsi_e);
		$this->db->set("jawaban",$row->jawaban);
		$this->db->set("bobot",$row->bobot);
		$this->db->set("urutan",$row->urutan);
		$this->db->insert("tr_soal");

	  }

   }
	 
}
