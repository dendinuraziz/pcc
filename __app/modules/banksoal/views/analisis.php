<?php 
  $ujian = $this->db->get_where("tm_ujian",array("id"=>$id))->row();

  $jml_soal = $this->db->query("select urutan,soal,id  from tr_soal where tmujian_id='".$id."'")->result();

  $h_ujian = $this->db->get_where("h_ujian",array("tmujian_id"=>$id))->result();

?>
<div class="alert alert-warning">
  Literasi <?php echo $ujian->nama; ?> - Paket <?php echo $ujian->paket; ?>

</div>
<br>
<hr>


<div class="table-responsive">
  <table class="table table-hover table-bordered table-stripped " id="analisissoaldata">
    <thead>
    <tr>
       <th>NO</th>
       <th>NO AKSI</th>
       <th>NAMA SISWA</th>
       <th>MADRASAH</th>
        <?php 
         foreach($jml_soal as $js){

            ?><th><?php echo $js->urutan; ?></th><?php 

  

         }
        ?>
    </tr>
    </thead>

    <tbody>

   

      <?php 

      
       $no=0;
        foreach($h_ujian as $hj){
            $no++;
      ?>
        <tr> 
           <td><?php echo $no; ?></td>
           <td><?php echo $this->Reff->get_kondisi(array("id"=>$hj->tmsiswa_id),"v_siswa","no_aksi"); ?></td>
           <td><?php echo $this->Reff->get_kondisi(array("id"=>$hj->tmsiswa_id),"v_siswa","nama"); ?></td>
           <td><?php echo $this->Reff->get_kondisi(array("id"=>$hj->tmsiswa_id),"v_siswa","madrasah"); ?></td>
           
           <?php 
         foreach($jml_soal as $i=>$js){
              
            $listjawaban = explode(",",$hj->list_jawaban);
            $jwbn        = explode(":",$listjawaban[$i]);

            if($jwbn[1]==1){

               ?><td style="background-color:green;color:white"> <?php echo $jwbn[1]; ?></td><?php 

            }else if($jwbn[1]==0){

                ?><td>  <?php echo $jwbn[1]; ?></td><?php 
            }else{

                ?><td> <?php echo $jwbn[1]; ?></td><?php 
            }

  

         }
        ?>
        </tr>

    <?php 
        }
        ?>


    </tbody>
</table>
</div>

<script type="text/javascript">
$(document).ready(function() {
   

    $('#analisissoaldata').DataTable( {
        dom: 'Bfrtip',
        buttons: [
             'excel', 'csv'
        ]
    });


} );


</script>
