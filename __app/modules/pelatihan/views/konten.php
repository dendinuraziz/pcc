<style>


.letter {
  background: #fff;
  box-shadow: 0 0 10px rgba(0,0,0,0.3);
 
  max-width: 850px;
  height: 650px;
  padding: 5px;
  overflow-y:scroll;
  overflow-x: hidden;
  position: relative;
  width: 100%;
}


</style>
                
   <div class="single-course-wrap mb-0">
                        <div class="thumb">
                            <?php 
                              if($dataIsi->konten_belajar==1 or $dataIsi->konten_belajar==9 or $dataIsi->konten_belajar==10){
                            ?>

                            <div class="letter">
                            <p><?php echo $dataIsi->konten; ?></p>
                            </div>

                      
                                

                            <?php 
                              }else if($dataIsi->konten_belajar==4 or $dataIsi->konten_belajar==5 or $dataIsi->konten_belajar==6){
                                ?>
                           
                            <iframe width="100%" height="400" src="<?php echo $dataIsi->konten; ?>" title="<?php echo $this->Reff->get_kondisi(array("id"=>$dataIsi->subsilabus_id),"subsilabus","nama"); ?>" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                              

                                <?php 

                              }else if($dataIsi->konten_belajar==7){
                                ?>
                                <iframe src="https://drive.google.com/file/d/<?php echo $dataIsi->konten; ?>/preview" width="100%" height="400"></iframe>
                               
                            <?php 

                              }
                            ?>
                        </div>
                        <div class="wrap-details">

                            <h5><a href="#"><?php echo $this->Reff->get_kondisi(array("id"=>$dataIsi->subsilabus_id),"subsilabus","nama"); ?></a></h5>
                            
                            <div class="user-area">
                                <div class="user-details">
                                    <img src="<?php echo base_url(); ?>__statics/img/author/1.png" alt="img">
                                    <a href="#">GTK Madrasah  </a>
                                </div>
                                <div class="date ms-auto">
                                    <i class="fa fa-calendar-alt me-2" style="color: var(--main-color);"></i> <?php echo $this->Reff->formattanggalstring($pelatihan->tanggal); ?>
                                </div>
                                
                                <div class="user-rating">
                                 <?php
                                    
                                  $waktu 	= $this->Reff->get_kondisi(array("id"=>$dataIsi->subsilabus_id),"subsilabus","durasi") * 60;
                                 ?>
                                <input type="hidden" id="waktu_selesai" value="<?php echo $waktu; ?>">
                                <span class="sisawaktu" data-time=""></span></span> 
                                </div>
                            </div>

                            
                            
                        </div>


                    </div>
                    
                    <div class="tab-content" id="pills-tabContent">

                        <div class="tab-pane fade show active" id="pills-01" role="tabpanel" aria-labelledby="pill-1"> <?php echo $dataIsi->deskripsi; ?></div>
                       
                       
                    </div>

                   <!--  <div class="row">
                                <center>
                                <div class="alert alert-primary">
                                    Apakah Anda sudah paham materi <b><?php // echo $this->Reff->get_kondisi(array("id"=>$dataIsi->subsilabus_id),"subsilabus","nama"); ?></b> ,
                                    dan akan melanjutkan ke materi selanjutnya ?
                                    <br>
                                    <br>

                               
                                   <a class="btn btn-base-light ms-3" href="javascript:void(0)" id="kasihpaham">Ya, saya paham </a>


                                </div>
                                </center>
                               
                                
                    </div> -->


<script type="text/javascript">
$(document).ready(function(){
    //$(this).scrollTop(0).slow();
    sisawaktu();
});

    // $(document).off("click","#kasihpaham").on("click","#kasihpaham",function(){

        // Swal.fire({
        // title: 'Apakah  sudah paham  ?',
        // text: "Materi selanjutnya akan otomatis terbuka jika Anda paham materi ini",
        // icon: 'warning',
        // showCancelButton: true,
        // confirmButtonColor: '#3085d6',
        // cancelButtonColor: '#d33',
        // confirmButtonText: 'Ya, saya akan ikut pelatihan'
        // }).then((result) => {
        //     if (result.value) {
              

                
                
        //     }
        // })

    // });


    
function sisawaktu() {

   var timeleft = $("#waktu_selesai").val();
 
    var downloadTimer = setInterval(function(){
    if(timeleft <= 0){
        clearInterval(downloadTimer);
        
                var pelatihan_id  = "<?php echo $pelatihan->id; ?>";
                var subsilabus_id = "<?php echo $dataIsi->subsilabus_id; ?>";
                var durasi        = $("#waktu_selesai").val();
                $.post("<?php echo site_url('pelatihan/kasihpaham') ?>",{pelatihan_id:pelatihan_id,subsilabus_id:subsilabus_id,durasi:durasi},function(data){
                  
                     $("#MenuArea").html(data);

                });
            
            
               


    } else {

       
        var sisawaktu = new Date(timeleft * 1000).toISOString().substr(11, 8);	

        $("#waktu_selesai").val(timeleft);
        $(".sisawaktu").html(sisawaktu);

        
        
    }
    timeleft -= 1;
    }, 1000);
}


    </script>