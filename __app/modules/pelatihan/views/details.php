<style>
    .btn2 {
  display: inline-block;
  margin-bottom: 0;
  font-weight: normal;
  text-align: center;
  white-space: nowrap;
  vertical-align: middle;
  -ms-touch-action: manipulation;
  touch-action: manipulation;
  cursor: pointer;
  background-image: none;
  border: 1px solid transparent;
  padding: 6px 12px;
  font-size: 14px;
  line-height: 1.42857143;
  border-radius: 4px;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
}
</style>

<section class="courses-details-area pd-top-65 pd-bottom-130">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <?php $this->load->view($isi); ?>
                </div>
                <div class="col-lg-4 sidebar-area" id="MenuArea">
                    <?php $this->load->view("menu"); ?>
                </div>
            </div>
        </div>
    </section>

    <script type="text/javascript">
     $(document).off("click","#ikutiPelatihan").on("click","#ikutiPelatihan",function(){

        Swal.fire({
        title: 'Apakah Anda yakin akan mengikuti Pelatihan ini ?',
        text: "Jika Anda mengikuti pelatihan ini ? ",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya, saya akan ikut pelatihan'
        }).then((result) => {
            if (result.value) {
              

                var pelatihan_id = "<?php echo $pelatihan->id; ?>";
                $.post("<?php echo site_url('pelatihan/ikut') ?>",{pelatihan_id:pelatihan_id},function(){
                    
                    location.reload();

                });
                
            }
        })

     });

    </script>