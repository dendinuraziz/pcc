<?php
use setasign\Fpdi\Fpdi;
require_once(APPPATH.'libraries/fpdf/fpdf.php');
require_once(APPPATH.'libraries/fpdi/src/autoload.php');
defined('BASEPATH') OR exit('No direct script access allowed');

class Pelatihan extends CI_Controller {

	 public function __construct()
      {
        parent::__construct();
		date_default_timezone_set("Asia/Jakarta");	
		//   if(!$this->session->userdata("peserta_id")){
			    
		// 		redirect(site_url("web/sign"));
			
		//   }
		  $this->load->model('M_pelatihan','m');
		
	  }
	  
   function _template($data)
	{
	  $this->load->view('page_header',$data);	
	}
		
	public function detail($slug)
	{  
	     
		 $ajax            	= $this->input->get_post("ajax",true);	
		// $data['data']    	=  $this->db->get_where("peserta",array("id"=>$_SESSION['peserta_id']))->row();
		 $data['pelatihan'] =  $this->db->get_where("pelatihan",array("slug"=>$slug))->row();
		 
		 $data['title']   = "Pelatihan ".$data['pelatihan']->nama;
		 
	     if(!empty($ajax)){
			 $data['isi']    = "default";		    
			 $this->load->view('details',$data);
		
		 }else{
			 
			    
		     $data['konten'] = "details";
		     $data['isi']    = "default";
			 
			 $this->_template($data);
		 }
	

	}


	public function ikut(){

		 $pelatihan_id = $this->input->post("pelatihan_id",true);
		 $cek 		   = $this->m->cekPelatihan($pelatihan_id)->num_rows();
		 $soal 		   = $this->m->getPelatihan($pelatihan_id)->result();
		 $list_silabus	= "";
		
		 if($cek < 1){
			if (count($soal) >0) {
				$list_silabus = array();
				foreach ($soal as $d) {
				
					$list_silabus[] = array("subsilabus_id"=>$d->id,"silabus_id"=>$d->silabus_id,"result"=>"T","tgl"=>NULL,"durasi"=>$d->durasi);
				}
			}

			
			$list_silabus 	= json_encode($list_silabus,true);
			$time_mulai		= date('Y-m-d H:i:s');

			
			$device = $this->Reff->mobile();
			$input = [
				'pelatihan_id' 		=> $pelatihan_id,				
				'peserta_id' 		=> $_SESSION['peserta_id'],				
				'list_silabus'		=> $list_silabus,
				'mulai'				=> $time_mulai,
				'device'	    => $device,
				'status'		=> 'Y'
			];
			
			
			$this->db->insert('tr_pelatihan', $input);
		}
		

	}


	public function study($slug,$id)
	{  
	     $subsilabus_id 	= base64_decode($id); 
		 $ajax            	= $this->input->get_post("ajax",true);	
		 $data['data']    	=  $this->db->get_where("peserta",array("id"=>$_SESSION['peserta_id']))->row();		
		 $data['dataIsi']   =  $this->db->get_where("konten",array("subsilabus_id"=>$subsilabus_id))->row();
		 $data['pelatihan'] =  $this->db->get_where("pelatihan",array("id"=>$data['dataIsi']->pelatihan_id))->row();
		 $data['title']     = $data['pelatihan']->nama;


		// $trpelatihan = $this->db->query("select * from tr_pelatihan where pelatihan_id='{$data['pelatihan']->id}' and peserta_id='{$_SESSION['peserta_id']}'")->row();
		// $list_silabus = json_decode($trpelatihan->list_silabus,true);
		// foreach($list_silabus as $key => $value)
		// {
		// 	if($list_silabus[$key]['subsilabus_id']==$subsilabus_id){
		// 		$list_silabus[$key]['result'] = "Y";
		// 		$list_silabus[$key]['tgl'] = date("Y-m-d H:i:s");
		// 	}
			
		// }

		// $list_silabus 	= json_encode($list_silabus,true);
		
		// $this->db->where("pelatihan_id",$data['pelatihan']->id);
		// $this->db->where("peserta_id",$_SESSION['peserta_id']);
		// $this->db->set("list_silabus",$list_silabus);
		// $this->db->update("tr_pelatihan");
		


		 
	     if(!empty($ajax)){

			 $data['isi']    = "konten";		    
			 $this->load->view('details',$data);
		
		 }else{
			 
			    
		     $data['konten'] = "details";
		     $data['isi']    = "konten";
			 
			 $this->_template($data);
		 }
	

	}

	public function kasihpaham(){
		$pelatihan_id  = $this->input->get_post("pelatihan_id",true);
		$subsilabus_id = $this->input->get_post("subsilabus_id",true);
		$durasi        = $this->input->get_post("durasi",true);

		$pelatihan = $this->db->query("select * from tr_pelatihan where pelatihan_id='{$pelatihan_id}' and peserta_id='{$_SESSION['peserta_id']}'")->row();
		$list_silabus = json_decode($pelatihan->list_silabus,true);
		foreach($list_silabus as $key => $value)
		{
			if($list_silabus[$key]['subsilabus_id']==$subsilabus_id){
				$list_silabus[$key]['result'] = "Y";
				$list_silabus[$key]['tgl'] = date("Y-m-d H:i:s");
				$list_silabus[$key]['durasi'] = $durasi;
			}
			
		}

		$list_silabus 	= json_encode($list_silabus,true);
		
		$this->db->where("pelatihan_id",$pelatihan_id);
		$this->db->where("peserta_id",$_SESSION['peserta_id']);
		$this->db->set("list_silabus",$list_silabus);
		$this->db->update("tr_pelatihan");


		 $data['data']    	=  $this->db->get_where("peserta",array("id"=>$_SESSION['peserta_id']))->row();		
		 $data['dataIsi']   =  $this->db->get_where("konten",array("subsilabus_id"=>$subsilabus_id))->row();
		 $data['pelatihan'] =  $this->db->get_where("pelatihan",array("id"=>$pelatihan_id))->row();

		 $this->load->view("menu",$data);
		
	}



	public function sertifikat(){

		$pelatihan_id  = base64_decode($this->input->get_post("pelatihan_id",true));
		$pelatihan         = $this->db->get_where("pelatihan",array("id"=>$pelatihan_id))->row();
		$h_ujian = $this->db->query("select * from h_ujian where pelatihan_id='" . $pelatihan_id . "' and tmsiswa_id='" . $_SESSION['peserta_id'] . "'")->row();
		if($h_ujian->nilai >= $pelatihan->grade){

		
		   $this->db->set("sertifikat",1);
		   $this->db->set("pelatihan_id",$pelatihan_id);
		   $this->db->set("peserta_id",$_SESSION['peserta_id']);
		   $this->db->update("tr_pelatihan");



			     

				$datasiswa =$this->db->get_where("peserta",array("id"=>$_SESSION['peserta_id']))->row();
					
				if(count($datasiswa) >0){

					$pdf = new Fpdi();

					$this->load->library('ciqrcode'); //pemanggilan library QR CODE

					$config['cacheable']	= true; 
					$config['cachedir']		= './tmp/chache'; 
					$config['errorlog']		= './tmp/'; 
					$config['imagedir']		= './tmp/'; 
					$config['quality']		= true; 
					$config['size']			= '1024'; 
					$config['black']		= array(224,255,255);
					$config['white']		= array(70,130,180); 
					$this->ciqrcode->initialize($config);

					//$image_name=$datasiswa->id.'.png'; 
					$image_name= $datasiswa->id.'.png'; 

					$params['data'] = "https://apps-gtkmadrasah.kemenag.go.id/web/validasi/sertifikat=".base64_encode($pelatihan_id).""; 
					$params['level'] = 'H'; 
					$params['size'] = 10;
					$params['savename'] = FCPATH.$config['imagedir'].$image_name; 
					$this->ciqrcode->generate($params);

			
				
						$status =   "PESERTA";				
						
					


						$pdf->addPage('L');
						
						$pdf->setSourceFile('sertifikat.pdf');
					
						$tplIdx = $pdf->importPage(1);
						$pdf ->useTemplate($tplIdx, null, null, null, null,FALSE);
						
						
						$pdf->SetFont('Arial','B',26);
						$pdf->SetTextColor(6, 0, 0);

						$pdf->Cell(null,93,$datasiswa->nama,0,0,'C');

						$pdf->Ln(10);
						$pdf->SetFont('Arial','B',15);
						$pdf->SetTextColor(6, 0, 0);
						$pdf->Cell(null,93,$datasiswa->instansi,0,0,'C');

						$pdf->Ln(20);
						$pdf->SetFont('Arial','B',26);
						$pdf->SetTextColor(6, 0, 0);
						$pdf->Cell(271,93,$status,0,0,'C');


						$pdf->Ln(15);
						$pdf->SetFont('Arial','B',15);
						$pdf->SetTextColor(6, 0, 0);
						$pdf->Cell(null,93,$pelatihan->nama,0,0,'C');

						
						$tgl_selesai = explode(" ",$h_ujian->tgl_selesai);
						$selesai     = $this->Reff->formattanggalstring($tgl_selesai[0]);
						$pdf->Ln(40);
						$pdf->SetFont('Arial','B',15);
						$pdf->SetTextColor(6, 0, 0);
						$pdf->Cell(null,93,"Jakarta, ".$selesai,0,0,'C');
						
						
						//$pdf->Write(0, '');


						$pdf->Image("tmp/".$image_name,260,170,30,30);


						$pdf->Output('I', 'SERTIFIKAT  '.str_replace(array("'","-",":","."),"",$pelatihan->nama).time().'.pdf');

						

				}else{

					echo "404 Not Found";
				}


		}else{
			$token = base64_encode($this->Reff->get_kondisi(array("pelatihan_id"=>$pelatihan->id),"tm_ujian","token"));
			redirect(site_url("cbt/dashboard/".$token.""));
		}


	}






}
