<?php

class M_pelatihan extends CI_Model {
 
    public function __construct() {
        parent::__construct();
    }

    
	public function cekPelatihan($pelatihan_id){

        $this->db->select("*");
        $this->db->from("tr_pelatihan");
        $this->db->where("pelatihan_id",$pelatihan_id);
        $this->db->where("peserta_id",$_SESSION['peserta_id']);
        return $this->db->get();
    }

    public function getPelatihan($pelatihan_id){

        $this->db->select("*");
        $this->db->from("v_silabus");
        $this->db->where("pelatihan_id",$pelatihan_id);
        $this->db->order_by("silabus_urutan","ASC");
        $this->db->order_by("urutan","ASC");       
        return $this->db->get();
    }

    public function showProg($pelatihan_id,$silabus_id){
       
        return  $this->db->query('select * from tr_pelatihan where json_contains(list_silabus, \'{"silabus_id" : "'.$silabus_id.'","result" : "Y"}\') and peserta_id="'.$_SESSION['peserta_id'].'"')->num_rows();
               
    }

    public function showProgsub($pelatihan_id,$subsilabus_id){
       
        return  $this->db->query('select * from tr_pelatihan where json_contains(list_silabus, \'{"subsilabus_id" : "'.$subsilabus_id.'","result" : "Y"}\') and peserta_id="'.$_SESSION['peserta_id'].'"')->num_rows();
               
    }

    public function progress($pelatihan_id){

        $data = $this->db->query("select * from tr_pelatihan where pelatihan_id='{$pelatihan_id}' and peserta_id='{$_SESSION['peserta_id']}'")->row();
        if(!is_null($data)){
		$list_silabus = json_decode($data->list_silabus,true);
        $no=0;
		foreach($list_silabus as $key => $value)
		{
			if($list_silabus[$key]['result']=="Y"){
				$no++;
			}
			
		}
        return $no;
     }else{
         return 0;
     }

    }

    public function progressSilabus($pelatihan_id,$silabus_id){

        $data = $this->db->query("select * from tr_pelatihan where pelatihan_id='{$pelatihan_id}' and peserta_id='{$_SESSION['peserta_id']}'")->row();
        if(!is_null($data)){
		$list_silabus = json_decode($data->list_silabus,true);
        $no=0;
		foreach($list_silabus as $key => $value)
		{
            
            if($list_silabus[$key]['silabus_id']==$silabus_id){
                if($list_silabus[$key]['result']=="Y"){
                    $no++;
                }
            }
			
		}
        return $no;
     }else{
         return 0;
     }

    }
	
	
}
