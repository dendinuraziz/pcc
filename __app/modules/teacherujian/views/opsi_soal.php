<style>
.funkyradio div {
  clear: both;
  overflow: hidden;
}

.funkyradio label {
  width: 100%;
  border-radius: 3px;
  border: 1px solid #D1D3D4;
  font-weight: normal;
}

.funkyradio input[type="radio"]:empty,
.funkyradio input[type="checkbox"]:empty {
  display: none;
}

.funkyradio input[type="radio"]:empty ~ label,
.funkyradio input[type="checkbox"]:empty ~ label {
  position: relative;
  line-height: 2.5em;
  text-indent: 3.25em;
  margin-top: 2em;
  cursor: pointer;
  -webkit-user-select: none;
     -moz-user-select: none;
      -ms-user-select: none;
          user-select: none;
}

.funkyradio input[type="radio"]:empty ~ label:before,
.funkyradio input[type="checkbox"]:empty ~ label:before {
  position: absolute;
  display: block;
  top: 0;
  bottom: 0;
  left: 0;
  content: '';
  width: 2.5em;
  background: #D1D3D4;
  border-radius: 3px 0 0 3px;
}

.funkyradio input[type="radio"]:hover:not(:checked) ~ label,
.funkyradio input[type="checkbox"]:hover:not(:checked) ~ label {
  color: #888;
}

.funkyradio input[type="radio"]:hover:not(:checked) ~ label:before,
.funkyradio input[type="checkbox"]:hover:not(:checked) ~ label:before {
  content: '\2714';
  text-indent: .9em;
  color: #C2C2C2;
}

.funkyradio input[type="radio"]:checked ~ label,
.funkyradio input[type="checkbox"]:checked ~ label {
  color: #777;
}

.funkyradio input[type="radio"]:checked ~ label:before,
.funkyradio input[type="checkbox"]:checked ~ label:before {
  content: '\2714';
  text-indent: .9em;
  color: #333;
  background-color: #ccc;
}

.funkyradio input[type="radio"]:focus ~ label:before,
.funkyradio input[type="checkbox"]:focus ~ label:before {
  box-shadow: 0 0 0 3px #999;
}

.funkyradio-default input[type="radio"]:checked ~ label:before,
.funkyradio-default input[type="checkbox"]:checked ~ label:before {
  color: #333;
  background-color: #ccc;
}

.funkyradio-primary input[type="radio"]:checked ~ label:before,
.funkyradio-primary input[type="checkbox"]:checked ~ label:before {
  color: #fff;
  background-color: #337ab7;
}

.funkyradio-success input[type="radio"]:checked ~ label:before,
.funkyradio-success input[type="checkbox"]:checked ~ label:before {
  color: #fff;
  background-color: #5cb85c;
}

.funkyradio-danger input[type="radio"]:checked ~ label:before,
.funkyradio-danger input[type="checkbox"]:checked ~ label:before {
  color: #fff;
  background-color: #d9534f;
}

.funkyradio-warning input[type="radio"]:checked ~ label:before,
.funkyradio-warning input[type="checkbox"]:checked ~ label:before {
  color: #fff;
  background-color: #f0ad4e;
}

.funkyradio-info input[type="radio"]:checked ~ label:before,
.funkyradio-info input[type="checkbox"]:checked ~ label:before {
  color: #fff;
  background-color: #5bc0de;
}

.input-group-addon {
  border-left-width: 0;
  border-right-width: 0;
}
.input-group-addon:first-child {
  border-left-width: 1px;
}
.input-group-addon:last-child {
  border-right-width: 1px;
}

</style>

<?php 

  if($jenis==1){
													    $pilihanganda = array("A"=>"opsi_a","B"=>"opsi_b","C"=>"opsi_c","D"=>"opsi_d","E"=>"opsi_e");
														  foreach($pilihanganda as $ig=>$pg){
															  ?>
																<div class="notbarsoal">
																<hr>
																     <h4>OPSI JAWABAN  <?php echo $ig; ?> </h4><br>
																	 <textarea id="editor<?php echo $pg; ?><?php echo $soal->id; ?>"  trsoal_id="<?php echo $soal->id; ?>" kolom="<?php echo $pg; ?>"   ><?php echo $soal->$pg; ?></textarea>
																	 
																	  <script type="text/javascript">
																
																
																 var editorjawaban = CKEDITOR.replace( 'editor<?php echo $pg; ?><?php echo $soal->id; ?>', {
																	        
																			  uploadUrl: base_url+'__statics/ckdrive/core/connector/php/connector.php?command=QuickUpload&type=Files&responseType=json&show=1',
																			  filebrowserBrowseUrl: base_url+'__statics/ckdrive/ckfinder.html',
																			  filebrowserImageBrowseUrl: base_url+'__statics/ckdrive/ckfinder.html?type=Images&show=1',
																			  filebrowserUploadUrl: base_url+'__statics/ckdrive/core/connector/php/connector.php?command=QuickUpload&type=Files&show=1',
																			  filebrowserImageUploadUrl: base_url+'__statics/ckdrive/core/connector/php/connector.php?command=QuickUpload&type=Images&show=1',
																			  enterMode : CKEDITOR.ENTER_BR,
                                                                              shiftEnterMode: CKEDITOR.ENTER_P,
																			  height:300,
																			  removeDialogTabs: 'image:advanced;link:advanced',
																			   allowedContent :true
																} );
																

																editorjawaban.on('instanceReady', function(evt) {
																		this.dataProcessor.htmlFilter.addRules({
																			elements: {
																				img: function(element) {
																					if (!element.hasClass('img-responsive')) {
																						element.addClass('img-responsive');
																					}
																				}
																			}
																		});
																	});


																editorjawaban.on('change',function(){
																	
																	cek_opsijawaban("<?php echo $pg; ?>","<?php echo $soal->id; ?>");
																});														
																															
																
																 </script>
																		  
																</div>
														  <?php } ?>
														
														
														<script type="text/javascript">
														function cek_opsijawaban(pg,soal){
																	var nilai = CKEDITOR.instances['editor'+pg+soal+''].getData();
																	var trsoal_id  = $("#editor"+pg+soal+"").attr("trsoal_id");
																	var kolom      = $("#editor"+pg+soal+"").attr("kolom");
																		$("#statussoal").html("<i class='fas fa-cog fa-spin'></i>    Sedang menyimpan..");
																			 $.post("<?php echo site_url("teacherujian/simpan_soal"); ?>",{trsoal_id:trsoal_id,kolom:kolom,nilai:nilai},function(){
																					 $("#statussoal").html("Tersimpan");
																			 })
                                                        }
														
														</script>
														
													   
											
																								   
												   <div class="row col-lg-12">
												
												
												
													         <div class="col-lg-2">
															     Kunci  
															    
															 </div>
															 <div class="col-lg-2">
															 <select class="form-control changejawaban" kolom="jawaban" trsoal_id="<?php echo $soal->id; ?>">
																	
																	<option value="A" <?php echo ($soal->jawaban=="A") ? "selected":""; ?>> A </option>
																	<option value="B" <?php echo ($soal->jawaban=="B") ? "selected":""; ?>> B </option>
																	<option value="C" <?php echo ($soal->jawaban=="C") ? "selected":""; ?>> C </option>
																	<option value="D" <?php echo ($soal->jawaban=="D") ? "selected":""; ?>> D </option>
																	<option value="E" <?php echo ($soal->jawaban=="E") ? "selected":""; ?>> E </option>
																</select>
															</div>
															 <div class="col-lg-2">
															     Bobot Nilai  
															    
															 </div>
															 <div class="col-lg-2">
															  <input class="form-control ketiksoal" onkeypress="return formatnilai(this);" trsoal_id="<?php echo $soal->id; ?>" kolom="bobot"   value="<?php echo $soal->bobot; ?>">  
															 </div>
															 
															  <div class="col-lg-4" style="float:right">
															   
															   <button class="btn btn-sm btn-info tambahsoal" tmujian_id="<?php echo $soal->tmujian_id ?>"><i class="la la-plus-circle"></i>  Tambah Soal </button>
                                                                														   
															
															 </div>
												   </div>
												  <br>
												  
				<?php 
	}else if($jenis==2){
		
		?>
		<div class="notbarsoal">
				<div class="funkyradio">
				<div class="funkyradio-danger">
					<input type="radio" class="benerteu"  jawaban="A" <?php if(isset($edit)){ echo ($soal->jawaban=="A") ? "checked":""; }  ?> trsoal_id="<?php echo $soal->id; ?>" name="radio<?php echo $soal->id; ?>" id="benar<?php echo $soal->id; ?>"  />
					<label for="benar<?php echo $soal->id; ?>">BENAR</label>
				</div>
		   
				<div class="funkyradio-danger">
					<input type="radio" class="benerteu" jawaban="B"  <?php if(isset($edit)){ echo ($soal->jawaban=="B") ? "checked":""; }  ?> trsoal_id="<?php echo $soal->id; ?>" name="radio<?php echo $soal->id; ?>" id="salah<?php echo $soal->id; ?>"  />
					<label for="salah<?php echo $soal->id; ?>">SALAH</label>
				</div>
			   
				</div>
				 <hr>
				  <div class="row">
						 <div class="col-lg-2">Bobot Nilai </div>
						 <div class="col-lg-4">
							 <input class="form-control ketiksoal" onkeypress="return formatnilai(this);" trsoal_id="<?php echo $soal->id; ?>" kolom="bobot"   value="<?php echo $soal->bobot; ?>">  
						 </div>
						<div class="col-lg-6" style="float:right">
							
							  <button class="btn btn-sm btn-info tambahsoal" tmujian_id="<?php echo $soal->tmujian_id ?>"><i class="la la-plus-circle"></i> Tambah Soal  </button>
                         </div>
				 </div>

				<input class="form-control ketiksoal" type="text" trsoal_id="<?php echo $soal->id; ?>" kolom="jawaban"   value="<?php echo $soal->jawaban; ?>" placeholder="Ketik jawaban disini ">  
									   
												   
		</div>

		
		
		<?php 
		
		
	}else if($jenis==3){
		
		?>
		<div class="notbarsoal">
		<input class="form-control ketiksoal" type="text" trsoal_id="<?php echo $soal->id; ?>" kolom="jawaban"   value="<?php echo $soal->jawaban; ?>" placeholder="Ketik jawaban disini ">  
		<br>
		
				<div class="alert alert-warning">Silahkan masukkan Jawaban dari soal essay, sistem akan mengkoreksi tingkat ketepatan jawaban menggunakan algoritma string matching </div>
				
          <hr>
				     <div class="col-lg-2">Bobot Nilai </div>
						 <div class="col-lg-2">
							 <input class="form-control ketiksoal" onkeypress="return formatnilai(this);" trsoal_id="<?php echo $soal->id; ?>" kolom="bobot"   value="<?php echo $soal->bobot; ?>">  
						 </div>
						
						<div class="col-lg-6" style="float:right">
							
							  <button class="btn btn-sm btn-info tambahsoal" tmujian_id="<?php echo $soal->tmujian_id ?>"><i class="la la-plus-circle"></i> Tambah Soal  </button>
                         </div>
							
												   
		</div>
		
		
		<?php 
		
		
		
	}else if($jenis==4){
		?>
		<div class="notbarsoal">
				<br/>
				<br/>
				<?php 
			         $pilihanganda = array("A"=>"opsi_a","B"=>"opsi_b","C"=>"opsi_c","D"=>"opsi_d","E"=>"opsi_e");
														  foreach($pilihanganda as $ig=>$pg){
															  if(isset($edit)){
															   $pecah = explode("[split]",$soal->$pg);
															  }
															  ?>
					<div class="row">
						<div class="input-group">
							  <div class="input-group-prepend">
								<span class="input-group-text" id=""><?php echo $ig; ?></span>
							  </div>
							  <input type="text" class="form-control inputjodoh" id="kolom1<?php echo $pg; ?><?php echo $soal->id; ?>" kolom="<?php echo $pg; ?>" trsoal_id="<?php echo $soal->id; ?>" value="<?php if(isset($edit)){ echo $pecah[0]; }  ?>" placeholder="Pertanyaan ">
							  <span class="input-group-text" id=""> <br> </span>
							  <input type="text" class="form-control inputjodoh" id="kolom2<?php echo $pg; ?><?php echo $soal->id; ?>" kolom="<?php echo $pg; ?>" trsoal_id="<?php echo $soal->id; ?>" value="<?php if(isset($edit)){ echo $pecah[1]; }  ?>"  placeholder="Jawaban..">
						</div>	
					</div>	
					
					
		 <?php } ?>			
				 
           <hr>
				  <div class="row">
				      
															
						 <div class="col-lg-2">		   <input class="form-control ketiksoal" rows="4" trsoal_id="<?php echo $soal->id; ?>" kolom="jawaban" placeholder="Ketik Jawaban disini " value="<?php echo $soal->jawaban; ?>"> </div>
						 <div class="col-lg-2">
							 <input class="form-control ketiksoal" onkeypress="return formatnilai(this);" trsoal_id="<?php echo $soal->id; ?>" kolom="bobot"   value="<?php echo $soal->bobot; ?>">  
						 </div>
						<div class="col-lg-2" style="float:right">
							 
							  <button class="btn btn-sm btn-info tambahsoal" tmujian_id="<?php echo $soal->tmujian_id ?>"><i class="la la-plus-circle"></i> Tambah Soal  </button>
                         </div>
				 </div>				 
		</div>
		
		<?php
		
	}else if($jenis==5){
		
		?>
		
			<div class="notbarsoal">
		
				
          <hr>
				   
						
						
						<div class="col-lg-6" style="float:right">
							
							  <button class="btn btn-sm btn-info tambahsoal" tmujian_id="<?php echo $soal->tmujian_id ?>"><i class="la la-plus-circle"></i> Tambah Soal  </button>
                         </div>
							
												   
		</div>
		
		
		<?php 
		
		
		
		
	}else if($jenis==6){

		$pilihanganda = array("A"=>"opsi_a","B"=>"opsi_b","C"=>"opsi_c","D"=>"opsi_d","E"=>"opsi_e");
		foreach($pilihanganda as $ig=>$pg){
			?>
			  <div class="notbarsoal">
				   <h4>OPSI JAWABAN  <?php echo $ig; ?> </h4><br>
				   <textarea id="editor<?php echo $pg; ?><?php echo $soal->id; ?>"  trsoal_id="<?php echo $soal->id; ?>" kolom="<?php echo $pg; ?>"   ><?php echo $soal->$pg; ?></textarea>
				   
					<script type="text/javascript">
			  
			  
			   var editorjawaban = CKEDITOR.replace( 'editor<?php echo $pg; ?><?php echo $soal->id; ?>', {
						 
							uploadUrl: base_url+'__statics/ckdrive/core/connector/php/connector.php?command=QuickUpload&type=Files&responseType=json&show=1',
							filebrowserBrowseUrl: base_url+'__statics/ckdrive/ckfinder.html',
							filebrowserImageBrowseUrl: base_url+'__statics/ckdrive/ckfinder.html?type=Images&show=1',
							filebrowserUploadUrl: base_url+'__statics/ckdrive/core/connector/php/connector.php?command=QuickUpload&type=Files&show=1',
							filebrowserImageUploadUrl: base_url+'__statics/ckdrive/core/connector/php/connector.php?command=QuickUpload&type=Images&show=1',
							height: 200,
							enterMode : CKEDITOR.ENTER_BR,
                            shiftEnterMode: CKEDITOR.ENTER_P,
							removeDialogTabs: 'image:advanced;link:advanced',
							 allowedContent :true
			  } );
			  

			  editorjawaban.on('instanceReady', function(evt) {
																		this.dataProcessor.htmlFilter.addRules({
																			elements: {
																				img: function(element) {
																					if (!element.hasClass('img-responsive')) {
																						element.addClass('img-responsive');
																					}
																				}
																			}
																		});
				});


			  editorjawaban.on('change',function(){
				  
				  cek_opsijawaban("<?php echo $pg; ?>","<?php echo $soal->id; ?>");
			  });														
																		  
			  
			   </script>
						
			  </div>
		<?php } ?>
	  
	  
	  <script type="text/javascript">
	  function cek_opsijawaban(pg,soal){
				  var nilai = CKEDITOR.instances['editor'+pg+soal+''].getData();
				  var trsoal_id  = $("#editor"+pg+soal+"").attr("trsoal_id");
				  var kolom      = $("#editor"+pg+soal+"").attr("kolom");
					  $("#statussoal").html("<i class='fas fa-cog fa-spin'></i>    Sedang menyimpan..");
						   $.post("<?php echo site_url("teacherujian/simpan_soal"); ?>",{trsoal_id:trsoal_id,kolom:kolom,nilai:nilai},function(){
								   $("#statussoal").html("Tersimpan");
						   })
	  }
	  
	  </script>
	  
	 

												 
 <div class="row col-lg-12">



		   <div class="col-lg-2">
			   Kunci  
			  
		   </div>
		   <div class="col-lg-2">
		   <input class="form-control ketiksoal"  trsoal_id="<?php echo $soal->id; ?>" kolom="jawaban"   value="<?php echo $soal->jawaban; ?>">  
	
		  
		  </div>
		   <div class="col-lg-2">
			   Bobot Nilai  
			  
		   </div>
		   <div class="col-lg-2">
			<input class="form-control ketiksoal" onkeypress="return formatnilai(this);" trsoal_id="<?php echo $soal->id; ?>" kolom="bobot"   value="<?php echo $soal->bobot; ?>">  
		   </div>
		   
			<div class="col-lg-4" style="float:right">
			 
			 <button class="btn btn-sm btn-info tambahsoal" tmujian_id="<?php echo $soal->tmujian_id ?>"><i class="la la-plus-circle"></i>  Tambah Soal </button>
																		 
		  
		   </div>
 </div>
<br>

<?php 


	}else if($jenis==7){


	 ?>
	 <br>
	 <br>
	   <div class="table-responsive">
		   <table class="table-hover table  ">
			    <tr>
					 <td width="60%"></td>
					 <td><input class="form-control ketiksoal"  trsoal_id="<?php echo $soal->id; ?>" kolom="subjek1"   value="<?php echo $soal->subjek1; ?>" placeholder="Subjek 1"></td>
					 <td><input class="form-control ketiksoal"  trsoal_id="<?php echo $soal->id; ?>" kolom="subjek2"   value="<?php echo $soal->subjek2; ?>" placeholder="Subjek 2"></td>
					 <td>Jawaban</td>
	           </tr>
	  <?php 

		$pilihanganda = array("A"=>"opsi_a","B"=>"opsi_b","C"=>"opsi_c","D"=>"opsi_d","E"=>"opsi_e");
		$jawabanganda = array("A"=>"file_a","B"=>"file_b","C"=>"file_c","D"=>"file_d","E"=>"file_e");
		foreach($pilihanganda as $ig=>$pg){
			$jj = $jawabanganda[$ig];
			?>
			   <tr>
					 <td><textarea class="form-control ketiksoal"  trsoal_id="<?php echo $soal->id; ?>" kolom="<?php echo $pg; ?>"><?php echo $soal->$pg; ?></textarea></td>
					 <td align="center"><input type="checkbox" name="cek<?php echo $pg; ?>"></td>
					 <td align="center"><input type="checkbox" name="cek<?php echo $pg; ?>"></td>
					 <td><input class="form-control ketiksoal"  trsoal_id="<?php echo $soal->id; ?>" kolom="<?php echo $jawabanganda[$ig]; ?>"   value="<?php echo $soal->$jj; ?>" ></td>
				
					
	           </tr>
		<?php } ?>
	  
		</table>
		</div>
	 

												 
 <div class="row col-lg-12">


  
		   <div class="col-lg-2">
		   <input class="form-control ketiksoal" rows="4" trsoal_id="<?php echo $soal->id; ?>" kolom="jawaban" placeholder="Ketik Jawaban disini " value="<?php echo $soal->jawaban; ?>">
			  
		   </div>
		   <div class="col-lg-2">
			<input class="form-control ketiksoal" onkeypress="return formatnilai(this);" trsoal_id="<?php echo $soal->id; ?>" kolom="bobot"   value="<?php echo $soal->bobot; ?>">  
		   </div>
		   
			<div class="col-lg-4" style="float:right">
			 
			 <button class="btn btn-sm btn-info tambahsoal" tmujian_id="<?php echo $soal->tmujian_id ?>"><i class="la la-plus-circle"></i>  Tambah Soal </button>
																		 
		  
		   </div>
 </div>
<br>

<?php 




}else if($jenis==8){
		
	?>
	<div class="notbarsoal">
	<textarea class="form-control ketiksoal" rows="4" trsoal_id="<?php echo $soal->id; ?>" kolom="jawaban_essay" placeholder="Ketik Pilihan disini "><?php echo $soal->jawaban_essay; ?></textarea>  
	<input class="form-control ketiksoal" rows="4" trsoal_id="<?php echo $soal->id; ?>" kolom="jawaban" placeholder="Ketik Jawaban disini " value="<?php echo $soal->jawaban; ?>">  
	<br>
	
			<div class="alert alert-warning">Silahkan masukkan Nomor dan jawaban kemudian enter</div>
			
	  <hr>
				 <div class="col-lg-2">Bobot Nilai </div>
					 <div class="col-lg-2">
						 <input class="form-control ketiksoal" onkeypress="return formatnilai(this);" trsoal_id="<?php echo $soal->id; ?>" kolom="bobot"   value="<?php echo $soal->bobot; ?>">  
					 </div>
					
					<div class="col-lg-6" style="float:right">
						
						  <button class="btn btn-sm btn-info tambahsoal" tmujian_id="<?php echo $soal->tmujian_id ?>"><i class="la la-plus-circle"></i> Tambah Soal  </button>
					 </div>
						
											   
	</div>
	
	
	<?php 
	
}else if($jenis==9){

	$pilihanganda = array("A"=>"opsi_a","B"=>"opsi_b","C"=>"opsi_c","D"=>"opsi_d","E"=>"opsi_e");
	foreach($pilihanganda as $ig=>$pg){
		?>
		  <div class="notbarsoal">
		  <hr>
			   <h4>OPSI JAWABAN  <?php echo $ig; ?> </h4><br>
			   <textarea id="editor<?php echo $pg; ?><?php echo $soal->id; ?>"  trsoal_id="<?php echo $soal->id; ?>" kolom="<?php echo $pg; ?>"   ><?php echo $soal->$pg; ?></textarea>
			   
				<script type="text/javascript">
		  
		  
		   var editorjawaban = CKEDITOR.replace( 'editor<?php echo $pg; ?><?php echo $soal->id; ?>', {
					  
						uploadUrl: base_url+'__statics/ckdrive/core/connector/php/connector.php?command=QuickUpload&type=Files&responseType=json&show=1',
						filebrowserBrowseUrl: base_url+'__statics/ckdrive/ckfinder.html',
						filebrowserImageBrowseUrl: base_url+'__statics/ckdrive/ckfinder.html?type=Images&show=1',
						filebrowserUploadUrl: base_url+'__statics/ckdrive/core/connector/php/connector.php?command=QuickUpload&type=Files&show=1',
						filebrowserImageUploadUrl: base_url+'__statics/ckdrive/core/connector/php/connector.php?command=QuickUpload&type=Images&show=1',
						enterMode : CKEDITOR.ENTER_BR,
						shiftEnterMode: CKEDITOR.ENTER_P,
						height:300,
						removeDialogTabs: 'image:advanced;link:advanced',
						 allowedContent :true
		  } );
		  

		  editorjawaban.on('instanceReady', function(evt) {
				  this.dataProcessor.htmlFilter.addRules({
					  elements: {
						  img: function(element) {
							  if (!element.hasClass('img-responsive')) {
								  element.addClass('img-responsive');
							  }
						  }
					  }
				  });
			  });


		  editorjawaban.on('change',function(){
			  
			  cek_opsijawaban("<?php echo $pg; ?>","<?php echo $soal->id; ?>");
		  });														
																	  
		  
		   </script>
					
		  </div>
	<?php } ?>
  
  
  <script type="text/javascript">
  function cek_opsijawaban(pg,soal){
			  var nilai = CKEDITOR.instances['editor'+pg+soal+''].getData();
			  var trsoal_id  = $("#editor"+pg+soal+"").attr("trsoal_id");
			  var kolom      = $("#editor"+pg+soal+"").attr("kolom");
				  $("#statussoal").html("<i class='fas fa-cog fa-spin'></i>    Sedang menyimpan..");
					   $.post("<?php echo site_url("teacherujian/simpan_soal"); ?>",{trsoal_id:trsoal_id,kolom:kolom,nilai:nilai},function(){
							   $("#statussoal").html("Tersimpan");
					   })
  }
  
  </script>
  
 

											 
<div class="row col-lg-12">



	 
	   <div class="col-lg-2">
		   Bobot Nilai  
		  
	   </div>
	   <div class="col-lg-2">
		<input class="form-control ketiksoal" onkeypress="return formatnilai(this);" trsoal_id="<?php echo $soal->id; ?>" kolom="bobot"   value="<?php echo $soal->bobot; ?>">  
	   </div>
	   
		<div class="col-lg-4" style="float:right">
		 
		 <button class="btn btn-sm btn-info tambahsoal" tmujian_id="<?php echo $soal->tmujian_id ?>"><i class="la la-plus-circle"></i>  Tambah Soal </button>
																	 
	  
	   </div>
</div>
<br>

<?php 

}
				  ?>
				  
				  
<script type="text/javascript">
 $(document).off("click",".benerteu").on("click",".benerteu",function(){
	     var trsoal_id = $(this).attr("trsoal_id");
	     
	     var jawaban     = $(this).attr("jawaban");
	      $("#statussoal").html("Proses..");
		$.post("<?php echo site_url("teacherujian/simpan_soal_bs"); ?>",{trsoal_id:trsoal_id,jawaban:jawaban},function(){
				 $("#statussoal").html("Tersimpan");
		 })
																			 
});												


$(document).off("input",".inputjodoh").on("input",".inputjodoh",function(){
	     var trsoal_id = $(this).attr("trsoal_id");
	     var kolom     = $(this).attr("kolom");
	     var kolom1    = $("#kolom1"+kolom+trsoal_id).val();
	     var kolom2    = $("#kolom2"+kolom+trsoal_id).val();
	    
		 
	     var jawaban     = $(this).attr("jawaban");
	      $("#statussoal").html("Proses..");
		$.post("<?php echo site_url("teacherujian/simpan_soal_jodoh"); ?>",{trsoal_id:trsoal_id,kolom:kolom,kolom1:kolom1,kolom2:kolom2},function(){
				 $("#statussoal").html("Tersimpan");
		 })
																			 
})	


</script>