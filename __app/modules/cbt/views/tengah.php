<style>

.rate {
    float: left;
    height: 46px;
    padding: 0 10px;
}
.rate:not(:checked) > input {
    position:absolute;
    top:-9999px;
}
.rate:not(:checked) > label {
    float:right;
    width:1em;
    overflow:hidden;
    white-space:nowrap;
    cursor:pointer;
    font-size:30px;
    color:#ccc;
}
.rate:not(:checked) > label:before {
    content: '★ ';
}
.rate > input:checked ~ label {
    color: #ffc700;    
}
.rate:not(:checked) > label:hover,
.rate:not(:checked) > label:hover ~ label {
    color: #deb217;  
}
.rate > input:checked + label:hover,
.rate > input:checked + label:hover ~ label,
.rate > input:checked ~ label:hover,
.rate > input:checked ~ label:hover ~ label,
.rate > label:hover ~ input:checked ~ label {
    color: #c59b08;
}

</style>
<div class="box box-warning">
    <div class="box-header with-border">
        <h3 class="box-title">Petunjuk Ujian CBT </h3>


    </div>


    <div class="box-body">
        <div class="row">

            <div class="col-md-10">
                <p>
                <ol start="1">
                    <li> Berdoalah kepada Allah SWT sebelum Anda memulai mengerjakan soal. </li>

                    <li> Waktu ujian akan dimulai ketika Anda menekan tombol <b><span class="fa fa-edit"></span> Mulai
                            Ujian </b> </li>

                </ol>

                </p>
            </div>
        </div>




    </div>

</div>



<?php
$ujian = $this->db->query("SELECT * FROM tm_ujian WHERE id='" . $ujian->id . "' ")->result();
foreach ($ujian as $rj) {

?>

<div class="box box-warning">


    <div class="box-body">

        <div class="info-box">
            <span class="info-box-icon bg-green"><i class="fa fa-file"></i></span>

            <div class="info-box-content">
                <span class="info-box-text" style="font-weight:bold"><?php echo ($rj->nama); ?> </span>
                <?php

                    $statusUjian = " Silahkan ikuti tes untuk mendapatkan sertifikat pelatihan ";


                    // $status        = "<span class='text-red'>Belum Aktif</span>";
                    // $button        = '<span class="text-red"> Belum Aktif </span>';

                    // if ($rj->status == 1) {
                        $status        = "<span class='text-yellow'> Aktif, Silahkan kerjakan </span>";
                        $button     = '<a class="btn btn-success" href="' . site_url('cbt/ujian?parameter=' . base64_encode($rj->id) . '&pelatihan_id=' . base64_encode($pelatihan->id) . '') . '"><i class="fa fa-edit"></i> Kerjakan  Ujian  </a> ';
                 //   }

                    $h_ujian = $this->db->query("select * from h_ujian where tmujian_id='" . $rj->id . "' and tmsiswa_id='" . $data->id . "'")->row();
                    if (!is_null($h_ujian)) {

                        if ($h_ujian->status == 'Y') {
                            $status        = "<span class='text-red'>Belum Menyelesaikan </span>";
                            $button        = '<a class="btn btn-success" href="' . site_url('cbt/ujian?parameter=' . base64_encode($rj->id) . '&pelatihan_id=' . base64_encode($pelatihan->id) . '') . '"><i class="fa fa-edit"></i> Lanjutkan Ujian  </a> ';
                        } else if ($h_ujian->status == 'N') {
                            // $status        ="<span class='text-success'>Sudah Mengikuti <br> Jumlah Benar : ".$h_ujian->jml_benar." <br> Skor : ".$h_ujian->nilai."</span>";
                            $status        = "<span class='text-success'>Sudah Mengikuti </span>";

                            $button        = '<span class="text-green"> Sudah dikerjakan </span> <br>';
                            $button        .= '<span class="text-green"> Skor Anda : '.$h_ujian->nilai.'<br> </span>';
                            $button        .= '<span class="text-black"> Passing Grade : '.$pelatihan->grade.' <br></span>';

                            if($h_ujian->nilai >= $pelatihan->grade){
                                $statusUjian        = '<h2 class="text-success" style="font-weight:bold"> SELAMAT  '.$data->nama.' !! </h2>  Anda lulus passing grade dan berhak mendapatkan sertifikat pelatihan   <br>';
                            }else{

                                $statusUjian        = '<h2 class="text-red" style="font-weight:bold">  MOHON MAAF  '.$data->nama.',  </h2>  Anda Tidak lulus passing grade pelatihan ini  <br> Apakah Anda ingin mengulangi pelatihan untuk mendapatkan sertifikat ? <br><br> <center><a href="'.site_url("cbt/repeat/".$pelatihan->slug."").'" class="btn btn-block btn-danger"> Ulangi Pelatihan </a> </center>  <br>';
                            }
                        }
                    }


                 




                    ?>



            </div>
            <div class="row">
                <div class="col-md-5"> <?php echo $rj->nama; ?>  </div>
                <div class="col-md-2">  <?php echo $rj->waktu; ?> Menit </div>                
                <div class="col-md-2"> <?php echo $button; ?> </div>


            </div>
                    <br>
                    <br>


            


        </div>
        <?php 
         if (!is_null($h_ujian)) {
             ?>

                 <div class="box box-warning">


                    <div class="box-body">
                        <?php echo $statusUjian; ?>
                        <br>
                         <?php 
                          if($h_ujian->nilai >= $pelatihan->grade){
                              ?>

                          <center> 
                              <a class="btn btn-success btn-block" href="<?php echo site_url('pelatihan/sertifikat?pelatihan_id='.base64_encode($pelatihan->id).''); ?>"><i class="fa fa-file"></i>  UNDUH SERTIFIKAT PELATIHAN  </a>
                          </center>
                             <?php 

                          }
                          ?>
                    </div>
                    </div>


                   
        <?php 
         }
         ?>

<div class="box box-primary">
<?php 
 $testimoni = $this->db->get_where("testimoni",array("pelatihan_id"=>$pelatihan->id,"peserta_id"=>$_SESSION['peserta_id']))->row();

 ?>

<div class="box-body">
     Berikan testimoni untuk kami, bagaimana pendapat Anda tentang platform pelatihan gratis ini ?
    <br>
    <form method="post" id="simpannorespon" action="javascript:void(0)" url="<?php echo site_url("cbt/saveTestimoni"); ?>">
    <input type="hidden" name="pelatihan_id" value="<?php echo $pelatihan->id; ?>">
    <input type="hidden" name="id" value="<?php echo isset($testimoni) ? $testimoni->id :""; ?>">
    <div class="rate">
        <input type="radio" id="star5" name="rate" value="5"  />
        <label for="star5" title="text">5 stars</label>
        <input type="radio" id="star4" name="rate" value="4" />
        <label for="star4" title="text">4 stars</label>
        <input type="radio" id="star3" name="rate" value="3" />
        <label for="star3" title="text">3 stars</label>
        <input type="radio" id="star2" name="rate" value="2" />
        <label for="star2" title="text">2 stars</label>
        <input type="radio" id="star1" name="rate" value="1" />
        <label for="star1" title="text">1 star</label>
    </div>

    <textarea class="form-control" name="testimoni" placeholder="Ketik Pendapat Anda disini..."><?php echo isset($testimoni) ? $testimoni->testimoni :""; ?></textarea>
    <br>
    <button type="submit" style="float:right" class="btn btn-primary btn-right"> <i class="fa fa-save"></i> Kirim Testimoni </button>

        </form>
</div>
</div>



    </div>
</div>

<?php
}
?>