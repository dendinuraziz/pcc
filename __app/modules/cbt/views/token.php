<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      CBT
      <small> <?php echo $this->Reff->owner(); ?> </small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
      <li class="active">Dashboard Ujian </li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">


    <div class="row">
      <div class="col-md-3">
        <!-- Widget: user widget style 1 -->
        <div class="box box-widget widget-user">
          <!-- Add the bg color to the header using any of the bg-* classes -->
          <div class="widget-user-header bg-aqua-active">
            <h5 class="" style="font-weight:bold">
              <center><?php echo $data->nama; ?></center>
            </h5>

          </div>
          <div class="widget-user-image">

            <img class="img-circle img-responsive" style="height:90px" src="<?php echo base_url(); ?>__statics/upload/profile/<?php echo $data->foto; ?>" alt="<?php echo $data->nama; ?>" onError="this.onerror=null;this.src='<?php echo base_url(); ?>__statics/img/not.png';">
          </div>
          <div class="box-footer">
            <div class="row">

              <!-- /.col -->

              <!-- /.col -->
              <div class="col-sm-12">
                <div class="description-block">
                  <h5 class="description-header"><?php echo $data->ptk_id; ?>  </h5>


                </div>
                <!-- /.description-block -->
              </div>
              <!-- /.col -->
            </div>
            <!-- /.row -->

            <div class="row">
              <div class="col-sm-12">

                <button class="btn btn-sm btn-danger btn-block bantuan" data-toggle="modal" data-target="#myModal"> <i class="fa fa-question-circle"></i> Bantuan </button>
                <a class="btn btn-sm btn-success btn-block " href="<?php echo site_url('pelatihan/detail/'.$pelatihan->slug); ?>"> <i class="fa fa-history"></i> Kembali  </a>
              </div>

            </div>
          </div>
          <!-- /.widget-user -->
        </div>



        <div class="box box-warning">

          <!-- /.box-header -->
          <div class="box-body">
            <small> Pastikan kamera laptop Anda aktif sebelum mengikuti ujian </small>

          
                <canvas id="canvas" width="320px" height="251px" style="display:none"></canvas>
                <video autoplay="true" id="player" style="width: 100%;height:auto">
                  Browsermu tidak mendukung, upgrade yaa !
                </video>
                <br>


                <button class="btn btn-danger btn-block" id="capture-btn"  style="display:none"><i class="fa fa-camera"></i> Ambil Gambar  </button>
              

            


          </div>
          <!-- /.box-body -->
        </div>
      </div>



      <div class="col-md-9">

        <?php $this->load->view("cbt/tengah"); ?>

      </div>


    </div>



  </section>

</div>



<script type="text/javascript">
  const videoPlayer = document.querySelector("#player");
  const canvasElement = document.querySelector("#canvas");
  const captureButton = document.querySelector("#capture-btn");
  const imagePicker = document.querySelector("#image-picker");
  //const imagePickerArea = document.querySelector("#pick-image");
  const newImages = document.querySelector("#newImages");

  // Image dimensions
  const width = 320;
  const height = 240;
  let zIndex = 1;

  const createImage = (src, alt, title, width, height, className) => {
    let newImg = document.createElement("img");

    if (src !== null) newImg.setAttribute("src", src);
    if (alt !== null) newImg.setAttribute("alt", alt);
    if (title !== null) newImg.setAttribute("title", title);
    if (width !== null) newImg.setAttribute("width", width);
    if (height !== null) newImg.setAttribute("height", height);
    if (className !== null) newImg.setAttribute("class", className);

    return newImg;
  };

  const startMedia = () => {
    if (!("mediaDevices" in navigator)) {
      navigator.mediaDevices = {};
    }

    if (!("getUserMedia" in navigator.mediaDevices)) {
      navigator.mediaDevices.getUserMedia = constraints => {
        const getUserMedia =
          navigator.webkitGetUserMedia || navigator.mozGetUserMedia;

        if (!getUserMedia) {
          return Promise.reject(new Error("getUserMedia is not supported"));
        } else {
          return new Promise((resolve, reject) =>
            getUserMedia.call(navigator, constraints, resolve, reject)
          );
        }
      };
    }

    navigator.mediaDevices
      .getUserMedia({
        video: true
      })
      .then(stream => {
        videoPlayer.srcObject = stream;
        videoPlayer.style.display = "block";
      })
      .catch(err => {

      });
  };


  captureButton.addEventListener("click", event => {
    $("#canvas").show("");



    canvasElement.style.display = "block";
    const context = canvasElement.getContext("2d");

    context.drawImage(videoPlayer, 0, 0, canvas.width, canvas.height);

    videoPlayer.srcObject.getVideoTracks().forEach(track => {

    });


    let picture = canvasElement.toDataURL();
    var ngepost = JSON.stringify({
      data: picture
    });

    $.post("<?php echo site_url('cbt/ambilfoto'); ?>", ngepost, function(data) {

      location.reload();

    });

  });

  window.addEventListener("load", event => startMedia());
</script>

<script type="text/javascript">
  var base_url = "<?php echo base_url(); ?>";
  $(document).ready(function() {
    //  ajaxcsrf();

    $('#btncek').on('click', function() {
      var token = $('#token').val();
      var literasi = "";

      var idUjian = $(this).data('id');
      var id_ujian = $(this).attr('id_ujian');
      var key = id_ujian;
      var hash = id_ujian;
      location.href = base_url + 'cbtguru/literasi?start=' + key + '&hash=' + hash;


    });

    var time = $('.countdown');
    if (time.length) {
      countdown(time.data('time'));
    }
  });
</script>