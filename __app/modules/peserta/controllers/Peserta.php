<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Peserta extends CI_Controller {

	 public function __construct()
      {
        parent::__construct();
		  if(!$this->session->userdata("peserta_id")){
			    
				echo $this->Reff->sessionhabis();
				exit();
			
		  }
		  $this->load->model('M_peserta','m');
		
	  }
	  
   function _template($data)
	{
	  $this->load->view('page_header',$data);	
	}
		
	public function index()
	{  
	     
		 $ajax            = $this->input->get_post("ajax",true);	
		 $data['data']    =  $this->db->get_where("peserta",array("id"=>$_SESSION['peserta_id']))->row();
		 $data['title']   = "Pelatihan Online - Peserta Pelatihan ".$data['data']->nama;
		 
	     if(!empty($ajax)){
					    
			 $this->load->view('page_default',$data);
		
		 }else{
			 
			
		     $data['konten'] = "page_default";
			 
			 $this->_template($data);
		 }
	

	}

	public function profile()
	{  
	     
		 $ajax            = $this->input->get_post("ajax",true);	
		 $data['data']    = $this->db->get_where("peserta",array("id"=>$_SESSION['peserta_id']))->row();
		 $data['title']   = "Profile Pelatihan ".$data['data']->nama;
		 
	     if(!empty($ajax)){
					    
			 $this->load->view('profile',$data);
		
		 }else{
			 
			
		     $data['konten'] = "profile";
			 
			 $this->_template($data);
		 }
	

	}



}
