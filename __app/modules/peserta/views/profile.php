<div class="instector-banner-area">
    </div>
   

    <div class="pd-bottom-115">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-4">
                    <div class="instructor-details-area text-center">
                        <div class="thumb">
                            <img src="<?php echo base_url(); ?>__statics/upload/profile/<?php echo $data->foto; ?>" onError="this.onerror=null;this.src='<?php echo base_url(); ?>__statics/img/not.png';" alt="img" style="width:150px">
                        </div>
                        <h3><?php echo $data->nama; ?></h3>
                        <p><?php echo $data->jabatan; ?></p>
                        <ul class="social-area d-inline-block">
                            <li><a href="#"><i class="fas fa-globe-asia"></i></a></li>
                            <li><a href="#"><i class="fab fa-linkedin"></i></a></li>
                            <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                            <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fab fa-youtube"></i></a></li>
                        </ul>
                        <ul class="achivement-fact">
                            <li class="ratting">
                                <div class="icon">
                                    <img src="<?php echo base_url(); ?>__statics/img/icon/star.png" alt="img">
                                </div>
                                <h5 class="counter">0.0</h5>
                                <p>Ratings</p>
                            </li>
                            <li class="students">
                                <div class="icon">
                                    <img src="<?php echo base_url(); ?>__statics/img/icon/user.png" alt="img">
                                </div>
                                <h5 class="counter">
                                 <?php 
                                        $pelatihanJml = $this->db->query("select count(id) as jml from tr_pelatihan where peserta_id='{$data->id}'")->row();
                                        echo $pelatihanJml->jml;
                                 ?>
                                  
                                </h5>
                                <p>Pelatihan</p>
                            </li>
                            <li class="courses">
                                <div class="icon">
                                    <img src="<?php echo base_url(); ?>__statics/img/icon/book.png" alt="img">
                                </div>
                                <h5 class="counter"><?php   echo $pelatihanJml->jml; ?> </h5>
                                <p>Sertifikat</p>
                            </li>
                        </ul>
                        <!-- <div class="text-start px-30">
                            <h5>Tentang Saya </h5>
                            <p><?php echo $data->about; ?></p>
                        </div> -->
                       
                    </div>
                </div>
                <div class="col-lg-8">
                    <ul class="nav instructor-nav nav-pills" id="pills-tab" role="tablist">
                        <li class="nav-item" role="presentation">
                          <button class="nav-link active" id="pills-home-tab" data-bs-toggle="pill" data-bs-target="#pills-home" type="button" role="tab" aria-controls="pills-home" aria-selected="true"> Profile Saya  </button>
                        </li>
                        <li class="nav-item" role="presentation">
                          <button class="nav-link" id="pills-profile-tab" data-bs-toggle="pill" data-bs-target="#pills-profile" type="button" role="tab" aria-controls="pills-profile" aria-selected="false"> Pelatihan Saya </button>
                        </li>
                        
                    </ul>
                    <div class="tab-content" id="pills-tabContent">

                        <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                            <div class="row">
                                <div class="col-md-12">
                                <div class="main-header">
                                        <div class="header-wraper">
                                            <div class="row">
                                                <div class="col-xl-6">
                                                    <span class="header-user">
                                                       
                                                        <span>Selamat Datang,
                                                            <h5><?php echo $data->nama; ?></h5>
                                                        </span>
                                                    </span>
                                                </div>
                                                <div class="col-xl-6 align-self-center text-lg-end">
                                                    <div class="d-lg-flex align-items-center">
                                                        <div class="user-rating text-center d-inline-block">
                                                            <span class="d-block">
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="far fa-star"></i>
                                                            </span>
                                                            4.0 (172 Ratings)
                                                        </div>
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- top header end  -->

                                    <!-- dashboard-area start  -->
                                    <div class="dashboard-profile-area">
                                        
                                        <!-- <a class="edit-btn" href="#" ><i class="fa fa-pencil-alt me-2"></i>Edit</a> -->
                                        <ul>
                                            <li><span>Simpatika ID </span><?php echo $data->simpatika_id; ?></li>
                                            <li><span>PTK  ID </span><?php echo $data->ptk_id; ?></li>
                                            <li><span>Nama Lengkap </span><?php echo $data->nama; ?></li>
                                            <li><span>Jenis Kelamin </span><?php echo $data->kelamin; ?></li>
                                            <li><span>Tanggal Lahir </span><?php echo $data->tgl_lahir; ?></li>
                                            <li><span>Instansi  </span><?php echo $data->instansi; ?></li>
                                            <li><span>Jabatan  </span><?php echo $data->jabatan; ?></li>
                                            <li><span>Status  </span><?php echo $data->m_status; ?></li>
                                            <li><span>Mapel Sertifikasi  </span><?php echo $data->m_mapel_sertifikasi; ?></li>
                                            <li><span>Provinsi   </span><?php echo $data->m_provinsi; ?></li>
                                            <li><span>Kab/Kota   </span><?php echo $data->m_kota; ?></li>
                                            
                                            <li><span>Bio </span><?php echo $data->about; ?></li>
                                        </ul>
                                    </div>
                                </div>
                               
                            </div>
                        </div>


                        <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
                            <div class="row">
                                <div class="col-md-12">

                                    <div class="table-responsive">
                                        <table class="table table-hover table-bordered table-striped">
                                            <thead>
                                                <tr>
                                                    <th> Kategori </th>
                                                    <th> Pelatihan </th>
                                                    <th> Progress </th>
                                                    
                                                    <th> Aksi </th>
                                                </tr>
                                               
                                            </thead>

                                            <tbody>
                                                <?php 
                                                 $pelatihan = $this->db->get_where("tr_pelatihan",array("peserta_id"=>$data->id))->result();
                                                 if(count($pelatihan) >0){
                                                  foreach($pelatihan as $row){
                                                      $pelatihan_data = $this->db->get_where("pelatihan",array("id"=>$row->pelatihan_id))->row();

                                                        $materi  = $this->db->query("select count(id) as jml from v_silabus where pelatihan_id='{$pelatihan_data->id}'")->row();
                                                        $progress= $this->m->progress($pelatihan_data->id);
                                                        $persen  = ($progress/$materi->jml) * 100;


                                                ?>
                                                    <tr>
                                                        <td><?php echo $this->Reff->get_kondisi(array("id"=>$pelatihan_data->kategori_id),"kategori","nama"); ?></td>
                                                        <td><?php echo $pelatihan_data->nama; ?></td>

                                                        <td> <div class="progress-item">
                                                <div class="row align-items-center">
                                                    
                                                    <div class="col-12 text-end">
                                                        <span>Progress: <br>  <span><?php echo $progress; ?> / <?php echo $materi->jml; ?></span></span>
                                                    </div>
                                                </div>
                                                <div class="progress-bg">
                                                    <div id="progress-1" class="progress-rate" data-value="<?php echo $persen; ?>">
                                                        
                                                    </div>
                                                </div>
                            </div>
                                            
                                                    </td>
                                                        <td><?php echo $pelatihan_data->nama; ?></td>
                                                        <td><a href="<?php echo site_url('pelatihan/detail/'.$pelatihan_data->slug); ?>" class="btn btn-primary btn-sm"><i class="fa fa-file"></i> Buka Pelatihan</a></td>
                                                    </tr>
                                                <?php 
                                                  }
                                                }else{

                                                    ?>

                                                    <tr>
                                                        <td align="center"> Anda belum pernah mengikuti pelatihan </td>
                                                        
                                                    </tr>



                                                    <?php 


                                                }
                                                ?>
                                            </tbody>

                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
           
        </div>            
    </div>
    <!-- instructor Area End -->

  