<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'web';
$route['404_override'] = 'web/notdendi';
$route['translate_uri_dashes'] = FALSE;

$route['course/(:any)'] = 'course/data/$1';
$route['search'] = 'course/search';
$route['api/v1/aman/shortlist'] = 'api/aman/shortlist';
$route['api/v1/aman/asesor']    = 'api/aman/asesor';
$route['api/v1/aman/generateKeyDendi'] = 'api/aman/generateKeyDendi';
$route['api/v1/aman/bukti']            = 'api/aman/bukti';

