<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// To use reCAPTCHA, you need to sign up for an API key pair for your site.
// link: http://www.google.com/recaptcha/admin
$config['recaptcha_site_key'] = '6Ld3rUEUAAAAAE3X3s5exr8-mM7V9Pgv38frXLbF';
$config['recaptcha_secret_key'] = '6Ld3rUEUAAAAAJLr_zCn8p3tCbwZtBjKXd8ZppEX';

// reCAPTCHA supported 40+ languages listed here:
// https://developers.google.com/recaptcha/docs/language
$config['recaptcha_lang'] = 'en';

/* End of file recaptcha.php */
/* Location: ./application/config/recaptcha.php */
