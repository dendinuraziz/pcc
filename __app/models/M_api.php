<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_api extends ci_Model
{
	 protected $headers = array();

    
     protected $options = array();
	 
	 
	public function __construct() {
        parent::__construct();
     	}
	 
   public function getMadrasah(){
      $tahun   = $this->input->post("tahun");
      $nsm     = $this->input->post("nsm");
      $this->db->select("nsm,npsn,jenjang,nama,provinsi_id,kota_id,bantuan,tahap");
      $this->db->from("madrasahedm");
      $this->db->where("tahun",$tahun);
      if(!empty($nsm)){

          $this->db->where("nsm",$nsm);
      }
      $this->db->where("shortlist",1);
      return $this->db->get();
   }

   public function getMadrasahDNT(){
      $tahun   = $this->input->post("tahun");
      $nsm     = $this->input->post("nsm");
      $this->db->select("nsm,npsn,jenjang,nama,provinsi_id,kota_id,provinsi,kota,bantuan,rank_visitasi as peringkat");
      $this->db->from("madrasahedm");
      $this->db->where("tahun",$tahun);
      $this->db->where("dnt",1);
      if(!empty($nsm)){

          $this->db->where("nsm",$nsm);
      }
      $this->db->where("shortlist",1);
      $this->db->order_by("rank_visitasi","ASC");
      return $this->db->get();
   }

   public function getAsesor(){
      $nik     = $this->input->post("nik");
      
      $this->db->select("id,nik,nama");
      $this->db->from("asesor");
      $this->db->where("nik",$nik);
     
      return $this->db->get();
   }

   public function getAsesorMadrasah($asesor){

      $nsm     = $this->input->post("nsm");
      $this->db->select("id,nsm,nama");
      $this->db->from("madrasahedm");
      $this->db->where("nsm",$nsm);      
      $this->db->where("id IN(select madrasah_id from visitasi_kegiatan where asesor='{$asesor}')");      
      $this->db->where("shortlist",1);
      return $this->db->get();
   }
	
}

?>